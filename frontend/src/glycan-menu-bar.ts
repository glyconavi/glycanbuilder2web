import {LitElement, html} from 'lit-element';
import '@vaadin/vaadin-ordered-layout/vaadin-horizontal-layout.js';
import '@vaadin/vaadin-menu-bar/vaadin-menu-bar.js';



class GlycanMenuBar extends LitElement {
    render() {
        return html`
            <vaadin-horizontal-layout>
                <vaadin-menu-bar id="glycanMenuBar"></vaadin-menu-bar>
            </vaadin-horizontal-layout>
            `;
    }
}

customElements.define('glycan-menu-bar', GlycanMenuBar);