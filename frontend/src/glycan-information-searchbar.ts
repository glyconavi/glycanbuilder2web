import {LitElement, html} from 'lit-element';
import '@vaadin/vaadin-ordered-layout/vaadin-horizontal-layout.js';
import '@vaadin/vaadin-button/vaadin-button.js';
import '@vaadin/vaadin-select/vaadin-select.js';

class GlycanInformationSearchbar extends LitElement {
    render() {
        return html`
            <vaadin-horizontal-layout>
                <vaadin-select id="selectedDatabase"></vaadin-select>
                <vaadin-button id="glycanInformationSeachButton" class="toolbarButton"><img src="/images/search.png"></vaadin-button>
            </vaadin-horizontal-layout>
            `;
    }
}

customElements.define('glycan-information-searchbar', GlycanInformationSearchbar);