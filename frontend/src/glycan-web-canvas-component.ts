import {LitElement, html} from 'lit-element';

class GlycanWebCanvasComponent extends LitElement {
    render() {
        return html`
            <slot></slot>
            `;
    }
}

customElements.define('glycan-web-canvas-component', GlycanWebCanvasComponent);