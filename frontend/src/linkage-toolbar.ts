import {LitElement, html, css} from 'lit-element';
import '@vaadin/vaadin-ordered-layout/vaadin-vertical-layout.js';
import '@vaadin/vaadin-select/vaadin-select.js';
import '@vaadin/vaadin-checkbox/theme/lumo/vaadin-checkbox.js';
import 'Frontend/src/multiple-checkbox.ts';

class LinkageToolbar extends LitElement {
    render() {
        return html`
            <vaadin-vertical-layout class="linkageToolbarContainer">
                <vaadin-vertical-layout  class="title">
                    <label>Linkage</label>
                </vaadin-vertical-layout>
                <vaadin-vertical-layout class="linkageContainer">
                    <vaadin-select id="anomericState" value="?"></vaadin-select>
                    <vaadin-select id="anomericCarbon"></vaadin-select>
                    <img src="/images/linkage-arrow.png">
                    <multiple-checkbox id="linkagePosition"></multiple-checkbox>
                </vaadin-vertical-layout>
                <vaadin-vertical-layout class="linkageContainer">
                    <vaadin-checkbox id="secondBond" value="2nd bond">2nd bond</vaadin-checkbox>
                    <vaadin-select id="secondChildPosition" class="secondBondIsNotChecked"></vaadin-select>
                    <img id="secondBondArrow" class="secondBondIsNotChecked" src="/images/linkage-arrow.png">
                    <multiple-checkbox id="secondParentPosition" class="secondBondIsNotChecked"></multiple-checkbox>
                </vaadin-vertical-layout>
                <vaadin-vertical-layout  class="title">
                    <label>Chirality</label>
                </vaadin-vertical-layout>
                <vaadin-vertical-layout class="linkageContainer">
                    <vaadin-select id="chirality"></vaadin-select>
                </vaadin-vertical-layout>
                <vaadin-vertical-layout  class="title">
                    <label>Ring</label>
                </vaadin-vertical-layout>
                <vaadin-vertical-layout class="linkageContainer">
                    <vaadin-select id="ringSize"></vaadin-select>
                </vaadin-vertical-layout>
            </vaadin-vertical-layout>
            `;
    }

    static get styles() {
        return css`
            .linkageToolbarContainer {
                align-items: center;
                width: 168px;
                min-height: calc(100vh - 90px);
                background: #3E3E3E;
            }
            .linkageContainer {
                align-items: center;
                width: 110px;
                margin-bottom: 24px
            }
            .linkageContainer img {
                margin-top: 18px;
                padding-right: 7px;
            }
            .title {
                justify-content: center;
                align-items: center;
                width: 100%;
                height: 30px;
                font-size: 14px;
                color: #D5D5D5;
                background: #343434;
                margin-bottom: 10px;
            }
            .secondBondIsNotChecked {
                display: none;
            }
            .secondBondIsChecked {
                display: block;
            }
        `;
    }
}

customElements.define('linkage-toolbar', LinkageToolbar);