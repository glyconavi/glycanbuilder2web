import {LitElement, html, css} from 'lit-element';
import { registerStyles, css as registeredCSS } from '@vaadin/vaadin-themable-mixin/register-styles.js';
import '@vaadin/vaadin-ordered-layout/vaadin-horizontal-layout.js';
import '@vaadin/vaadin-ordered-layout/vaadin-vertical-layout.js';
import 'Frontend/src/glycan-menu-bar.ts';
import 'Frontend/src/residues-toolbar.ts';
import 'Frontend/src/structure-toolbar.ts';
import 'Frontend/src/linkage-toolbar.ts';
import 'Frontend/src/glycan-information-searchbar.ts';
import 'Frontend/src/glycan-web-canvas-component.ts';

class GlycanBuilder2Web extends LitElement {
    render() {
        return html`
            <vaadin-horizontal-layout>
                <glycan-menu-bar id="glycanMenubar"></glycan-menu-bar>
            </vaadin-horizontal-layout>
            <div class="toolbar">
                <vaadin-horizontal-layout>
                    <structure-toolbar id="structureToolbar"></structure-toolbar>
                    <glycan-information-searchbar id="glycanInformationSearchbar"></glycan-information-searchbar>
                </vaadin-horizontal-layout>
            </div>
            <vaadin-horizontal-layout>
                <linkage-toolbar id="linkageToolbar"></linkage-toolbar>
                <vaadin-vertical-layout>
                    <residues-toolbar id="residuesToolbar"></residues-toolbar>
                    <glycan-web-canvas-component id="glycanWebCanvasComponent"></glycan-web-canvas-component>
                </vaadin-vertical-layout>
            </vaadin-horizontal-layout>
            `;
    }

    static get styles() {
        return css`
            glycan-menu-bar {
                box-sizing: border-box;
                width: 100%;
                background: #141414;
                padding: 8px 0px 0px 8px;
                --lumo-primary-color-50pct: #00000000;
            }
            .toolbar {
                background: #413838;
                padding: 6px 18px;
            }
            structure-toolbar {
                flex: 1;
            }
            glycan-information-searchbar {
                margin-left: auto;
            }
            residues-toolbar {
                width: calc(100% - 24px);
                height: 46px;
                background: #EBEBEB;
                border-bottom: 1px solid #707070;
                padding-left: 24px;
            }
            .structuresStringTextArea {
                font-size: 14px;
            }
        `;
    }
}

registerStyles('vaadin-vertical-layout', registeredCSS`
    :host([theme~='padding']) {
        padding: 0;
    }
`);
registerStyles('vaadin-horizontal-layout', registeredCSS`
    :host(#scaleButtons) {
        margin-left: auto;
    }
`);
registerStyles('vaadin-menu-bar', registeredCSS`
    [part="menu-bar-button"] {
        height: 34px;
        box-sizing: border-box;
        margin: 0;
        padding: 0 12px;
        --lumo-primary-color-10pct: #454545;
    }
    [part="menu-bar-button"]:hover {
        background: #454545;
    }
    [part="menu-bar-button"]:first-of-type,
    [part="menu-bar-button"]:nth-last-of-type(2) {
        border-radius: 0;
    }
    [theme="menu-bar-item"] {
        color: #DDD;
    }
    :host(#allResidueMenuBar) {
        margin: 2px 0;
    }
    :host(#allResidueMenuBar) [part="menu-bar-button"] {
        //width: 36px;
        height: 36px;
        box-sizing: border-box;
        background: #00000000;
        margin: 2px 0;
        padding: 0;
    }
    :host(#allResidueMenuBar) [part="menu-bar-button"]:hover {
        background: #C6C6C6;
    }
    :host(#allResidueMenuBar) [part=overflow-button] {
        color: #9A9A9A;
        border-radius: 0;
        background: #00000000;
    }
    :host(#allResidueMenuBar) [part=overflow-button]:hover {
        color: #9A9A9A;
        background: #C6C6C6;
    }
`);
registerStyles('vaadin-context-menu-overlay', registeredCSS`
    :host(:first-of-type) {
        padding: 0;
    }
    [part="overlay"] {
        box-sizing: border-box;
        min-width: 240px;
        font-size: 16px;
        color: #D5D5D5;
        background: #141414;
        padding: 6px 0;
        border-radius: 0;
    }
    [part="content"] {
        padding: 0;
    }
`);
registerStyles('vaadin-context-menu-item', registeredCSS`
    :host {
        height: 36px;
        text-align: left;
        border-radius: 0!important;
        --lumo-primary-color-50pct: #00000000;
        --lumo-primary-color-10pct: #454545;
        --lumo-primary-text-color: #D5D5D5;
        --lumo-tertiary-text-color: #D5D5D5;
        --lumo-border-radius-m: 96px;
    }
    :host(.vaadin-menu-item)::before {
        margin-right: 8px;
    }
`);
registerStyles('vaadin-button', registeredCSS`
    :host(.toolbarButton) {
        min-width: 24px;
        border-radius: 0!important;
        margin: 0 6px;
        padding: 6px;
        --lumo-primary-color-50pct: #00000000;
        --lumo-primary-text-color: #D5D5D5;
    }
    :host(#repeatButton) {
        margin: 0 0 0 6px;
    }
    :host(#residuePropertiesButton) {
        margin: 0 6px 0 0;
    }
    :host(#orientationButton) {
        margin: 0 20px 0 6px;
    }
    :host(#enlargeButton),
    :host(#shrinkButton) {
        margin: 0;
    }
    :host(.toolbarButton:hover) {
        background: #D5D5D566;
    }
    :host(.toolbarButton[disabled][disabled]) {
        opacity: 0.3;
    }
    :host(.propertyToolbar) {
        min-width: 18px;
        border-radius: 0!important;
        background: #342E2E;
        margin: 0 6px;
        padding: 0;
        --lumo-primary-color-50pct: #00000000;
        --lumo-primary-text-color: #D5D5D5;
    }
    :host(.propertyToolbar:hover) {
        background: #D5D5D566;
    }
    :host(.propertyToolbar[disabled][disabled]) {
        opacity: 0.3;
    }
    :host(.residueButton) {
        --lumo-space-m: 0;
        padding: 6px;
        border-radius: 0!important;
        --lumo-contrast-5pct: #00000000;
        --lumo-primary-text-color: #D5D5D5;
    }
    :host(.residueButton:hover) {
        background: #C6C6C6;
    }
    :host(.buttonInDialog) {
        font-size: 12px;
        color: #D5D5D5;
        min-width: 24px;
        height: 28px;
        border-radius: 2px;
        border: 1px solid #707070;
        background: #00000000;
        margin: 24px 8px 0 0!important;
        padding: 4px 20px 8px;
        --lumo-primary-color-50pct: #00000000;
        --lumo-primary-text-color: #707070;
    }
    :host(.buttonInDialog:hover) {
        background: #707070;
        --lumo-primary-text-color: #FFF;
    }
    :host(.buttonHorizontalLayoutInDialog) {
        margin: 0 8px 0 16px!important;
    }
    :host(.button2ndHorizontalLayoutInDialog) {
        margin: 0 8px 0 0!important;
    }
    :host(.buttonInOutputTextAreaDialog) {
        margin: 24px 0 0!important;
    }
    :host(.buttonInCompositionDialog) {
        margin-top: 14px!important;
    }
`);
registerStyles('vaadin-select', registeredCSS`
    :host(#selectedDatabase),
    :host(#anomericState),
    :host(#anomericCarbon),
    :host(#secondChildPosition),
    :host(#chirality),
    :host(#ringSize),
    :host(.selectInDialog) {
        height: 28px;
        margin: -10px 6px 0 6px;
        padding: 0;
        --lumo-size-m: 0;
        --lumo-line-height-m: 0;
        --lumo-contrast-10pct: #00000000;
        --lumo-primary-color-50pct: #00000000;
        --lumo-body-text-color: #D5D5D5;
        --lumo-disabled-text-color: #D5D5D5;
        --lumo-contrast-5pct: #00000000;
        --lumo-space-xs: 0;
        --lumo-icon-size-m: 16px;
    }
    :host(#selectedDatabase) {
        width: 210px;
    }
    :host(#anomericState),
    :host(#anomericCarbon),
    :host(#chirality),
    :host(#ringSize),
    :host(#secondChildPosition) {
        width: 100%;
        margin: -10px 6px 12px 6px;
    }
    :host([disabled]) {
        opacity: 0.26;
    }
    [part='toggle-button']::before {
        padding-top: 3px;
    }
    :host(.selectInDialog) {
        width: 100%;
        margin: -24px 0 10px!important;
    }
    :host(.selectInOutputStringDialog) {
        width: 168px;
        margin: -24px 0 24px!important;
    }
    :host(.selectInDisplaySettingDialog) {
        width: auto;
    }
`);
registerStyles('vaadin-select-text-field', registeredCSS`
    :host {
        border-bottom: 1px solid #D5D5D5;
        --lumo-contrast-60pct: #D5D5D5;
        --lumo-contrast-80pct: #D5D5D5;
    }
    [part="input-field"] {
        text-align: center;
        margin: 4px 0 -4px;
        padding-left: 16px;
    }
`);
registerStyles('vaadin-item', registeredCSS`
    :host {
        text-align: center;
        padding: 0 0 2px 0;
        border-radius: 0!important;
        --lumo-size-m: 28px;
        --lumo-space-l: 32px;
        --lumo-primary-text-color: #707070;
    }
`);
registerStyles('vaadin-select-overlay', registeredCSS`
    :host {
        margin-top: 24px;
        padding: 0;
        --lumo-primary-color-50pct: #00000000;
    }
    [part="content"] {
        padding: 0;
    }
    [part="overlay"] {
        border-radius: 0;
    }
`);
registerStyles('vaadin-checkbox', registeredCSS`
    :host(#secondBond),
    :host(.checkboxInDialog) {
        font-size: 12px;
        color: #D5D5D5;
        --lumo-shade-20pct: #00000000;
        --lumo-contrast-20pct: #00000000;
        --lumo-primary-color-50pct: #00000000;
        --lumo-disabled-text-color: #D5D5D5;
        --lumo-primary-color: #D5D5D5;
    }
    :host(#secondBond) [part="checkbox"],
    :host(.checkboxInDialog) [part="checkbox"] {
        border: 1px solid #D5D5D5;
        --lumo-border-radius-s: 0;
    }
    :host(#secondBond[checked]) [part='checkbox']::after,
    :host(.checkboxInDialog) [part='checkbox']::after {
        --lumo-primary-contrast-color: #3E3E3E;
    }
    :host(#linkageInformation),
    :host(#masses) {
        color: #D5D5D5;
        --lumo-shade-20pct: #00000000;
        --lumo-contrast-20pct: #00000000;
        --lumo-primary-color-50pct: #00000000;
        --lumo-disabled-text-color: #D5D5D5;
        --lumo-primary-color: #00000000;
    }
    :host(#linkageInformation) [part="checkbox"],
    :host(#masses) [part="checkbox"] {
        border: none;
        --lumo-border-radius-s: 0;
        --lumo-shade-30pct: #00000000;
    }
    :host(#linkageInformation) [part='checkbox']::after,
    :host(#masses) [part="checkbox"]::after,
    :host(#linkageInformation[checked]) [part='checkbox']::after,
    :host(#masses[checked]) [part="checkbox"]::after {
        top: 0;
        left: 0;
        border-width: 0;
        transform: none;
    }
    :host(#linkageInformation) [part='checkbox']::after,
    :host(#masses) [part="checkbox"]::after {
        top: 2px;
        content: url('/images/eye-blocked.png');
        opacity: 0.26;
    }
    :host(#linkageInformation[checked]) [part='checkbox']::after,
    :host(#masses[checked]) [part="checkbox"]::after {
        content: url('/images/eye.png');
        opacity: 1;
    }
    :host(#linkageInformation:hover) [part="checkbox"],
    :host(#masses:hover) [part="checkbox"] {
        background: #00000000;
    }
    :host(.multipleCheckbox) {
        width: 24px;
        height: 24px;
        text-align: center;
        border: 1px solid #D5D5D544;
        margin: 0 0 4px;
        color: #D5D5D5;
        --lumo-disabled-text-color: #D5D5D5;
    }
    :host(.intermediateMultipleCheckbox) {
        margin-left: 2px!important;
    }
    :host(.multipleCheckbox[checked]) {
        color: #3E3E3E;
        background: #D5D5D5;
    }
    :host([disabled]) {
        opacity: 0.26;
    }
    :host(.multipleCheckbox[disabled]) {
        border: 1px solid #D5D5D5;
    }
    :host(.multipleCheckbox) [part="checkbox"] {
        display: none;
    }
    :host(.multipleCheckbox) [part="label"] {
        margin: 0;
    }
`);
registerStyles('vaadin-dialog-overlay', registeredCSS`
    [part="overlay"] {
        color: #D5D5D5;
        box-shadow: 3px 3px 12px rgba(0, 0, 0, 0.6);
        background: #3E3E3E;
        --lumo-border-radius-l: 16px;
    }
    [part="content"] {
        padding: 64px 72px;
    }
`);
registerStyles('vaadin-text-area', registeredCSS`
    :host {
        font-size: 14px;
        --lumo-primary-color-50pct: #00000000;
    }
    [part="input-field"] {
        color: #D5D5D5;
        border: 1px solid #D5D5D5;
        background: #636363!important;
    }
`);
registerStyles('vaadin-integer-field', registeredCSS`
    :host {
        width: 88px;
        --lumo-space-xs: 0;
        --lumo-shade-50pct: #00000000;
        --lumo-primary-color-50pct: #00000000;
    }
    [part="input-field"] {
        height: 22px;
        color: #D5D5D5;
        border: 1px solid #707070;
        border-radius: 0;
        --lumo-contrast-10pct: #00000000;
    }
    [part="input-field"]:hover {
        --lumo-contrast-10pct: #00000000;
    }
    [part="increase-button"] {
        width: 21px;
        height: 20px;
        font-size: 14px;
        color: #D5D5D5;
        border-left: 1px solid #707070;
        border-radius: 0;
        background: #464646;
    }
    [part="increase-button"]:hover {
        color: #D5D5D5;
        background: #707070;
    }
    [part="decrease-button"] {
        width: 21px;
        height: 20px;
        font-size: 14px;
        color: #D5D5D5;
        border-right: 1px solid #707070;
        border-radius: 0;
    }
    [part="decrease-button"]:hover {
        color: #D5D5D5;
        background: #707070;
    }
`);
customElements.define('glycan-builder2-web', GlycanBuilder2Web);