import {LitElement, html, css} from 'lit-element';
import '@vaadin/vaadin-ordered-layout/vaadin-horizontal-layout.js';

class MultipleCheckbox extends LitElement {
    render() {
        return html`
            <vaadin-horizontal-layout id="multipleCheckboxLayout"></vaadin-horizontal-layout>
            `;
    }

    static get styles() {
        return css`
            #multipleCheckboxLayout {
                flex-wrap: wrap;
                margin: 12px 0 0;
                --lumo-space-m: 0;
            }
        `;
    }
}

customElements.define('multiple-checkbox', MultipleCheckbox);