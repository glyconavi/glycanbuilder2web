import {LitElement, html, css} from 'lit-element';
import '@vaadin/vaadin-button/vaadin-button.js';
import '@vaadin/vaadin-checkbox/theme/lumo/vaadin-checkbox.js';

class StructureToolbar extends LitElement {
    render() {
        return html`
            <vaadin-horizontal-layout>
                <vaadin-button  id="reloadButton" class="toolbarButton"><img src="/images/reload.png"></vaadin-button>
                <vaadin-button  id="undoButton" class="toolbarButton"><img src="/images/undo.png"></vaadin-button>
                <vaadin-button  id="redoButton" class="toolbarButton"><img src="/images/redo.png"></vaadin-button>
                <vaadin-button  id="deleteButton" class="toolbarButton"><img src="/images/eraser.png"></vaadin-button>
                <div class="partition"></div>
                <vaadin-button  id="importButton" class="toolbarButton"><img src="/images/import.png"></vaadin-button>
                <vaadin-button  id="exportButton" class="toolbarButton"><img src="/images/export.png"></vaadin-button>
                <div class="partition"></div>
                <vaadin-button  id="bracketButton" class="toolbarButton"><img src="/images/bracket.png"></vaadin-button>
                <vaadin-button  id="cyclicButton" class="toolbarButton"><img src="/images/cyclic.png"></vaadin-button>
                <div class="partition"></div>
                <vaadin-button  id="repeatButton" class="toolbarButton"><img src="/images/repeat.png"></vaadin-button>
                <vaadin-button  id="residuePropertiesButton" class="propertyToolbar"><img src="/images/properties.png"></vaadin-button>
                <div class="partition"></div>
                <vaadin-button  id="orientationButton" class="toolbarButton"><img src="/images/rl.png"></vaadin-button>
                <vaadin-checkbox id="linkageInformation">Linkage info</vaadin-checkbox>
                <vaadin-checkbox id="masses">Masses</vaadin-checkbox>
                <vaadin-horizontal-layout id="scaleButtons">
                    <vaadin-button  id="enlargeButton" class="toolbarButton"><img src="/images/enlarge.png"></vaadin-button>
                    <vaadin-button  id="shrinkButton" class="toolbarButton"><img src="/images/shrink.png"></vaadin-button>
                </vaadin-horizontal-layout>
            </vaadin-horizontal-layout>
            `;
    }

    static get styles() {
        return css`
            .partition {
                width: 1px;
                background: #141414;
            }
        `;
    }
}

customElements.define('structure-toolbar', StructureToolbar);