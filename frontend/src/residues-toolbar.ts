import {LitElement, html} from 'lit-element';
import '@vaadin/vaadin-ordered-layout/vaadin-horizontal-layout.js';
import '@vaadin/vaadin-menu-bar/vaadin-menu-bar.js';

class ResiduesToolbar extends LitElement {
    render() {
        return html`
            <vaadin-horizontal-layout>
                <vaadin-horizontal-layout id="residuesToolbarLayout"></vaadin-horizontal-layout>
                <vaadin-menu-bar id="allResidueMenuBar"></vaadin-menu-bar>
            </vaadin-horizontal-layout>
            `;
    }
}

customElements.define('residues-toolbar', ResiduesToolbar);