# GlycanBuilder4Web
## Features


## Requirements
* Java 11+
* Apache Maven 3.8.3+
* Node.js 10+

Note that may not work the application if you use latest version of Node.js. We recomend you use LTS version of it. We comfirmed to work the application with below version of it:
* 10.19.0
* 14.17.0
* 16.16.0

## Build
```
mvn validate
```
```
mvn compile
```

```
mvn jetty:run
```
* You can access from http://localhost:8080/, if you run on local server.

## Change URL of searching glycan
* Open SearchAddress.java from src/main/java/org.glycoinfo.vaadin.glycanbuilder2web.util.
* Open GlycanInformationSearchbar.java from src/main/java/org/glycoinfo/vaadin/glycanbuilder2web/toolbar.
* Change sparqlistOfGlycoNaviAdress on SearchAddress.java and glyTouCanIDURL on GlycanInformationSearchbar.java for URL of getting GlyTouCanID.
* Change glyTouCanAdress on SearchAddress.java and glyTouCanURL on GlycanInformationSearchbar.java for URL of searching glycan with GlyTouCan.
* Change glyCosmosAdress on SearchAddress.java and glyCosmosURL on GlycanInformationSearchbar.java for URL of searching glycan with GlyCosmos.
* Change glycoNaviAdress on SearchAddress.java and glycoNaviURL on GlycanInformationSearchbar.java for URL of searching glycan with GlycoNavi.

## Location of getting GlyTouCanID
* private String getGlyTouCanID(String wurcs) on GlycanInformationSearchbar.java from src/main/java/org/glycoinfo/vaadin/glycanbuilder2web/toolbar.

## How to debug on Visual Studio Code
* Clone this repository.
* Open this repository on Visual Studio Code.
* Ctrl + Shift + P and select "Run Task".
* Select below task.
  * Maven Validate
  * Maven Compile
  * Maven Run
* Open Debug tab and select "Debug Vaadin App".

## Release notes

### 1.5.3 (29 Oct 2024)
* Updated lib: glycanbuilder2 version 1.25.4

### 1.5.2 (20 May 2024)
* Updated lib: glycanbuilder2 version 1.25.2

### 1.5.1 (8 Apr 2024) 
* Updated lib: glycanbuilder2 version 1.25.1

### 1.5.0 (31 Oct 2023) 
* Updated lib: glycanbuilder2 version 1.25.0
  * (changed not to export to WURCS from Composition structure.)

### 1.4.0 on Sep 15th, 2023
* Update lib: glycanbuilder2 version 1.24.0

### 1.3.1.0 on Aug. 14th, 2023
* Add function of display settings to tweak canvas settings.
* Add function to copy image to clipboard using MetaKey + C.

### 1.3.0.0 on Aug. 1st, 2023
* Enabled copy glycan canvas to clipboard.
* Update version of VaadinWebCanvas to 1.0.0.8.
* Fix the text for mass to be 14px instead of 14pt.

### 1.2.0 on June 29th, 2023
* update glycanbuilder2 version 1.23.1
* change software name: GlycanBuilder2Web to GlycanBuilder4Web 

### 1.1.0-SNAPSHOT on June. 27th, 2023
* update glycanbuilder2 1.23.0-SNAPSHOT

### 1.0.3.0 on Mar. 25th, 2022
* Fix issue of the root node of N-glycan oligosaccharide is wrong.

### 1.0.0.0 on Mar. 10th, 2022
* Fix issue of being not able to seach glycan if not existing structure on the GlyCosmos (partial)、GlycoNAVI.
* Fix issue of not be displayed border top of increase button.
* Fix issue of displaying dialog about live reload.

### 1.0.0.0 on Mar. 9th, 2022
* Add search method of Glycosmos partia.
* Add function of exporting to svg.

### 0.0.25.0 on Mar. 9th, 2022
* Apply design for dialog when search glycan.
* Add function of display / hide second bond parameter when checking 2nd bond on linkage toolbar.
* Chane GlycanBuilder2 on pom.xml.

### 0.0.24.0 on Mar. 4th, 2022
* Add function of switching whether to display masses, and switching linkage info.
* Add function of change scale canvas.
* Add button of all residue menui.
* Fix an issue that characters do not become large when changing scale.

### 0.0.23.0 on Mar. 2nd, 2022
* Apply design of dialog.

### 0.0.22.0 on Feb. 22th, 2022
* Apply design of main screen.

### 0.0.20.2 on Feb. 2nd, 2022
* Update Lib.: GlycanBuilder2 version 1.12.0 to 1.15.0

### 0.0.20.1 on Feb. 2nd, 2022
* Change URL of searching glycan

### 0.0.20.0 on Feb. 1st, 2022
* Replace MultipleSelect to MultipleCheckbox on LinkageToolbar.

### 0.0.19.0 on Jan. 7th, 2022
* Add tooltip to buttons.
* Add function of figuring whether if current residue is alditol when selecting linkage information.

### 0.0.18.0 on Dec. 28th, 2021
* Add function of adjusting size of canvas.

### 0.0.17.0 on Dec. 23th, 2021
* Fix issue of not adding structure when changing notation.

### 0.0.16.0 on Dec. 22th, 2021
* Add function of selecting multiple linkage position.

### 0.0.15.0 on Dec. 16th, 2021
* Add function of export to image.

### 0.0.14.0 on Dec. 10th, 2021
* Add function changing notation of structure.

### 0.0.13.0 on Dec. 9th, 2021
* Add function of changing reducing end type.
* Add function of changing scale rate.

### 0.0.12.0 on Dec. 8th, 2021
* Add function of adding terminal.
* Add function of adding composition.

### 0.0.11.0 on Nov. 24th, 2021
* Add function of showing about dialog.

### 0.0.10.0 on Nov. 19th, 2021
* Add function of reload canvas.
* Add function of changing style of structures.
* Add function of canvas view setting.

### 0.0.9.0 on Nov. 17th, 2021
* Add function of export structures string.
* Add function of import structures string.

### 0.0.8.0 on Nov. 11th, 2021
* Add function of orientation for structures.
* Add function of undo and redo.

### 0.0.7.0 on Nov. 11th, 2021
* Add function of delete residue or structure.

### 0.0.6.0 on Nov. 5th, 2021
* Add function of template.
* Add function of cyclic.

### 0.0.5.0 on Oct. 28th, 2021
* Add function of repetation.

### 0.0.4.0 on Oct. 22th, 2021
* Add function of fragment structure.

### 0.0.3.0 on Oct. 20th, 2021
* Add adding residue menubar.
* Add function of searching about a structure using drawn structure.

### 0.0.2.0 on Sep. 7th, 2021
* Add toolbar of residues.
* Add a function of linkage toolbar.
* Add a function to paint rectangle to indicate a selecting monosaccharide.

### 0.0.1.0 on Aug. 4th, 2021
* Add a function of drawing a residue.
* Add a function of selecting a residue and linkage.
* Add a function of rectangle selection for residues and linkages.
