package org.glycoinfo.vaadin.glycanbuilder2web;

import org.glycoinfo.vaadin.glycanbuilder2web.canvas.GlycanWebCanvas;
import org.glycoinfo.vaadin.glycanbuilder2web.canvas.GlycanWebCanvasComponent;
import org.glycoinfo.vaadin.glycanbuilder2web.menubar.GlycanMenuBar;
import org.glycoinfo.vaadin.glycanbuilder2web.toolbar.GlycanInformationSearchbar;
import org.glycoinfo.vaadin.glycanbuilder2web.toolbar.LinkageToolbar;
import org.glycoinfo.vaadin.glycanbuilder2web.toolbar.ResiduesToolbar;
import org.glycoinfo.vaadin.glycanbuilder2web.toolbar.StructureToolbar;

import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.littemplate.LitTemplate;
import com.vaadin.flow.component.orderedlayout.Scroller;
import com.vaadin.flow.component.page.Page;
import com.vaadin.flow.component.template.Id;
import com.vaadin.flow.router.Route;

/**
 * Route component of GlycanBuilder2Web.
 */
@Route
@Tag("glycan-builder2-web")
@JsModule("./src/glycan-builder2-web.ts")
@CssImport("./styles/styles.css")
@SuppressWarnings("serial")
public class MainView extends LitTemplate {
    /**
     * Width of menu and toolbar.
     */
    private final int MENU_AND_TOOLBAR_WIDTH = 168;

    /**
     * Height of menu and toolbar.
     */
    private final int MENU_AND_TOOLBAR_HEIGHT = 137;

    /**
     * Menu bar for Glycan Builder 2.
     */
    @Id("glycanMenubar")
    private GlycanMenuBar glycanMenuBar;

    /**
     * Residues toolbar that selects a drawing residue.
     */
    @Id("residuesToolbar")
    private ResiduesToolbar residuesToolbar;

    /**
     * Structure toolbar to manipulate residues. 
     */
    @Id("structureToolbar")
    private StructureToolbar structureToolbar;

    /**
     * Linkage toolbar to edit the bonding information.
     */
    @Id("linkageToolbar")
    private LinkageToolbar linkageToolbar;

    /**
     * Glycan information searchbar.
     */
    @Id("glycanInformationSearchbar")
    private GlycanInformationSearchbar glycanInformationSearchbar;

    /**
     * Component that has GlycanWebCanvas to draw residues and linkages.
     */
    @Id("glycanWebCanvasComponent")
    private GlycanWebCanvasComponent glycanWebCanvasComponent;

    /**
     * Constructs GlycanBuilser2Web.
     */
    public MainView() {
        // Creates glycan menu bar to call some functions.
        glycanMenuBar.createGlycanMenuBar(glycanWebCanvasComponent.getGlycanWebCanvas());
        glycanWebCanvasComponent.getGlycanWebCanvas().addNotationChangeListener(glycanMenuBar.getStructureMenu());

        // Creates residues toolbar to select a drawing residue on the specified canvas.
        residuesToolbar.createResiduesToolbar(glycanWebCanvasComponent.getGlycanWebCanvas());
        glycanWebCanvasComponent.getGlycanWebCanvas().addNotationChangeListener(residuesToolbar);

        // Adds click listener for structure toolbar.
        structureToolbar.addClickListener(glycanWebCanvasComponent.getGlycanWebCanvas(), glycanMenuBar.getViewMenu());

        // Adds value change listener for linkage toolbar.
        linkageToolbar.addValueChangeListener(glycanWebCanvasComponent.getGlycanWebCanvas());

        // Sets a structure toolbar instance.
        glycanWebCanvasComponent.getGlycanWebCanvas().setStructureToolbar(structureToolbar);

        // Sets a linkage toolbar instance.
        glycanWebCanvasComponent.getGlycanWebCanvas().setLinkageToolbar(linkageToolbar);

        // Sets MainView insetance.
        glycanWebCanvasComponent.getGlycanWebCanvas().setGlycanWebCanvasScroller(glycanWebCanvasComponent.getGlycanWebCanvasScroller());

        // Adds click listener for searching glycan information.
        glycanInformationSearchbar.addClickListener(glycanWebCanvasComponent.getGlycanWebCanvas());

        // Sets size of canvas that is drawn residues and linkages.
        Page page = UI.getCurrent().getPage();
        page.retrieveExtendedClientDetails(details -> {
            setCanvasSize(details.getWindowInnerWidth(), details.getWindowInnerHeight());
        });

        // Sets size of canvas when resizing window.
        page.addBrowserWindowResizeListener(event -> {
            setCanvasSize(event.getWidth(), event.getHeight());
            glycanWebCanvasComponent.getGlycanWebCanvas().updateCanvas();
        });
    }

    /**
     * Sets size of canvas.
     * @param windowWidth Width of window.
     * @param windowHeight Height of window.
     */
    public void setCanvasSize(int windowWidth, int windowHeight) {
        Scroller glycanWebCanvasScroller = glycanWebCanvasComponent.getGlycanWebCanvasScroller();
        GlycanWebCanvas glycanWebCanvas = glycanWebCanvasComponent.getGlycanWebCanvas();
        int width = windowWidth - MENU_AND_TOOLBAR_WIDTH;
        int height = windowHeight - MENU_AND_TOOLBAR_HEIGHT;
        glycanWebCanvasScroller.getStyle().set("width", String.valueOf(width) + "px");
        glycanWebCanvasScroller.getStyle().set("height", String.valueOf(height) + "px");
        glycanWebCanvas.setIsFullSize(true);
        glycanWebCanvas.setSize(width, height - glycanWebCanvas.CANVAS_HEIGHT_COMPLETION_VALUE);
    }
}
