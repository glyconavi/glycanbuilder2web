package org.glycoinfo.vaadin.glycanbuilder2web.renderer;

import static org.eurocarbdb.application.glycanbuilder.renderutil.Geometry.isDown;
import static org.eurocarbdb.application.glycanbuilder.renderutil.Geometry.isLeft;
import static org.eurocarbdb.application.glycanbuilder.renderutil.Geometry.isUp;
import static org.eurocarbdb.application.glycanbuilder.renderutil.Geometry.midx;
import static org.eurocarbdb.application.glycanbuilder.renderutil.Geometry.midy;
import static org.eurocarbdb.application.glycanbuilder.renderutil.Geometry.textBounds;

import java.awt.Dimension;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;

import javax.swing.Icon;

import org.eurocarbdb.application.glycanbuilder.Residue;
import org.eurocarbdb.application.glycanbuilder.ResidueStyle;
import org.eurocarbdb.application.glycanbuilder.ResidueType;
import org.eurocarbdb.application.glycanbuilder.renderutil.AbstractResidueRenderer;
import org.eurocarbdb.application.glycanbuilder.renderutil.Geometry;
import org.eurocarbdb.application.glycanbuilder.renderutil.Paintable;
import org.eurocarbdb.application.glycanbuilder.renderutil.ResAngle;
import org.eurocarbdb.application.glycanbuilder.util.TextUtils;
import org.glycoinfo.application.glycanbuilder.util.GlycanUtils;
import org.glycoinfo.vaadin.WebCanvasRenderingContext2D;
import org.glycoinfo.vaadin.glycanbuilder2web.shapes.Bracket;
import org.glycoinfo.vaadin.glycanbuilder2web.shapes.Circle;
import org.glycoinfo.vaadin.glycanbuilder2web.shapes.Cyclic;
import org.glycoinfo.vaadin.glycanbuilder2web.shapes.Diamond;
import org.glycoinfo.vaadin.glycanbuilder2web.shapes.End;
import org.glycoinfo.vaadin.glycanbuilder2web.shapes.FlatDiamond;
import org.glycoinfo.vaadin.glycanbuilder2web.shapes.FlatHexagon;
import org.glycoinfo.vaadin.glycanbuilder2web.shapes.FlatSquare;
import org.glycoinfo.vaadin.glycanbuilder2web.shapes.HatDiamond;
import org.glycoinfo.vaadin.glycanbuilder2web.shapes.Heptagon;
import org.glycoinfo.vaadin.glycanbuilder2web.shapes.Hexagon;
import org.glycoinfo.vaadin.glycanbuilder2web.shapes.Pentagon;
import org.glycoinfo.vaadin.glycanbuilder2web.shapes.RHatDiamond;
import org.glycoinfo.vaadin.glycanbuilder2web.shapes.Rhombus;
import org.glycoinfo.vaadin.glycanbuilder2web.shapes.Square;
import org.glycoinfo.vaadin.glycanbuilder2web.shapes.SquareBracket;
import org.glycoinfo.vaadin.glycanbuilder2web.shapes.Star;
import org.glycoinfo.vaadin.glycanbuilder2web.shapes.Triangle;

/**
 * Renderer to draw a residue.
 */
public class GlycanWebResidueRenderer extends AbstractResidueRenderer {
    /**
     * Color of text notation.
     */
    private String colorOfTextNotation = null;

    /**
     * Background of text notation.
     */
    private String backgroundOfTextNotation = null;

    /**
     * Calls super class constructor.
     * @param glycanWebGlycanRenderer GlycanWebGlycanRenderer instance.
     */
    public GlycanWebResidueRenderer(GlycanWebGlycanRenderer glycanWebGlycanRenderer) {
        super(glycanWebGlycanRenderer);
    }

    /**
     * Draws a residue and its information.
     * @param paintable Paintable instance to draw a residue.
     * @param residue Residue that draws.
     * @param isSelected Whether this residue is selected.
     * @param isActive This argument is not used.
     * @param isOnBorder Whether text is drawn on border.
     * @param parentBoundingBox Parent bounding box for a drawn residue.
     * @param currentBoundingBox Bounding box that be drawn a residue.
     * @param supportBoundingBox Bounding box that supports space for the canvas.
     * @param residueOrientation Represents orientation of a residue drawn.
     */
    @Override
    public void paint(Paintable paintable, Residue residue, boolean isSelected, boolean isActive, boolean isOnBorder, Rectangle parentBoundingBox, Rectangle currentBoundingBox, Rectangle supportBoundingBox, ResAngle residueOrientation) {
        // Sets canvas for drawing the shape.
        WebCanvasRenderingContext2D renderer = (WebCanvasRenderingContext2D) paintable.getObject();
        renderer.save();
        renderer.setLineWidth(2d);

        // Gets coordinate and width of drawing the shape.
        double x = currentBoundingBox.x;
        double y = currentBoundingBox.y;
        double nodeSize = currentBoundingBox.width;

        // Gets coordinate of current.
        Point currentPoint = Geometry.center(currentBoundingBox);

        // Draws the shape according to the residue style.
        ResidueStyle residueStyle = theResidueStyleDictionary.getStyle(residue);
        boolean isPainted = paintShape(residueStyle, renderer, paintable, residue, isSelected, parentBoundingBox, currentBoundingBox, supportBoundingBox, residueOrientation, x, y, nodeSize);

        // Sets background color to an area of a residue.
        if (!isPainted) {
            renderer.save();
            if (backgroundOfTextNotation != null) {
                renderer.setFillStyle(backgroundOfTextNotation);
            } else {
                renderer.setFillStyle("#FFF");
            }
            renderer.fillRect(x, y, currentBoundingBox.width, currentBoundingBox.height);
            renderer.restore();
        }

        // Draws information of a residue.
        paintInformationOfResidue(isPainted, renderer, residueStyle, residue, isOnBorder, currentBoundingBox, currentPoint, nodeSize);

        // Draws anomer.
        paintAnomericState(isPainted, renderer, residue, residueOrientation, currentBoundingBox);

        // If residues are displayed text or be selected a substituent, draws square for representing the selected state.
        if (residueStyle.getShape() == null && isSelected) {
            renderer.save();
            renderer.setGlobalAlpha(0.4);
            new Square(x, y, currentBoundingBox.width, currentBoundingBox.height, residueStyle, renderer, isSelected).paint();
            renderer.restore();
        }
        renderer.restore();
    }

    /**
     * Draws the shape according to the residue style.
     * @param residueStyle Residue style to draw the shape.
     * @param renderer Renderer to draw the shape.
     * @param paintable Paintable instance to draw the shape.
     * @param residue Residue that draws.
     * @param isSelected Whether this residue is selected.
     * @param parentBoundingBox Parent bounding box for a drawn residue.
     * @param currentBoundingBox Bounding box that be drawn a residue.
     * @param supportBoundingBox Bounding box that supports space for the canvas.
     * @param residueOrientation Represents orientation of a residue drawn.
     * @param x X coordinate of drawing the shape.
     * @param y Y coordinate of drawing the shape.
     * @param nodeSize Width of drawing the shape.
     * @return Returns whether the shape is painted.
     */
    private boolean paintShape(ResidueStyle residueStyle, WebCanvasRenderingContext2D renderer, Paintable paintable, Residue residue, boolean isSelected, Rectangle parentBoundingBox, Rectangle currentBoundingBox, Rectangle supportBoundingBox, ResAngle residueOrientation, double x, double y, double nodeSize) {
        // If residue style doesn't have shape, ends the process.
        if (residueStyle.getShape() == null) {
            return false;
        }

        // Gets size of drawing the shape.
        double width = currentBoundingBox.width;
        double height = currentBoundingBox.height;

        // Gets coordinate of parent, support.
        Point parentPoint = (parentBoundingBox != null) ? Geometry.center(parentBoundingBox) : Geometry.center(currentBoundingBox);
        Point supportPoint = (supportBoundingBox != null) ? Geometry.center(supportBoundingBox) : Geometry.center(currentBoundingBox);

        // Draws the shape according to the residue style.
        switch (residueStyle.getShape()) {
            case "square":
                new Square(x, y, nodeSize, nodeSize, residueStyle, renderer, isSelected).paint();
                return true;
            case "flatsquare":
                new FlatSquare(x, y, width, height, residueStyle, renderer, isSelected).paint();
                return true;
            case "circle":
                new Circle(x, y, nodeSize, nodeSize, residueStyle, renderer, isSelected).paint();
                return true;
            case "triangle":
                new Triangle(Geometry.angle(parentPoint, supportPoint), x, y, nodeSize, nodeSize, residueStyle, renderer, isSelected).paint();
                return true;
            case "diamond":
                new Diamond(x, y, nodeSize, nodeSize, residueStyle, renderer, isSelected).paint();
                return true;
            case "flatdiamond":
                new FlatDiamond(x, y, width, height, residueStyle, renderer, isSelected).paint();
                return true;
            case "hexagon":
                new Hexagon(x, y, nodeSize, nodeSize, residueStyle, renderer, isSelected).paint();
                return true;
            case "heptagon":
                new Heptagon(x, y, nodeSize, nodeSize, residueStyle, renderer, isSelected).paint();
                return true;
            case "flathexagon":
                new FlatHexagon(x, y, width, height, residueStyle, renderer, isSelected).paint();
                return true;
            case "star":
                new Star(x, y, nodeSize, nodeSize, residueStyle, renderer, 5, isSelected).paint();
                return true;
            case "sixstar":
                new Star(x, y, nodeSize, nodeSize, residueStyle, renderer, 6, isSelected).paint();
                return true;
            case "sevenstar":
                new Star(x, y, nodeSize, nodeSize, residueStyle, renderer, 7, isSelected).paint();
                return true;
            case "pentagon":
                new Pentagon(x, y, nodeSize, nodeSize, residueStyle, renderer, isSelected).paint();
                return true;
            case "hatdiamond":
                new HatDiamond(x, y, nodeSize, nodeSize, residueStyle, renderer, isSelected).paint();
                return true;
            case "rhatdiamond":
                new RHatDiamond(x, y, nodeSize, nodeSize, residueStyle, renderer, isSelected).paint();
                return true;
            case "rhombus":
                new Rhombus(x, y, nodeSize, nodeSize, residueStyle, renderer, isSelected).paint();
                return true;
            case "end":
                new End(Geometry.angle(parentPoint, supportPoint), x, y, nodeSize, nodeSize, residueStyle, colorOfTextNotation, renderer, isSelected).paint();
                return true;
            case "bracket":
                new Bracket(residueOrientation.getAngle(), x, y, width, height, residueStyle, renderer, isSelected).paint();
                return true;
            case "startrep":
                new SquareBracket(residueOrientation.opposite().getAngle(), x, y, width, height, residueStyle, renderer, isSelected).paint();
                return true;
            case "endrep":
                new SquareBracket(residueOrientation.getAngle(), x, y, width, height, residueStyle, renderer, isSelected).paint();
                paintRepetitionText(renderer, residueStyle, residue, parentBoundingBox, currentBoundingBox, supportBoundingBox, residueOrientation);
                return true;
            case "startcyclic":
                new Cyclic(residueOrientation, x, y, width, height, residueStyle, renderer, isSelected).paint();
                return true;
            case "endcyclic":
                new Cyclic(residueOrientation.opposite(), x, y, width, height, residueStyle, renderer, isSelected).paint();
                return true;
            default:
                return false;
        }
    }

    /**
     * Draws repetition text.
     * @param residueStyle Residue style to draw the residue.
     * @param renderer Renderer to draw the shape.
     * @param residue Residue that draws.
     * @param parentBoundingBox Parent bounding box for a drawn residue.
     * @param currentBoundingBox Bounding box that be drawn a residue.
     * @param supportBoundingBox Bounding box that supports space for the canvas.
     * @param residueOrientation Represents orientation of a residue drawn.
     */
    private void paintRepetitionText(WebCanvasRenderingContext2D renderer, ResidueStyle residueStyle, Residue residue, Rectangle parentBoundingBox, Rectangle currentBoundingBox, Rectangle supportBoundingBox, ResAngle residueOrientation) {
        // Gets size of drawing repetition text.
        double x = (double) currentBoundingBox.getX();
        double y = (double) currentBoundingBox.getY();
        double width = (double) currentBoundingBox.getWidth();
        double height = (double) currentBoundingBox.getHeight();

        // Gets radius, center x and y.
        double radius = Math.min(width, height);
        double centerX = x + width / 2d;
        double centerY = y + height / 2d;

        // Calculate angle. x2, y2, x3 and y3.
        double angle = residueOrientation.getAngle();
        double x2 = centerX + radius * Math.cos(angle - Math.PI / 2d);
        double y2 = centerY + radius * Math.sin(angle - Math.PI / 2d);
        double x3 = centerX + radius * Math.cos(angle + Math.PI / 2d);
        double y3 = centerY + radius * Math.sin(angle + Math.PI / 2d);

        // Gets min and max repetition.
        int min = residue.getMinRepetitions();
        int max = residue.getMaxRepetitions();
        if(min == max && (min != -1 && max != -1)) min = -1;
        
        // Draws repetition min text.
        if (min >= 0 || max >= 0 || (min == -1 && max == -1)) {
            // Calculate min x and y.
            String repetitionText = (min >= 0) ? String.valueOf(min) :"n";
            Dimension textBounds = textBounds(repetitionText, theGraphicOptions.LINKAGE_INFO_FONT_FACE, theGraphicOptions.LINKAGE_INFO_SIZE);
            double distance = (isUp(angle) || isDown(angle)) ? (textBounds.width / 2) + 4 : (textBounds.height / 2) + 4;
            double minX;
            double minY;
            if (isLeft(angle) || isUp(angle)) {
                minX = x2 + distance * Math.cos(angle - Math.PI / 2d) - textBounds.width / 2d;
                minY = y2 + distance * Math.sin(angle - Math.PI / 2d) + textBounds.height / 2d;
            } else {
                minX = x3 + distance * Math.cos(angle + Math.PI / 2d) - textBounds.width / 2d;
                minY = y3 + distance * Math.sin(angle + Math.PI / 2d) + textBounds.height / 2d;
            }

            // Draws min text.
            renderer.save();
            renderer.setFillStyle("black");
            renderer.setTextAlign("center");
            renderer.setFont("12pt Calibri");
            renderer.fillText(repetitionText, minX, minY, theGraphicOptions.LINKAGE_INFO_SIZE);
            renderer.restore();
        }

        // Draws repetition max text.
        if (min >= 0 || max >= 0) {
            // Calculate max x and y.
            String repetitionText = (max >= 0) ? String.valueOf(max) : "n";
            Dimension textBounds = textBounds(repetitionText, theGraphicOptions.LINKAGE_INFO_FONT_FACE, theGraphicOptions.LINKAGE_INFO_SIZE);
            double distance = (isUp(angle) || isDown(angle)) ? (textBounds.width / 2) + 4 : (textBounds.height) / 2 + 4;
            double maxX;
            double maxY;
            if (isLeft(angle) || isUp(angle)) {
                maxX = x3 + distance * Math.cos(angle+Math.PI / 2d) - textBounds.width / 2d;
                maxY = y3 + distance * Math.sin(angle+Math.PI / 2d) + textBounds.height / 2d;
            }
            else {
                maxX = x2 + distance * Math.cos(angle - Math.PI / 2d) - textBounds.width / 2d;
                maxY = y2 + distance * Math.sin(angle - Math.PI / 2d) + textBounds.height / 2d;
            }

            // Draws max text.
            renderer.save();
            renderer.setFillStyle("black");
            renderer.setTextAlign("center");
            renderer.setFont("12pt Calibri");
            renderer.fillText(repetitionText, maxX, maxY, theGraphicOptions.LINKAGE_INFO_SIZE);
            renderer.restore();
        }
    }

    /**
     * Draws information of a residue.
     * @param isPainted Whether the residue is painted.
     * @param renderer Renderer to draw to the residue.
     * @param residueStyle Residue style to draw the residue.
     * @param residue Residue that draws.
     * @param isOnBorder Whether a text is drawn on border.
     * @param currentBoundingBox Bounding box that be drawn a residue.
     * @param currentPoint Coordinate of current.
     * @param nodeSize Width of drawing the shape.
     * @param residueOrientation Represents orientation of a residue drawn.
     */
    private void paintInformationOfResidue(boolean isPainted, WebCanvasRenderingContext2D renderer, ResidueStyle residueStyle, Residue residue, boolean isOnBorder, Rectangle currentBoundingBox, Point currentPoint, double nodeSize) {
        // Gets whether notation is SNFG.
        boolean isSNFG = !checkComposiiton(residue).isEmpty();

        // Draws chirarity and ring size.
        if (!isPainted || residueStyle.getText() != null || isSNFG) {
            // Gets information of chirarity and ring size.
            String text = getText(residue, isOnBorder);
            if (isSNFG) {
                ArrayList<String> configurations = checkComposiiton(residue);
                if (isPainted) {
                    if (!configurations.isEmpty()) text = "";
                    for (String configuration : configurations) {
                        text += configuration;
                    }
                } else {
                    for (String configuration : configurations) {
                        if (configuration.equals("p") || configuration.equals("f")) text = text + configuration;
                        if(configuration.equals("D") || configuration.equals("L")) text = configuration + text;
                    }
                }
            }

            // Draws the text.
            if (text != null) {
                // Sets renderer for drawing the text.
                renderer.save();
                if (colorOfTextNotation != null) {
                    renderer.setFillStyle(colorOfTextNotation);
                } else {
                    renderer.setFillStyle("black");
                }
                renderer.setTextAlign("center");
                renderer.setTextBaseline("middle");

                // If draws a substituent, increase the font size.
                if (residueStyle.getShape() == null) {
                    int nodeFontSize = theGraphicOptions.NODE_FONT_SIZE;
                    int nodeFontWidth = textBounds(text, theGraphicOptions.NODE_FONT_FACE, nodeFontSize).width;
                    nodeFontSize = sat(8 * nodeFontSize * currentBoundingBox.width / nodeFontWidth / 10, nodeFontSize) + 4;
                    renderer.setFont(nodeFontSize + "pt Calibri");
                } else {
                    int insideFontSize = theGraphicOptions.NODE_FONT_SIZE - 2;
                    renderer.setFont(insideFontSize + "pt Calibri");
                }

                // Draws a text either horizontally or vertically according to residue angle.
                // First block draws it horizontally.
                ResAngle residueAngle = theGraphicOptions.getOrientationAngle();
                if (residueAngle.getIntAngle() == 0 || residueAngle.getIntAngle() == 180) {
                    renderer.rotate(0d);
                    renderer.fillText(text, (double) currentPoint.x, (double) currentPoint.y + 3, (double) nodeSize);
                } else {
                    renderer.translate(currentPoint.x + 3d, currentPoint.y);
                    double rotateValue = isSNFG ? 0d : -Math.PI / 2.0;
                    renderer.rotate(rotateValue);
                    renderer.fillText(text, 0d, 0d, currentBoundingBox.height);
                }
                renderer.restore();
            }
        }
    }

    /**
     * Draws anomeric state when don't show reducing end.
     * @param isPainted Whether the residue is painted.
     * @param renderer Renderer to draw to the residue.
     * @param residue Residue that draws.
     * @param residueOrientation Represents orientation of a residue drawn.
     * @param currentBoundingBox Bounding box that be drawn a residue.
     */
    private void paintAnomericState(boolean isPainted, WebCanvasRenderingContext2D renderer, Residue residue, ResAngle residueOrientation, Rectangle currentBoundingBox) {
        // If residue isn't bracket, ends the process.
        boolean shouldShowAnomericState = false;
        if (residue.isSaccharide()) shouldShowAnomericState = GlycanUtils.isFacingAnom(residue);
        if (!theGraphicOptions.SHOW_REDEND_CANVAS && residue.isSaccharide()) {
            String viewType = theGraphicOptions.DISPLAY;
            shouldShowAnomericState = (residue.getTreeRoot().firstChild().equals(residue) && !residue.isAlditol() && !residue.getTreeRoot().isBracket() && viewType == "normalinfo");
        }
        if (!isPainted || !shouldShowAnomericState) return;

        // Draws anomeric state.
        renderer.setFont("9pt Calibri");
        renderer.setFillStyle("black");
        renderer.setTextAlign("left");
        String anomericState = TextUtils.toGreek(residue.getAnomericState());
        double scale = 0.4;
        double width = renderer.calculateTextWidth(anomericState, scale);
        Dimension textBounds = new Dimension((int) width, (int) (12d * scale));
        if(residueOrientation.equals(0) || residueOrientation.equals(180)) {
            int margin = (residueOrientation.equals(0)) ? -15 : 15;
            Rectangle2D.Double txtRect = new Rectangle2D.Double(midx(currentBoundingBox) - textBounds.width / 2, midy(currentBoundingBox) - textBounds.height / 2, textBounds.width, textBounds.height);
            renderer.fillText(anomericState, txtRect.x + (double) margin, txtRect.y + txtRect.height);
        } else {
            int margin = (residueOrientation.equals(90)) ? -15 : 15;
            Rectangle2D.Double txtRect = new Rectangle2D.Double(midx(currentBoundingBox) - textBounds.height / 2, midy(currentBoundingBox) - textBounds.width / 2, textBounds.height, textBounds.width);
            renderer.rotate(-Math.PI / 2.0);
            renderer.fillText(anomericState, - (txtRect.y+txtRect.height + (double) margin), txtRect.x + txtRect.width);
            renderer.rotate(+Math.PI / 2.0);
        }
    }

    @Override
    public Icon getIcon(ResidueType type, int max_y_size) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Image getImage(ResidueType type, int max_y_size) {
        // TODO Auto-generated method stub
        return null;
    }

    /**
     * Sets stroke style of text notation.
     * @param color Color of stroke style.
     */
    public void setColorOfTextNotation(String color) {
        colorOfTextNotation = color;
    }

    /**
     * Sets stroke style of text notation.
     * @param color Color of fill style.
     */
    public void setBackgroundOfTextNotation(String color) {
        backgroundOfTextNotation = color;
    }
}
