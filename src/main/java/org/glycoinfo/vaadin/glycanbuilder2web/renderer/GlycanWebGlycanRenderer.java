package org.glycoinfo.vaadin.glycanbuilder2web.renderer;

import static org.eurocarbdb.application.glycanbuilder.renderutil.Geometry.bottom;
import static org.eurocarbdb.application.glycanbuilder.renderutil.Geometry.left;
import static org.eurocarbdb.application.glycanbuilder.renderutil.Geometry.midx;
import static org.eurocarbdb.application.glycanbuilder.renderutil.Geometry.midy;
import static org.eurocarbdb.application.glycanbuilder.renderutil.Geometry.right;
import static org.eurocarbdb.application.glycanbuilder.renderutil.Geometry.textBounds;
import static org.eurocarbdb.application.glycanbuilder.renderutil.Geometry.top;

import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.util.Collection;

import org.eurocarbdb.application.glycanbuilder.Glycan;
import org.eurocarbdb.application.glycanbuilder.Residue;
import org.eurocarbdb.application.glycanbuilder.renderutil.AbstractGlycanRenderer;
import org.eurocarbdb.application.glycanbuilder.renderutil.BBoxManager;
import org.eurocarbdb.application.glycanbuilder.renderutil.Geometry;
import org.eurocarbdb.application.glycanbuilder.renderutil.Paintable;
import org.eurocarbdb.application.glycanbuilder.renderutil.PositionManager;
import org.eurocarbdb.application.glycanbuilder.renderutil.ResAngle;
import org.glycoinfo.vaadin.WebCanvasRenderingContext2D;

/**
 * Renderer to draw information for residues.
 */
public class GlycanWebGlycanRenderer extends AbstractGlycanRenderer {
    /**
     * Registers residue and linkage renderer.
     */
    @Override
    protected void initialiseRenderers() {
        theResidueRenderer = new GlycanWebResidueRenderer(this);
        theLinkageRenderer = new GlycanWebLinkageRenerer(this);
    }

    /**
     * Displays a mass text according to the bounding box manager.
     * @param paintable Paintable interface instance.
     * @param structure Glycan instance.
     * @param haveReducingEnd Whether the glycan have reducing end.
     * @param boundingBoxManager Bounding box manager instance.
     */
    @Override
    protected void displayMass(Paintable paintable, Glycan structure, boolean haveReducingEnd, BBoxManager boundingBoxManager) {
        // If structure doesn't have monosaccharide of root the process ends.
        if (structure.getRoot(haveReducingEnd) == null) return;

        // Sets drawing texts.
        WebCanvasRenderingContext2D renderer = (WebCanvasRenderingContext2D) paintable.getObject();
        renderer.setFont((theGraphicOptions.MASS_TEXT_SIZE / 1.33f) + "pt Calibri");
        renderer.setFillStyle("black");
        renderer.setTextAlign("left");

        // Draws a mass text according to the bounding box manager.
        Rectangle structuresBoundingBox = boundingBoxManager.getComplete(structure.getRoot(haveReducingEnd));
        renderer.fillText(getMassText(structure), Double.valueOf(Geometry.left(structuresBoundingBox)), Double.valueOf(Geometry.bottom(structuresBoundingBox) + theGraphicOptions.MASS_TEXT_SPACE + theGraphicOptions.MASS_TEXT_SIZE));
    }

    /**
     * Draws quantity text.
     * @param paintable Paintable interface instance.
     * @param antenna Fragmented residue.
     * @param quantity Quantity of fragmented residues.
     * @param boundingBoxManager Bounding box manager instance.
     */
    @Override
    protected void paintQuantity(Paintable paintable, Residue antenna, int quantity, BBoxManager boundingBoxManager) {
        // Creates quantity Text.
        ResAngle residueOrientation = theGraphicOptions.getOrientationAngle();
        String quantityText;
        if (residueOrientation.equals(0) || residueOrientation.equals(-90)) {
            quantityText = "x" + quantity;
        } else {
            quantityText = quantity + "x";
        }

        // Created text rectangle for space drawn quantity text.
        Dimension quantityTextDimension = textBounds(quantityText, theGraphicOptions.NODE_FONT_FACE, theGraphicOptions.NODE_FONT_SIZE);
        Rectangle antennaBoundingBox = boundingBoxManager.getComplete(antenna);
        Rectangle textRectangle;
        if (residueOrientation.equals(0)) {
            textRectangle = new Rectangle(right(antennaBoundingBox) + 3, midy(antennaBoundingBox) - 1 - quantityTextDimension.height / 2, quantityTextDimension.width, quantityTextDimension.height);
        } else if (residueOrientation.equals(180)) {
            textRectangle = new Rectangle(left(antennaBoundingBox) - 3 - quantityTextDimension.width, midy(antennaBoundingBox) - 1 - quantityTextDimension.height / 2, quantityTextDimension.width, quantityTextDimension.height);
        } else if (residueOrientation.equals(90)) {
            textRectangle = new Rectangle(midx(antennaBoundingBox) - quantityTextDimension.height / 2, bottom(antennaBoundingBox) + 3, quantityTextDimension.height, quantityTextDimension.width);
        } else {
            textRectangle = new Rectangle(midx(antennaBoundingBox) - quantityTextDimension.height / 2, top(antennaBoundingBox) - 3 - quantityTextDimension.width, quantityTextDimension.height, quantityTextDimension.width);
        }

        // Sets renderer for drawing the text.
        WebCanvasRenderingContext2D renderer = (WebCanvasRenderingContext2D) paintable.getObject();
        renderer.save();
        renderer.setFillStyle("black");
        renderer.setFont("12pt Calibri");

        // Draws quantity text.
        if (residueOrientation.equals(0) || residueOrientation.equals(180))
            renderer.fillText(quantityText, left(textRectangle), bottom(textRectangle));
        else {
            renderer.rotate(-Math.PI / 2.0);
            renderer.fillText(quantityText, -bottom(textRectangle), right(textRectangle));
            renderer.rotate(+Math.PI / 2.0);
        }
    }

    @Override
    public BufferedImage getImage(Collection<Glycan> structures, boolean opaque, boolean show_masses, boolean show_redend, double scale, PositionManager posManager, BBoxManager bboxManager) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    protected void assignID(Glycan arg0) {
        // TODO Auto-generated method stub
        
    }

    @Override
    protected void displayLegend(Paintable arg0, Glycan arg1, boolean arg2, BBoxManager arg3) {
        // TODO Auto-generated method stub
        
    }
}
