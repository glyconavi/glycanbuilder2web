package org.glycoinfo.vaadin.glycanbuilder2web.renderer;

import static org.eurocarbdb.application.glycanbuilder.renderutil.Geometry.textBounds;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;

import org.eurocarbdb.application.glycanbuilder.Residue;
import org.eurocarbdb.application.glycanbuilder.linkage.Linkage;
import org.eurocarbdb.application.glycanbuilder.linkage.LinkageStyle;
import org.eurocarbdb.application.glycanbuilder.renderutil.AbstractLinkageRenderer;
import org.eurocarbdb.application.glycanbuilder.renderutil.Geometry;
import org.eurocarbdb.application.glycanbuilder.renderutil.Paintable;
import org.eurocarbdb.application.glycanbuilder.util.TextUtils;
import org.glycoinfo.vaadin.WebCanvasRenderingContext2D;
import org.glycoinfo.vaadin.glycanbuilder2web.shapes.BaseShape;

/**
 * Renderer to draw linkage for residues.
 */
public class GlycanWebLinkageRenerer extends AbstractLinkageRenderer {
    /**
     * Calls super class constructor.
     * @param glycanWebGlycanRenderer GlycanWebGlycanRenderer instance.
     */
    public GlycanWebLinkageRenerer(GlycanWebGlycanRenderer glycanWebGlycanRenderer) {
        super(glycanWebGlycanRenderer);
    }

    /**
     * Draws an edge according to the bounding box manager.
     * @param paintable Paintable interface instance.
     * @param linkage Linkage to bind residues.
     * @param isSelected Whether the linkage is selected.
     * @param parentBoundingBox Parent bounding box for a related linkage to residue.
     * @param parentBorderBoundingBox Parent border bounding box for a related linkage to residue.
     * @param childBoundingBox Child bounding box for a related linkage to residue.
     * @param childBorderBoundingBox Child border bounding box for a related linkage to residue.
     */
    @Override
    public void paintEdge(Paintable paintable, Linkage linkage, boolean isSelected, Rectangle parentBoundingBox, Rectangle parentBorderBoundingBox, Rectangle childBoundingBox, Rectangle childBorderBoundingBox) {
        // If the linkage is not specified, ends the process.
        if (linkage == null) return;

        // If the linkage is selected, draws thick.
        WebCanvasRenderingContext2D renderer = (WebCanvasRenderingContext2D) paintable.getObject();
        if (isSelected) {
            renderer.setLineWidth(3d);
        } else {
            renderer.setLineWidth(2d);
        }

        // Draws linkage.
        createShape(paintable,linkage, parentBoundingBox, childBoundingBox);

        // Draws linkage information.
        if(theGraphicOptions.SHOW_INFO){
            paintInfo(paintable,linkage,parentBoundingBox,parentBorderBoundingBox,childBoundingBox,childBorderBoundingBox);
        }
    }

    /**
     * Draws linkage information.
     * @param paintable Paintable interface instance.
     * @param linkage Linkage to bind residues.
     * @param parentBoundingBox Parent bounding box for a related linkage to residue.
     * @param parentBorderBoundingBox Parent border bounding box for a related linkage to residue.
     * @param childBoundingBox Child bounding box for a related linkage to residue.
     * @param childBorderBoundingBox Child border bounding box for a related linkage to residue.
     */
    @Override
    public void paintInfo(Paintable paintable, Linkage linkage, Rectangle parentBoundingBox, Rectangle parentBorderBoundingBox, Rectangle childBoundingBox, Rectangle childBorderBoundingBox) {
        // If the linkage is not specified or be set not shoing linkage information, ends the process.
        if (linkage == null || !theGraphicOptions.SHOW_INFO) return;

        // If parent linkage is shown, draws linkage information.
        LinkageStyle linkageStyle = theLinkageStyleDictionary.getStyle(linkage);
        Residue childResidue = linkage.getChildResidue();
        if (linkageStyle.showParentLinkage(linkage)) {
            paintInfo(paintable, linkage.getParentPositionsString(), parentBoundingBox, parentBorderBoundingBox, childBoundingBox, childBorderBoundingBox, true, false, linkage.hasMultipleBonds());
        }

        // If child residue is shown, draws anomeric carbon.
        if (linkageStyle.showAnomericCarbon(linkage)){
            paintInfo(paintable, linkage.getChildPositionsString(), parentBoundingBox, parentBorderBoundingBox, childBoundingBox, childBorderBoundingBox, false, true, linkage.hasMultipleBonds());
        }

        // If child residue is shown, draws anomeric state.
        if (linkageStyle.showAnomericState(linkage, childResidue.getAnomericState())){
            paintInfo(paintable, TextUtils.toGreek(childResidue.getAnomericState()), parentBoundingBox, parentBorderBoundingBox, childBoundingBox, childBorderBoundingBox, false, false, linkage.hasMultipleBonds());
        }
        
    }

    /**
     * Draws linkage information.
     * @param paintable Paintable interface instance.
     * @param linkageInformation Linkage information.
     * @param parentBoundingBox Parent bounding box for a related linkage to residue.
     * @param parentBorderBoundingBox Parent border bounding box for a related linkage to residue.
     * @param childBoundingBox Child bounding box for a related linkage to residue.
     * @param childBorderBoundingBox Child border bounding box for a related linkage to residue.
     * @param isToParent Whether the linkage is to parent residue.
     * @param isAbove Whether the linkage is above parent residue.
     * @param isMultiple Whether the linkage relates multiple residues.
     */
    @Override
    protected void paintInfo(Paintable paintable, String linkageInformation, Rectangle parentBoundingBox, Rectangle parentBorderBoundingBox, Rectangle childBoundingBox, Rectangle childBorderBoundingBox, boolean isToParent, boolean isAbove, boolean isMultiple) {
        WebCanvasRenderingContext2D renderer = (WebCanvasRenderingContext2D) paintable.getObject();
        try {
            // Calculates dimension and position.
            Dimension dimension = textBounds(linkageInformation, theGraphicOptions.LINKAGE_INFO_FONT_FACE, theGraphicOptions.LINKAGE_INFO_SIZE);
            Point position = computePosition(dimension, parentBoundingBox, parentBorderBoundingBox, childBoundingBox, childBorderBoundingBox, isToParent, isAbove, isMultiple);
 
            // Draws linkage information.
            renderer.setFont((theGraphicOptions.LINKAGE_INFO_SIZE  - (int) (3 * theGraphicOptions.SCALE_CANVAS)) + "pt Calibri");
            renderer.setFillStyle("black");
            renderer.setTextAlign("left");
            int correctionY = theGraphicOptions.SCALE_CANVAS >= 2.0 ? (int) (3 * theGraphicOptions.SCALE_CANVAS) : 0;
            renderer.fillText(linkageInformation, Double.valueOf(position.x), Double.valueOf(position.y) + correctionY);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Calculates position of linkage information.
     * @param textBounds Text of linkage information.
     * @param parentBoundingBox Parent bounding box for a related linkage to residue.
     * @param parentBorderBoundingBox Parent border bounding box for a related linkage to residue.
     * @param childBoundingBox Child bounding box for a related linkage to residue.
     * @param childBorderBoundingBox Child border bounding box for a related linkage to residue.
     * @param isToParent Whether the linkage is to parent residue.
     * @param isAbove Whether the linkage is above parent residue.
     * @param isMultiple Whether the linkage relates multiple residues.
     */
    @Override
    protected Point computePosition(Dimension textBounds, Rectangle parentBoundingBox, Rectangle parentBorderBoundingBox, Rectangle childBoundingBox, Rectangle childBorderBoundingBox, boolean isToParent, boolean isAbove, boolean isMultiple) {
        //
        Point parentCenterPosition = Geometry.center(parentBoundingBox);
        Point childCenterPosition = Geometry.center(childBoundingBox);
        double centerX = 0d;
        double centerY = 0d;
        double defaultRadius = 0.5 * 12d;
        double radius = 0d;
        double angle = 0d;
        if (isToParent) {
            centerX = parentCenterPosition.x;
            centerY = parentCenterPosition.y;
            angle = Geometry.angle(childCenterPosition, parentCenterPosition);
            radius = GlycanWebGeometry.getExclusionRadius(parentCenterPosition, angle, parentBorderBoundingBox) + 2;
        } else {
            centerX = childBoundingBox.x + childBoundingBox.width / 2;
            centerY = childBoundingBox.y + childBoundingBox.height / 2;
            angle = Geometry.angle(parentCenterPosition, childCenterPosition);
            radius = GlycanWebGeometry.getExclusionRadius(childCenterPosition, angle, childBorderBoundingBox) + 2;
        }

        //
        boolean isAboveTemp = isAbove;
        if(isToParent) isAboveTemp = !isAboveTemp;

        //
        double space = (isMultiple) ? 4d : 2d;
        double textX = 0d;
        double textY = 0d;
        if (isAboveTemp) {
            textX = centerX + (radius + defaultRadius) * Math.cos(angle) + (defaultRadius + space) * Math.cos(angle - Math.PI / 2d);
            textY = centerY + (radius + defaultRadius) * Math.sin(angle) + (defaultRadius + space) * Math.sin(angle - Math.PI / 2d);
        } else {
            textX = centerX + (radius + defaultRadius) * Math.cos(angle) + (defaultRadius + space) * Math.cos(angle + Math.PI / 2d);
            textY = centerY + (radius + defaultRadius) * Math.sin(angle) + (defaultRadius + space) * Math.sin(angle + Math.PI / 2d);
        }
        textX -= textBounds.getWidth() / 2;
        textY += textBounds.getHeight() / 2;
        return new Point((int) textX, (int) textY);
    }

    /**
     * Draws linkage.
     * @param paintable Paintable interface instance.
     * @param linkage Linkage to bind residues.
     * @param parentBoundingBox Parent bounding box for a related linkage to residue.
     * @param childBoundingBox Child bounding box for a related linkage to residue.
     */
    private void createShape(Paintable paintable,Linkage linkage, Rectangle parentBoundingBox, Rectangle childBoundingBox) {
        // Gets center position of parent and child bounding box.
        Point parentCenterPosition = Geometry.center(parentBoundingBox);
        Point childCenterPosition = Geometry.center(childBoundingBox);

        // Draws linkage.
        LinkageStyle linkageStyle = theLinkageStyleDictionary.getStyle(linkage);
        String shapeStyle = linkageStyle.getShape();
        if (shapeStyle.equals("line")) {
            paintLine(paintable, parentCenterPosition, childCenterPosition, linkage.hasMultipleBonds(), linkage);
        } else if (shapeStyle.equals("curve")) {
            paintCurve(paintable, parentCenterPosition, childCenterPosition, linkage.hasMultipleBonds(), linkage);
        } else {
            paintLine(paintable, parentCenterPosition, childCenterPosition, linkage);
        }
    }

    /**
     * Draws line.
     * @param paintable Paintable interface instance.
     * @param point1 Parent center position.
     * @param point2 Child center position.
     * @param isMultiple Whether the linkage relates multiple residues.
     * @param linkage Linkage to bind residues.
     */
    private void paintLine(Paintable paintable, Point point1, Point point2, boolean isMultiple, Linkage linkage) {
        if (isMultiple) {
            double angle = Geometry.angle(point1, point2);
            paintLine(paintable, Geometry.translate(point1, 2d * Math.cos(angle + Math.PI / 2), 2d * Math.sin(angle + Math.PI / 2)), Geometry.translate(point2, 2d * Math.cos(angle + Math.PI / 2), 2d * Math.sin(angle + Math.PI / 2)), false, linkage);
            paintLine(paintable, Geometry.translate(point1, 2d * Math.cos(angle - Math.PI / 2), 2d * Math.sin(angle - Math.PI / 2)), Geometry.translate(point2, 2d * Math.cos(angle - Math.PI / 2), 2d * Math.sin(angle - Math.PI / 2)), false, linkage);
        } else {
            paintLine(paintable, point1, point2,linkage);
        }
    }

    /**
     * Draws line.
     * @param paintable Paintable interface instance.
     * @param point1 Parent center position.
     * @param point2 Child center position.
     * @param linkage Linkage to bind residues.
     */
    private void paintLine(Paintable paintable, Point point1, Point point2, Linkage linkage) {
        // Draws line according to linkage style.
        WebCanvasRenderingContext2D renderer = (WebCanvasRenderingContext2D) paintable.getObject();
        renderer.beginPath();
        LinkageStyle linkageStyle = theLinkageStyleDictionary.getStyle(linkage);
        if (linkageStyle.isDashed()) {
            BaseShape.paintDashedLine(renderer, point1.x, point1.y, point2.x, point2.y);
        } else {
            renderer.moveTo(point1.x, point1.y);
            renderer.lineTo(point2.x, point2.y);
        }
        renderer.closePath();

        // Draws line.
        setStrokeColor(paintable, Color.BLACK);
        renderer.stroke();
    }

    /**
     * Draws curve.
     * @param paintable Paintable interface instance.
     * @param point1 Parent center position.
     * @param point2 Child center position.
     * @param isMultiple Whether the linkage relates multiple residues.
     * @param linkage Linkage to bind residues.
     */
    private void paintCurve(Paintable paintable, Point point1, Point point2, boolean isMultiple, Linkage linkage) {
        if (isMultiple) {
            double angle = Geometry.angle(point1, point2);
            paintCurve(paintable, Geometry.translate(point1, 2. * Math.cos(angle + Math.PI / 2), 2. * Math.sin(angle + Math.PI / 2)), Geometry.translate(point2, 2. * Math.cos(angle + Math.PI / 2), 2. * Math.sin(angle + Math.PI / 2)), false, linkage);
            paintCurve(paintable, Geometry.translate(point1, 2. * Math.cos(angle - Math.PI / 2), 2. * Math.sin(angle - Math.PI / 2)), Geometry.translate(point2, 2. * Math.cos(angle - Math.PI / 2), 2. * Math.sin(angle - Math.PI / 2)), false, linkage);
        }
        createCurve(paintable, point1, point2);
    }

    /**
     * Draws curve.
     * @param paintable Paintable interface instance.
     * @param point1 Parent center position.
     * @param point2 Child center position.
     */
    private void createCurve(Paintable paintable, Point point1, Point point2) {
        //
        double centerX = (point1.x + point2.x) / 2d;
        double centerY = (point1.y + point2.y) / 2d;
        double radius = Geometry.distance(point1, point2) / 2d;
        double angle = Geometry.angle(point1, point2);

        // start point
        double x1 = centerX + radius * Math.cos(angle);
        double y1 = centerY + radius * Math.sin(angle);

        // end point
        double x2 = centerX + radius * Math.cos(angle + Math.PI);
        double y2 = centerY + radius * Math.sin(angle + Math.PI);

        // ctrl point 1
        double cx1 = centerX + 0.1 * radius * Math.cos(angle);
        double cy1 = centerY + 0.1 * radius * Math.sin(angle);
        double tx1 = cx1 + radius * Math.cos(angle + Math.PI / 2d);
        double ty1 = cy1 + radius * Math.sin(angle + Math.PI / 2d);

        // ctrl point 2
        double cx2 = centerX + 0.1 * radius * Math.cos(angle + Math.PI);
        double cy2 = centerY + 0.1 * radius * Math.sin(angle + Math.PI);
        double tx2 = cx2 + radius * Math.cos(angle - Math.PI / 2d);
        double ty2 = cy2 + radius * Math.sin(angle - Math.PI / 2d);
        
        //Bit of a guess at this point
        WebCanvasRenderingContext2D canvas = (WebCanvasRenderingContext2D) paintable.getObject();
        
        canvas.beginPath();
        canvas.moveTo(x1, y1);
        canvas.bezierCurveTo(tx1, ty1, tx2, ty2, x2, y2);
        setStrokeColor(paintable,Color.BLACK);
        canvas.stroke();
        canvas.closePath();
    }

    /**
     * Sets stroke color.
     * @param paintable Paintable interface instance.
     * @param color Color to set to stroke.
     */
    final protected void setStrokeColor(Paintable paintable,Color color){
        WebCanvasRenderingContext2D canvas = (WebCanvasRenderingContext2D) paintable.getObject();
        canvas.setStrokeStyle(color);
    }
}
