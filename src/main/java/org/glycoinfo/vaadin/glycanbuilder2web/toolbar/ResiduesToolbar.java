package org.glycoinfo.vaadin.glycanbuilder2web.toolbar;

import org.eurocarbdb.application.glycanbuilder.ResidueType;
import org.eurocarbdb.application.glycanbuilder.dataset.ResidueDictionary;
import org.glycoinfo.vaadin.glycanbuilder2web.canvas.GlycanWebCanvas;
import org.glycoinfo.vaadin.glycanbuilder2web.canvas.NotationChangeListener;
import org.glycoinfo.vaadin.glycanbuilder2web.canvas.ResidueIconCreator;
import org.glycoinfo.vaadin.glycanbuilder2web.menubar.StructureMenu;

import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.contextmenu.MenuItem;
import com.vaadin.flow.component.contextmenu.SubMenu;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.littemplate.LitTemplate;
import com.vaadin.flow.component.menubar.MenuBar;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.template.Id;

/**
 * Residues toolbar that selects a drawing residue.
 */
@Tag("residues-toolbar")
@JsModule("./src/residues-toolbar.ts")
@SuppressWarnings("serial")
public class ResiduesToolbar extends LitTemplate implements NotationChangeListener {
    /**
     * Layout for buttons that has an event of a drawing residue.
     */
    @Id("residuesToolbarLayout")
    private HorizontalLayout residuesToolBarLayout;

    /**
     * Menu bar of all residue.
     */
    @Id("allResidueMenuBar")
    private MenuBar allResidueMenuBar;

    /**
     * Creates residues toolbar to select a drawing residue on the specified canvas.
     * @param glycanWebCanvas Canvas drawn residues and linkages.
     */
   public void createResiduesToolbar(GlycanWebCanvas glycanWebCanvas) {
        // Creates buttons to be able to draw a residue on the canvas.
        for (ResidueType residueType : ResidueDictionary.directResidues()) {
            // If residue type can't have parent, continues the process.
            if (!residueType.canHaveParent()) continue;

            // Creates a residue icon, then adds an event to draw a residue on the canvas to a button that has the residue icon.
            try {
                ResidueIconCreator residueIconCreator = new ResidueIconCreator();
                String notation = glycanWebCanvas.getBuilderWorkspace().getGraphicOptions().NOTATION;
                residueIconCreator.createResidueIcon(residuesToolBarLayout, notation, null, "#00000000", residueType, 24, image -> {
                    Button button = new Button(image);
                    button.addClassName("residueButton");
                    button.getElement().setProperty("title", residueType.getDescription());
                    button.addClickListener(mouseClickEvent -> {
                        glycanWebCanvas.addResidue(residueType);
                    });
                    residuesToolBarLayout.add(button);
                });
            } catch (Exception e) {
            }
        }

        // Creates add residue menu.
        allResidueMenuBar.removeAll();
        MenuItem allResidueMenuItem = allResidueMenuBar.addItem(new Image("images/residue.png", ""));
        SubMenu allResidueSubMenu = allResidueMenuItem.getSubMenu();
        StructureMenu structureMenu = new StructureMenu();
        structureMenu.createAddResidueMenu(glycanWebCanvas, allResidueSubMenu);
    }

   /**
    * Changes notation of structure.
    * @param glycanWebCanvas Canvas drawn residues and linkages.
    */
    @Override
    public void changeNotation(GlycanWebCanvas glycanWebCanvas) {
        residuesToolBarLayout.removeAll();
        createResiduesToolbar(glycanWebCanvas);
    }
}
