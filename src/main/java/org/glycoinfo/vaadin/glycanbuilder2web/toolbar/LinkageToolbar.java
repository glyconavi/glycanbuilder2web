package org.glycoinfo.vaadin.glycanbuilder2web.toolbar;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eurocarbdb.application.glycanbuilder.Residue;
import org.eurocarbdb.application.glycanbuilder.linkage.Linkage;
import org.glycoinfo.application.glycanbuilder.util.GlycanUtils;
import org.glycoinfo.vaadin.glycanbuilder2web.canvas.GlycanWebCanvas;
import org.glycoinfo.vaadin.glycanbuilder2web.component.MultipleCheckbox;

import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.littemplate.LitTemplate;
import com.vaadin.flow.component.select.Select;
import com.vaadin.flow.component.template.Id;

/**
 * Linkage toolbar to edit the bonding information.
 */
@Tag("linkage-toolbar")
@JsModule("./src/linkage-toolbar.ts")
@SuppressWarnings("serial")
public class LinkageToolbar extends LitTemplate {
    /**
     * Anomeric state.
     */
    @Id("anomericState")
    private Select<String> anomericState;

    /**
     * Default of anomeric state.
     */
    private final List<String> defaultAnomericStateItems = Arrays.asList("?", "a", "b");

    /**
     * Anomeric carbon.
     */
    @Id("anomericCarbon")
    private Select<String> anomericCarbon;

    /**
     * Default of anomeric carbon.
     */
    private final List<String> defaultAnomericCarbonItems = Arrays.asList("?", "1", "2", "3");

    /**
     * Linkage position.
     */
    @Id("linkagePosition")
    private MultipleCheckbox linkagePosition;

    /**
     * Default of linkage position.
     */
    private final List<String> defaultLinkagePositionItems = Arrays.asList("?", "1", "2", "3", "4", "5", "6", "7", "8", "9");

    /**
     * Chirality.
     */
    @Id("chirality")
    private Select<String> chirality;

    /**
     * Default of chirality.
     */
    private final List<String> defaultChiralityItems = Arrays.asList("?", "D", "L");

    /**
     * Ring size.
     */
    @Id("ringSize")
    private Select<String> ringSize;

    /**
     * Default of ring size.
     */
    private final List<String> defaultRingSizeItems = Arrays.asList("?", "p", "f", "o");

    /**
     * Checkbox of second bond to indicate whether there is it.
     */
    @Id("secondBond")
    private Checkbox secondBond;

    /**
     * Second parent position.
     */
    @Id("secondParentPosition")
    private MultipleCheckbox secondParentPosition;

    /**
     * Default of second parent position.
     */
    private final List<String> defaultSecondParentPositionItems = Arrays.asList("?", "1", "2", "3", "4", "5", "6", "7", "8", "9");

    @Id("secondBondArrow")
    private Image secondBondArrow;

    /**
     * Second child position.
     */
    @Id("secondChildPosition")
    private Select<String> secondChildPosition;

    /**
     * Default of second child position.
     */
    private final List<String> defaultSecondChildPositionItems = Arrays.asList("?", "1", "2", "3");

    /**
     * Whether linkage toolbar is updating.
     */
    private boolean isUpdatingLinkageToolbar = false;

    /**
     * Initializes linkage toolbar.
     */
    public LinkageToolbar() {
        initLinkageToolbar();
    }

    /**
     * Initializes linkage toolbar.
     */
    public void initLinkageToolbar() {
        // Initializes first linkage information.
        anomericState.setItems(defaultAnomericStateItems);
        anomericState.setValue("?");
        anomericCarbon.setItems(defaultAnomericCarbonItems);
        anomericCarbon.setValue("?");
        linkagePosition.setItems(defaultLinkagePositionItems);
        linkagePosition.setValue("?");
        chirality.setItems(defaultChiralityItems);
        chirality.setValue("?");
        ringSize.setItems(defaultRingSizeItems);
        ringSize.setValue("?");
        setEnabledOfLinkage(false);

        // Initializes second linkage information.
        setEnabledOfCheckboxOfSecondBond(false);
        secondParentPosition.setItems(defaultSecondParentPositionItems);
        secondParentPosition.setValue("?");
        secondChildPosition.setItems(defaultSecondChildPositionItems);
        secondChildPosition.setValue("?");
        setEnabledOfSelectOfSecondBond(false);
    }

    /**
     * Sets enabled of first linkage.
     * @param isEnabled Whether first linkage is enabled.
     */
    private void setEnabledOfLinkage(boolean isEnabled) {
        anomericState.setEnabled(isEnabled);
        anomericCarbon.setEnabled(isEnabled);
        linkagePosition.setEnabled(isEnabled);
        chirality.setEnabled(isEnabled);
        ringSize.setEnabled(isEnabled);
    }

    /**
     * Sets enabled of checkbox of second bond.
     * @param isEnabled Whether checkbox of second bond is enabled.
     */
    private void setEnabledOfCheckboxOfSecondBond(boolean isEnabled) {
        secondBond.setEnabled(isEnabled);
    }

    /**
     * Sets enabled of select of second bond.
     * @param isEnabled Whether select of second bond is enabled.
     */
    private void setEnabledOfSelectOfSecondBond(boolean isEnabled) {
        secondParentPosition.setEnabled(isEnabled);
        secondChildPosition.setEnabled(isEnabled);
    }

    /**
     * Updates linkage toolbar from getting GlycanWebCanvas.
     * @param isShowingControls
     * @param glycanWebCanvas GlycanWebCanvas to get the bonding information.
     */
    public void updateLinkageToolbar(boolean isShowingControls, GlycanWebCanvas glycanWebCanvas) {
        // Updates linkage toolbar.
        isUpdatingLinkageToolbar = true;
        Residue currentResidue = glycanWebCanvas.getCurrentResidue();
        if (currentResidue != null) {
            // Enable first linkage toolbar.
            anomericState.setEnabled(currentResidue.isSaccharide() && !currentResidue.getType().getSuperclass().equals("Bridge"));
            anomericCarbon.setEnabled(currentResidue.isSaccharide() || currentResidue.getType().getSuperclass().equals("Bridge"));
            chirality.setEnabled(currentResidue.isSaccharide() && !currentResidue.getType().getSuperclass().equals("Bridge"));
            ringSize.setEnabled(currentResidue.isSaccharide() && !currentResidue.getType().getSuperclass().equals("Bridge"));

            // Sets value of linkage toolbar.
            anomericState.setValue(String.valueOf(currentResidue.getAnomericState()));
            anomericCarbon.setValue(String.valueOf(currentResidue.getAnomericCarbon()));
            chirality.setValue(String.valueOf(currentResidue.getChirality()));
            ringSize.setValue(String.valueOf(currentResidue.getRingSize()));

            // Updates second linkage toolbar.
            if (!currentResidue.isEndRepetition() || currentResidue.isCleavage()) {
                // If current residue has parent, updates second linkage toolbar.
                Linkage parentLinkage =  currentResidue.getParentLinkage();
                if (parentLinkage != null) {
                    // Gets whether current residue is having parent residue.
                    boolean isHavingParentResidue = false;
                    Residue parentResidue = parentLinkage.getParentResidue();
                    if (parentResidue != null) {
                        if (parentResidue.isSaccharide() || parentResidue.isBracket() || parentResidue.isRepetition() || parentResidue.isRingFragment()) {
                            isHavingParentResidue = true;
                        }
                    }

                    // Sets enabled linkage position, second bond, second parent position and second child position.
                    linkagePosition.setEnabled(isHavingParentResidue || currentResidue.isEndRepetition());
                    secondBond.setEnabled(isHavingParentResidue);
                    secondParentPosition.setEnabled(isHavingParentResidue && parentLinkage.hasMultipleBonds());
                    secondChildPosition.setEnabled(isHavingParentResidue && parentLinkage.hasMultipleBonds());

                    // Sets linkage and parent position.
                    List<String> parentLinkagePosition = createParentLinkagePositionItems(parentLinkage.getParentResidue());
                    linkagePosition.setItems(parentLinkagePosition);
                    secondParentPosition.setItems(parentLinkagePosition);

                    // Sets value of linkage position, second bond, second parent position and second child position.
                    secondBond.setValue(parentLinkage.hasMultipleBonds());
                    char[] linkagePositionsArray = { '?', };
                    if (!String.valueOf(parentLinkage.glycosidicBond().getParentPositions()).equals("")) {
                        linkagePositionsArray = parentLinkage.glycosidicBond().getParentPositions();
                    }
                    Set<String> linkagePositions = new HashSet<String>();
                    for (int i = 0; i < linkagePositionsArray.length; i++) {
                        linkagePositions.add(String.valueOf(linkagePositionsArray[i]));
                    }
                    linkagePosition.setValue(linkagePositions);
                    char[] parentPositionsArray = { '?', };
                    if (!String.valueOf(parentLinkage.getBonds().get(0).getParentPositions()).equals("")) {
                        parentPositionsArray = parentLinkage.getBonds().get(0).getParentPositions();
                    }
                    Set<String> parentPositions = new HashSet<String>();
                    for (int i = 0; i < parentPositionsArray.length; i++) {
                        parentPositions.add(String.valueOf(parentPositionsArray[i]));
                    }
                    secondParentPosition.setValue(parentPositions);
                    secondChildPosition.setValue(String.valueOf(parentLinkage.getBonds().get(0).getChildPosition()));
                } else {
                    // Sets default value to linkage position, second bond, second parent position and second child position.
                    linkagePosition.setValue("?");
                    secondBond.setValue(false);
                    secondParentPosition.setValue("?");
                    secondChildPosition.setValue("?");
                }
            }

            // If current residue is end repetition, sets enabled linkage position and value of anomeric carbon.
            if (currentResidue.isEndRepetition()) {
                linkagePosition.setEnabled(true);
                anomericCarbon.setValue("1");
            }
        } else {
            // Disables linkage, check box of second bond and select of second bond.
            setEnabledOfLinkage(false);
            setEnabledOfCheckboxOfSecondBond(false);
            setEnabledOfSelectOfSecondBond(false);

            // Sets default value of linkage toolbar.
            linkagePosition.setValue("?");
            anomericState.setValue("?");
            anomericCarbon.setValue("?");
            chirality.setValue("?");
            ringSize.setValue("?");
            secondBond.setValue(false);
            secondParentPosition.setValue("?");
            secondChildPosition.setValue("?");
        }
        isUpdatingLinkageToolbar = false;
    }

    /**
     * Creates parent linkage position items.
     * @param parentResidue Parent residue.
     * @return Returns parent linkage position items.
     */
    public List<String> createParentLinkagePositionItems(Residue parentResidue) {
        // If current residue doesn't have parent, returns default linkage position items.
        if (parentResidue == null || parentResidue.getType().getLinkagePositions().length == 0) return defaultLinkagePositionItems;

        // Creates parent linkage position items using parent residue.
        List<String> parentLinkagePositionItems = new ArrayList<String>();
        parentLinkagePositionItems.add("?");
        char[] parentLinkagePositionCharArray = parentResidue.getType().getLinkagePositions();
        for (char parentLinkagePositionChar : parentLinkagePositionCharArray) {
            parentLinkagePositionItems.add(String.valueOf(parentLinkagePositionChar));
        }
        return parentLinkagePositionItems;
    }

    /**
     * Adds value change listener for linkage toolbar.
     * @param glycanWebCanvas GlycanWebCanvas to draw the bonding information.
     */
    public void addValueChangeListener(GlycanWebCanvas glycanWebCanvas) {
        // Adds value change listener for current linkage.
        anomericState.addValueChangeListener(valueChangedEvent -> {
            setAnomericState(glycanWebCanvas, valueChangedEvent.getValue().toCharArray()[0]);
        });
        anomericCarbon.addValueChangeListener(valueChangedEvent -> {
            setAnomericCarbon(glycanWebCanvas, valueChangedEvent.getValue().toCharArray()[0]);
        });
        chirality.addValueChangeListener(valueChangedEvent -> {
            setChirality(glycanWebCanvas, valueChangedEvent.getValue().toCharArray()[0]);
        });
        ringSize.addValueChangeListener(valueChangedEvent -> {
            setRingSize(glycanWebCanvas, valueChangedEvent.getValue().toCharArray()[0]);
        });

        // Adds value change listener for parent linkage.
        secondBond.addValueChangeListener(valueChangedEvent -> {
            if (valueChangedEvent.getValue()) {
                secondParentPosition.setClassName("secondBondIsChecked");
                secondBondArrow.setClassName("secondBondIsChecked");
                secondChildPosition.setClassName("secondBondIsChecked");
            } else {
                secondParentPosition.setClassName("secondBondIsNotChecked");
                secondBondArrow.setClassName("secondBondIsNotChecked");
                secondChildPosition.setClassName("secondBondIsNotChecked");
            }
            setLinkagePositionValue(glycanWebCanvas, valueChangedEvent.getValue(), linkagePosition.getValue(), secondParentPosition.getValue(), secondChildPosition.getValue());
            updateLinkageToolbar(true, glycanWebCanvas);
        });
        linkagePosition.addValueChangeListener(valueChangedEvent -> {
            setLinkagePositionValue(glycanWebCanvas, secondBond.getValue(), valueChangedEvent.getValue(), secondParentPosition.getValue(), secondChildPosition.getValue());
        });
        secondParentPosition.addValueChangeListener(valueChangedEvent -> {
            setLinkagePositionValue(glycanWebCanvas, secondBond.getValue(), linkagePosition.getValue(), valueChangedEvent.getValue(), secondChildPosition.getValue());
        });
        secondChildPosition.addValueChangeListener(valueChangedEvent -> {
            setLinkagePositionValue(glycanWebCanvas, secondBond.getValue(), linkagePosition.getValue(), secondParentPosition.getValue(), valueChangedEvent.getValue());
        });
    }

    /**
     * Sets anomeric state.
     * @param glycanWebCanvas GlycanWebCanvas to draw the bonding information.
     * @param value Value of anomeric state.
     */
    private void setAnomericState(GlycanWebCanvas glycanWebCanvas, char value) {
        if (isUpdatingLinkageToolbar) return;
        Residue currentResidue = glycanWebCanvas.getCurrentResidue();
        if (currentResidue == null) return;

        // If current residue is not alditol, changes ringsize and anomeric state.
        if (currentResidue.isSaccharide() && !GlycanUtils.isFacingAnom(currentResidue) && currentResidue.isAlditol() && value != '?' || ringSize.getValue() != "o") {
            currentResidue.setAlditol(false);
            ringSize.setValue(currentResidue.getRingSize() == 'o' ? "?" : String.valueOf(currentResidue.getRingSize()));
        }

        // Sets anomeric state and update canvas.
        currentResidue.setAnomericState(value);
        if (!isUpdatingLinkageToolbar) glycanWebCanvas.updateCanvas();
    }

    /**
     * Sets anomeric carbon.
     * @param glycanWebCanvas GlycanWebCanvas to draw the bonding information.
     * @param value Value of anomeric carbon.
     */
    private void setAnomericCarbon(GlycanWebCanvas glycanWebCanvas, char value) {
        if (isUpdatingLinkageToolbar) return;
        Residue currentResidue = glycanWebCanvas.getCurrentResidue();
        if (currentResidue == null) return;

        // Sets anomeric carbon and update canvas.
        currentResidue.setAnomericCarbon(value);
        if (!isUpdatingLinkageToolbar) glycanWebCanvas.updateCanvas();
    }

    /**
     * Sets chirality.
     * @param glycanWebCanvas GlycanWebCanvas to draw the bonding information.
     * @param value Value of chirality.
     */
    private void setChirality(GlycanWebCanvas glycanWebCanvas, char value) {
        if (isUpdatingLinkageToolbar) return;
        Residue currentResidue = glycanWebCanvas.getCurrentResidue();
        if (currentResidue == null) return;

        // Sets chirality and update canvas.
        currentResidue.setChirality(value);
        if (!isUpdatingLinkageToolbar) glycanWebCanvas.updateCanvas();
    }

    /**
     * Sets ring size.
     * @param glycanWebCanvas GlycanWebCanvas to draw the bonding information.
     * @param value Value of ring size.
     */
    private void setRingSize(GlycanWebCanvas glycanWebCanvas, char value) {
        if (isUpdatingLinkageToolbar) return;
        Residue currentResidue = glycanWebCanvas.getCurrentResidue();
        if (currentResidue == null) return;

        // If current residue is alditol, changes anomeric state to ?.
        if (currentResidue.isSaccharide() && !GlycanUtils.isFacingAnom(currentResidue)) {
            if (!currentResidue.isAlditol() && value == 'o') {
                currentResidue.setAlditol(true);
                anomericState.setValue("?");
            }
            if (currentResidue.isAlditol() && value != 'o') {
                currentResidue.setAlditol(false);
            }
        }

        // Sets ring size and update canvas.
        currentResidue.setRingSize(value);
        if (!isUpdatingLinkageToolbar) glycanWebCanvas.updateCanvas();
    }

    /**
     * Sets linkage position value.
     * @param glycanWebCanvas
     * @param hasSecondBond
     * @param linkagePositionValues
     * @param secondParentPositionValues
     * @param secondChildPositionValue
     */
    private void setLinkagePositionValue(GlycanWebCanvas glycanWebCanvas, boolean hasSecondBond, Set<String> linkagePositionValues, Set<String> secondParentPositionValues, String secondChildPositionValue) {
        if (isUpdatingLinkageToolbar) return;
        if (linkagePositionValues == null || secondParentPositionValues == null) return;
        Residue currentResidue = glycanWebCanvas.getCurrentResidue();
        if (currentResidue == null) return;
        Linkage parentLinkage =  currentResidue.getParentLinkage();
        if (parentLinkage == null) return;
        if (linkagePositionValues.size() <= 0 || secondParentPositionValues.size() <= 0) return;

        // Sets linkage position value and update canvas.
        char[] linkagePositionValuesArray = new char[linkagePositionValues.size()];
        int i = 0;
        for (String linkagePositionValue : linkagePositionValues) {
            linkagePositionValuesArray[i] = linkagePositionValue.toCharArray()[0];
            i++;
        }
        char[] secondParentPositionValuesArray = new char[secondParentPositionValues.size()];
        i = 0;
        for (String secondParentPositionValue : secondParentPositionValues) {
            secondParentPositionValuesArray[i] = secondParentPositionValue.toCharArray()[0];
            i++;
        }
        if (hasSecondBond) {
            parentLinkage.setLinkagePositions(linkagePositionValuesArray, secondParentPositionValuesArray, secondChildPositionValue.toCharArray()[0]);
        } else {
            parentLinkage.setLinkagePositions(linkagePositionValuesArray);
        }
        if (!isUpdatingLinkageToolbar) glycanWebCanvas.updateCanvas();
    }
}
