package org.glycoinfo.vaadin.glycanbuilder2web.toolbar;

import java.io.IOException;

import java.net.URLEncoder;
import java.util.Arrays;
import java.util.List;

import org.glycoinfo.vaadin.glycanbuilder2web.canvas.GlycanWebCanvas;
import org.glycoinfo.vaadin.glycanbuilder2web.dialog.ExplanationDialog;
import org.glycoinfo.vaadin.glycanbuilder2web.util.HTTP;
import org.glycoinfo.vaadin.glycanbuilder2web.util.SearchAddress;
import org.glycoinfo.vaadin.glycanbuilder2web.util.WURCSConverter;

import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.littemplate.LitTemplate;
import com.vaadin.flow.component.select.Select;
import com.vaadin.flow.component.template.Id;

/**
 * Searcbar that accesses a database of glycan and searches glycan information in the database.
 */
@Tag("glycan-information-searchbar")
@JsModule("./src/glycan-information-searchbar.ts")
@SuppressWarnings("serial")
public class GlycanInformationSearchbar extends LitTemplate {
    /**
     * URL of getting GlyTouCanID.
     */
    private String glyTouCanIDURL = SearchAddress.sparqlistOfGlycoNaviAdress + "/api/WURCS2GlyTouCan_text?WURCS=%s";

    /**
     * URL of searching glycan with GlyTouCan.
     */
    private String glyTouCanURL = SearchAddress.glyTouCanAdress + "/Structures/Glycans/%s";

    /**
     * URL of searching glycan with GlyCosmos (exact).
     */
    private String glyCosmosExactURL = SearchAddress.glyCosmosAdress + "/glycans/show/%s";

    /**
     * URL of searching glycan with GlyCosmos (partial).
     */
    private String glyCosmosPartialURL = SearchAddress.glyCosmosAdress + "/glycans/text?format=wurcs&method=partial&text=%s";

    /**
     * URL of searching glycan with GlycoNavi.
     */
    private String glycoNaviURL = SearchAddress.glycoNaviAdress + "/hub/?wurcs=%s";

    /**
     * Select that which database to access.
     */
    @Id("selectedDatabase")
    private Select<String> selectedDatabase;

    /**
     * Default of selected database.
     */
    private final List<String> defaultSelectedDatabase = Arrays.asList("GlyTouCan", "GlyCosmos (exact)", "GlyCosmos (partial)", "GlycoNAVI");

    /**
     * Button that search the database of glycan.
     */
    @Id("glycanInformationSeachButton")
    private Button glycanInformationSearchButton;

    /**
     * Initializes glycan information searchbar.
     */
    public GlycanInformationSearchbar() {
        initGlycanInformationSearchbar();
    }

    /**
     * Initializes glycan information searchbar.
     */
    private void initGlycanInformationSearchbar() {
        selectedDatabase.setItems(defaultSelectedDatabase);
        selectedDatabase.setValue(defaultSelectedDatabase.get(3));
        glycanInformationSearchButton.getElement().setProperty("title", "Search glycan");
    }

    /**
     * Adds click listener for searching glycan information.
     * @param glycanWebCanvas Canvas drawn residues and linkages.
     */
    public void addClickListener(GlycanWebCanvas glycanWebCanvas) {
        glycanInformationSearchButton.addClickListener(mouseClickEvent -> {
            searchGlycanInformation(glycanWebCanvas);
        });
    }

    /**
     * Searches glycan information.
     */
    private void searchGlycanInformation(GlycanWebCanvas glycanWebCanvas) {
        // If structure doesn't exist, display message about it and ends process.
        String wurcsFromFirstStructure = WURCSConverter.getWURCSFromFirstStructure(glycanWebCanvas);
        if (wurcsFromFirstStructure == null) {
            new ExplanationDialog("Issue", "You need to draw a structure.");
            return;
        }
        if (wurcsFromFirstStructure == "") {
            new ExplanationDialog("Issue", "This structure is not searchable.");
            return;
        }

        // Searches glycan information.
        if (selectedDatabase.getValue().equals("GlyCosmos (partial)") || selectedDatabase.getValue().equals("GlycoNAVI")) {
            openSearchResultWithWurcs(wurcsFromFirstStructure);
        } else {
            // If GlyTouCan ID doesn't exist, display message about it and ends process.
            String glyTouCanID = getGlyTouCanID(wurcsFromFirstStructure);
            if (glyTouCanID == null) {
                new ExplanationDialog("Issue", "This structure isn't registered yet.");
                return;
            }

            // Opens search result using GlyTouCan ID.
            glyTouCanID = glyTouCanID.replaceAll("\r\n", "").replaceAll("\n", "");
            openSearchResultWithGlyTouCanID(glyTouCanID);
        }
    }

    /**
     * Gets GlyTouCan ID from WORCS string.
     * @param wurcs WURCS string.
     * @return Returns GlyTouCan ID.
     */
    private String getGlyTouCanID(String wurcs) {
        String glyTouCanID = null;
        try {
            String url = String.format(glyTouCanIDURL, URLEncoder.encode(wurcs, "UTF-8"));
            glyTouCanID = HTTP.httpGet(url);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return glyTouCanID;
    }

    /**
     * Opens search result using GlyTouCan ID.
     * @param glyTouCanID GlyTouCan ID.
     */
    private void openSearchResultWithGlyTouCanID(String glyTouCanID) {
        try {
            String url;
            switch (selectedDatabase.getValue()) {
                case "GlyTouCan":
                    url = String.format(glyTouCanURL, URLEncoder.encode(glyTouCanID, "UTF-8"));
                    break;
                case "GlyCosmos (exact)":
                default:
                    url = String.format(glyCosmosExactURL, URLEncoder.encode(glyTouCanID, "UTF-8"));
                    break;
            }
            openSearchResul(url);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Opens search result using WURCS.
     * @param wurcs WURCS string.
     */
    private void openSearchResultWithWurcs(String wurcs) {
        try {
            String url;
            switch (selectedDatabase.getValue()) {
                case "GlyCosmos (partial)":
                    url = String.format(glyCosmosPartialURL, URLEncoder.encode(wurcs, "UTF-8"));
                    break;
                case "GlycoNAVI":
                default:
                    url = String.format(glycoNaviURL, URLEncoder.encode(wurcs, "UTF-8"));
                    break;
            }
            openSearchResul(url);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Opens search result.
     * @param url URL to open search result page.
     */
    private void openSearchResul(String url) {
        getElement().getNode().runWhenAttached(ui -> {
            ui.getInternals().getStateTree().beforeClientResponse(getElement().getNode(), context -> {
                ui.getPage().open(url);
            });
        });
    }
}
