package org.glycoinfo.vaadin.glycanbuilder2web.toolbar;

import org.glycoinfo.vaadin.glycanbuilder2web.canvas.GlycanWebCanvas;
import org.glycoinfo.vaadin.glycanbuilder2web.menubar.ViewMenu;

import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.littemplate.LitTemplate;
import com.vaadin.flow.component.template.Id;

/**
 * Structure toolbar to manipulate residues.
 */
@Tag("structure-toolbar")
@JsModule("./src/structure-toolbar.ts")
@SuppressWarnings("serial")
public class StructureToolbar extends LitTemplate {
    /**
     * Bracket button for fragment structure.
     */
    @Id("bracketButton")
    private Button bracketButton;

    /**
     * Repeat button for repetition structure.
     */
    @Id("repeatButton")
    private Button repeatButton;

    /**
     * Cyclic button for cyclic structure.
     */
    @Id("cyclicButton")
    private Button cyclicButton;

    /**
     * Resisue properties button.
     */
    @Id("residuePropertiesButton")
    private Button residuePropertiesButton;

    /**
     * Reload button to reset the canvas.
     */
    @Id("reloadButton")
    private Button reloadButton;

    /**
     * Import button to convert structures from structures string.
     */
    @Id("importButton")
    private Button importButton;

    /**
     * Export button to export structures string.
     */
    @Id("exportButton")
    private Button exportButton;

    /**
     * Delete button to delete selected residue or structure.
     */
    @Id("deleteButton")
    private Button deleteButton;

    /**
     * Orientation button to rotate structure clockwise.
     */
    @Id("orientationButton")
    private Button orientationButton;

    /**
     * Undo button to undo operation.
     */
    @Id("undoButton")
    private Button undoButton;

    /**
     * Redo button to redo operation.
     */
    @Id("redoButton")
    private Button redoButton;

    /**
     * Checkbox switching linkage information.
     */
    @Id("linkageInformation")
    private Checkbox linkageInformation;

    /**
     * Checkbox to switch displaying masses.
     */
    @Id("masses")
    private Checkbox masses;

    /**
     * Enlarge button to enlarge canvas.
     */
    @Id("enlargeButton")
    private Button enlargeButton;

    /**
     * Shrink button to shrink canvas.
     */
    @Id("shrinkButton")
    private Button shrinkButton;

    /**
     * Initializes structure toolbar.
     */
    public StructureToolbar() {
        bracketButton.setEnabled(false);
        bracketButton.getElement().setProperty("title", "Add bracket");
        repeatButton.setEnabled(false);
        repeatButton.getElement().setProperty("title", "Add repeating unit");
        cyclicButton.setEnabled(false);
        cyclicButton.getElement().setProperty("title", "Add cyclic symbol");
        residuePropertiesButton.setEnabled(false);
        residuePropertiesButton.getElement().setProperty("title", "Residue properties");
        reloadButton.setEnabled(true);
        reloadButton.getElement().setProperty("title", "Reload canvas");
        importButton.setEnabled(true);
        importButton.getElement().setProperty("title", "Add structure from string");
        exportButton.setEnabled(true);
        exportButton.getElement().setProperty("title", "Get string from structure");
        deleteButton.setEnabled(false);
        deleteButton.getElement().setProperty("title", "Delete");
        orientationButton.setEnabled(true);
        orientationButton.getElement().setProperty("title", "Change orientation");
        undoButton.setEnabled(false);
        undoButton.getElement().setProperty("title", "Undo");
        redoButton.setEnabled(false);
        redoButton.getElement().setProperty("title", "Redo");
        linkageInformation.setValue(true);
        linkageInformation.getElement().setProperty("title", "Switch whether to display linkage information");
        masses.setValue(true);
        masses.getElement().setProperty("title", "Switch to show / hide masses");
        enlargeButton.getElement().setProperty("title", "Enlarge");
        shrinkButton.getElement().setProperty("title", "Enlarge");
    }

    /**
     * Sets enabled of structure toolbar.
     * @param enabled Whether structure toolbar is enabled.
     */
    public void setEnabled(boolean isEnabled) {
        bracketButton.setEnabled(isEnabled);
        repeatButton.setEnabled(isEnabled);
        cyclicButton.setEnabled(isEnabled);
        residuePropertiesButton.setEnabled(isEnabled);
        deleteButton.setEnabled(isEnabled);
    }

    /**
     * Sets enabled of undo button.
     * @param isEnabled Whether undo button is enabled.
     */
    public void setEnabledOfUndoButton(boolean isEnabled) {
        undoButton.setEnabled(isEnabled);
    }

    /**
     * Sets enabled of redo button.
     * @param isEnabled Whether redo button is enabled.
     */
    public void setEnabledOfRedoButton(boolean isEnabled) {
        redoButton.setEnabled(isEnabled);
    }

    /**
     * Adds click listener for each button.
     * @param glycanWebCanvas Canvas drawn residues and linkages.
     * @param viewMenu View menu bar to change display related functions.
     */
    public void addClickListener(GlycanWebCanvas glycanWebCanvas, ViewMenu viewMenu) {
        bracketButton.addClickListener(mouseClickEvent -> {
            glycanWebCanvas.addBracket();
        });
        repeatButton.addClickListener(mouseClickEvent -> {
            glycanWebCanvas.addRepeat();
        });
        cyclicButton.addClickListener(mouseClickEvent -> {
            glycanWebCanvas.addCyclic();
        });
        residuePropertiesButton.addClickListener(mouseClickEvent -> {
            glycanWebCanvas.adjustResidueProperties();
        });
        reloadButton.addClickListener(mouseClickEvent -> {
            glycanWebCanvas.resetCanvas();
        });
        importButton.addClickListener(mouseClickEvent -> {
            glycanWebCanvas.importStructures();
        });
        exportButton.addClickListener(mouseClickEvent -> {
            glycanWebCanvas.exportStructures();
        });
        deleteButton.addClickListener(mouseClickEvent -> {
            glycanWebCanvas.deleteResidue();
        });
        orientationButton.addClickListener(mouseClickEvent -> {
            glycanWebCanvas.rotateStructureClockwise();
        });
        undoButton.addClickListener(mouseClickEvent -> {
            glycanWebCanvas.undoOperation();
        });
        redoButton.addClickListener(mouseClickEvent -> {
            glycanWebCanvas.redoOperation();
        });
        linkageInformation.addValueChangeListener(valueChangedEvent -> {
            if (valueChangedEvent.getValue()) {
                viewMenu.changeToNormalViewWithLinkageInfo(glycanWebCanvas);
            } else {
                viewMenu.changeToCompactView(glycanWebCanvas);
            }
        });
        masses.addValueChangeListener(valueChangedEvent -> {
            viewMenu.showHideMassesInTheDrawingCanvas(glycanWebCanvas, valueChangedEvent.getValue());
        });
        enlargeButton.addClickListener(mouseClickEvent -> {
            if (glycanWebCanvas.getScaleRate() == 3d) {
                viewMenu.changeScaleRateTo400(glycanWebCanvas);
            } else if (glycanWebCanvas.getScaleRate() == 2d) {
                viewMenu.changeScaleRateTo300(glycanWebCanvas);
            } else if (glycanWebCanvas.getScaleRate() == 1.5d) {
                viewMenu.changeScaleRateTo200(glycanWebCanvas);
            } else if (glycanWebCanvas.getScaleRate() == 1d) {
                viewMenu.changeScaleRateTo150(glycanWebCanvas);
            } else if (glycanWebCanvas.getScaleRate() == 0.67d) {
                viewMenu.changeScaleRateTo100(glycanWebCanvas);
            } else if (glycanWebCanvas.getScaleRate() == 0.5d) {
                viewMenu.changeScaleRateTo67(glycanWebCanvas);
            } else if (glycanWebCanvas.getScaleRate() == 0.33d) {
                viewMenu.changeScaleRateTo50(glycanWebCanvas);
            } else if (glycanWebCanvas.getScaleRate() == 0.25d) {
                viewMenu.changeScaleRateTo33(glycanWebCanvas);
            }
        });
        shrinkButton.addClickListener(mouseClickEvent -> {
            if (glycanWebCanvas.getScaleRate() == 4d) {
                viewMenu.changeScaleRateTo300(glycanWebCanvas);
            } else if (glycanWebCanvas.getScaleRate() == 3d) {
                viewMenu.changeScaleRateTo200(glycanWebCanvas);
            } else if (glycanWebCanvas.getScaleRate() == 2d) {
                viewMenu.changeScaleRateTo150(glycanWebCanvas);
            } else if (glycanWebCanvas.getScaleRate() == 1.5d) {
                viewMenu.changeScaleRateTo100(glycanWebCanvas);
            } else if (glycanWebCanvas.getScaleRate() == 1d) {
                viewMenu.changeScaleRateTo67(glycanWebCanvas);
            } else if (glycanWebCanvas.getScaleRate() == 0.67d) {
                viewMenu.changeScaleRateTo50(glycanWebCanvas);
            } else if (glycanWebCanvas.getScaleRate() == 0.5d) {
                viewMenu.changeScaleRateTo33(glycanWebCanvas);
            } else if (glycanWebCanvas.getScaleRate() == 0.33d) {
                viewMenu.changeScaleRateTo25(glycanWebCanvas);
            }
        });
    }
}
