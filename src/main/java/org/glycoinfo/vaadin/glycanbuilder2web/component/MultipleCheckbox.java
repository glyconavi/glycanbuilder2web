package org.glycoinfo.vaadin.glycanbuilder2web.component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;

import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.littemplate.LitTemplate;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.template.Id;

/**
 * Multiple checkbox to be able to get checked values of checkboxes as String.
 */
@Tag("multiple-checkbox")
@JsModule("./src/multiple-checkbox.ts")
@SuppressWarnings("serial")
public class MultipleCheckbox extends LitTemplate {
    /**
     * Whether multiple checkbox is enabled.
     */
    boolean enabled = true;

    /**
     * Current list of checkbox to be able to check.
     */
    List<Checkbox> checkboxList = new ArrayList<Checkbox>();

    /**
     * Layout of multiple checkbox.
     */
    @Id("multipleCheckboxLayout")
    HorizontalLayout multipleCheckboxLayout;

    /**
     * Last checkbox currently checked.
     */
    Checkbox lastCheckbox = null;

    /**
     * Whether is setting values.
     */
    boolean isSettingValues = false;

    /**
     * Value change listener.
     */
    Consumer<MultipleCheckbox> valueChangeListener = null;

    /**
     * Sets enabled of multiple checkbox.
     * @param enabled Whether multiple checkbox is enabled.
     */
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
        for (Checkbox checkbox : checkboxList) {
            checkbox.setEnabled(enabled);
        }
    }

    /**
     * Sets items specified.
     * @param items
     */
    public void setItems(Collection<String> items) {
        checkboxList.clear();
        multipleCheckboxLayout.removeAll();
        int currentNumberOfItem = 0; 
        for (String item : items) {
            Checkbox checkbox = new Checkbox();
            checkbox.addClassName("multipleCheckbox");
            if (currentNumberOfItem % 4 != 0) checkbox.addClassName("intermediateMultipleCheckbox");
            checkbox.setLabel(item);
            checkbox.addValueChangeListener(valueChangedEvent -> {
                changeValue();
            });
            checkbox.setEnabled(enabled);
            checkboxList.add(checkbox);
            multipleCheckboxLayout.add(checkbox);
            currentNumberOfItem++;
        }
    }

    /**
     * Changes value when checking checkbox.
     */
    public void changeValue() {
        // Checks at least one checked.
        boolean isAtLeastOneChecked = false;
        for (Checkbox checkbox : checkboxList) {
            if (checkbox.getValue()) {
                lastCheckbox = checkbox;
                isAtLeastOneChecked = true;
                break;
            }
        }

        // If at least one isn't checked, last of checkbox is checked.
        if (isAtLeastOneChecked) {
            if (isSettingValues) return;
            if (valueChangeListener != null) valueChangeListener.accept(this);
        } else {
            lastCheckbox.setValue(true);
        }
    }

    /**
     * Sets one value.
     * @param value Set value.
     */
    public void setValue(String value) {
        // Checks checkbox corresponding to value.
        isSettingValues = true;
        for (Checkbox checkbox : checkboxList) {
            checkbox.setValue(false);
        }
        for (Checkbox checkbox : checkboxList) {
            if (checkbox.getLabel().equals(value)) {
                checkbox.setValue(true);
                break;
            }
        }
        isSettingValues = false;

        // Calls value change listener.
        if (valueChangeListener != null) valueChangeListener.accept(this);
    }

    /**
     * Sets multiple value.
     * @param values Set values.
     */
    public void setValue(Set<String> values) {
        // Checks checkbox corresponding to values.
        isSettingValues = true;
        for (Checkbox checkbox : checkboxList) {
            checkbox.setValue(false);
        }
        for (String value : values) {
            for (Checkbox checkbox : checkboxList) {
                if (checkbox.getLabel().equals(value)) {
                    checkbox.setValue(true);
                    continue;
                }
            }
        }
        isSettingValues = false;

        // Calls value change listener.
        if (valueChangeListener != null) valueChangeListener.accept(this);
    }

    /**
     * Adds value change listener.
     * @param valueChangeListener Value change listener.
     */
    public void addValueChangeListener(Consumer<MultipleCheckbox> valueChangeListener) {
        this.valueChangeListener = valueChangeListener;
    }

    /**
     * Gets checked value.
     * @return Returns checked value.
     */
    public Set<String> getValue() {
        Set<String> selectedValues = new HashSet<String>();
        for (Checkbox checkbox : checkboxList) {
            if (checkbox.getValue()) {
                selectedValues.add(checkbox.getLabel());
            }
        }
        return selectedValues;
    }
}
