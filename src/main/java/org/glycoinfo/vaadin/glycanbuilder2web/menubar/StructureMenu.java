package org.glycoinfo.vaadin.glycanbuilder2web.menubar;

import org.eurocarbdb.application.glycanbuilder.CoreType;
import org.eurocarbdb.application.glycanbuilder.ResidueType;
import org.eurocarbdb.application.glycanbuilder.TerminalType;
import org.eurocarbdb.application.glycanbuilder.dataset.CoreDictionary;
import org.eurocarbdb.application.glycanbuilder.dataset.ResidueDictionary;
import org.eurocarbdb.application.glycanbuilder.dataset.TerminalDictionary;
import org.glycoinfo.application.glycanbuilder.dataset.SubstituentTypeDescriptor;
import org.glycoinfo.vaadin.glycanbuilder2web.canvas.GlycanWebCanvas;
import org.glycoinfo.vaadin.glycanbuilder2web.canvas.NotationChangeListener;
import org.glycoinfo.vaadin.glycanbuilder2web.canvas.ResidueIconCreator;

import com.vaadin.flow.component.contextmenu.MenuItem;
import com.vaadin.flow.component.contextmenu.SubMenu;
import com.vaadin.flow.component.menubar.MenuBar;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;

/**
 * Structure menu bar to add, insert and change a residue.
 */
public class StructureMenu implements NotationChangeListener {
    /**
     * Sub menu of composition.
     */
    private SubMenu compositionSubMenu;

    /**
     * Sub menu of structure.
     */
    private SubMenu structureSubMenu;

    /**
     * Menu of add residue.
     */
    private MenuItem addResidueMenu;

    /**
     * Sub menu of terminal.
     */
    private SubMenu terminalSubMenu;

    /**
     * Sub menu of reducing end type.
     */
    private SubMenu reducingEndTypeMenu;

    /**
     * Initializes structure menu.
     */
    public StructureMenu() {}

    /**
     * Initializes structure menu.
     * @param glycanMenuBar Menu bar for Glycan Builder 2.
     */
    public StructureMenu(MenuBar glycanMenuBar) {
        MenuItem structureMenuItem = glycanMenuBar.addItem("Structure");
        compositionSubMenu = structureMenuItem.getSubMenu();
        structureSubMenu = structureMenuItem.getSubMenu();
        terminalSubMenu = structureMenuItem.getSubMenu();
        reducingEndTypeMenu = structureMenuItem.getSubMenu();
    }

    /**
     * Creates add composition menu to add specified composition.
     * @param glycanWebCanvas Canvas drawn residues and linkages.
     */
    public void createAddComposition(GlycanWebCanvas glycanWebCanvas) {
        compositionSubMenu.addItem("Add composition", mouseClickEvent -> {
            glycanWebCanvas.showCompositionDialog();
        });
    }

    /**
     * Creates add structure menu to add template structure.
     * @param glycanWebCanvas Canvas drawn residues and linkages.
     */
    public void createAddStructureMenu(GlycanWebCanvas glycanWebCanvas) {
        // Creates add structure menu.
        MenuItem addStructureMenu = structureSubMenu.addItem("Add structure");
        SubMenu addStructureSubMenu = addStructureMenu.getSubMenu();
        boolean isSettingCoreMenu = false;
        for (String structureSuperclass : CoreDictionary.getSuperclasses()) {
            MenuItem structureClassMenu = addStructureSubMenu.addItem(structureSuperclass);
            SubMenu structureClassSubMenu = structureClassMenu.getSubMenu();
            for (CoreType coreType : CoreDictionary.getCores(structureSuperclass)) {
                // If core description doesn't contain dermatan or heparan, substitutes false to flag.
                String coreDescription = coreType.getDescription();
                if ((!coreDescription.contains("Dermatan sulfate") && !coreDescription.contains("Heparan sulfate")) && isSettingCoreMenu == true) isSettingCoreMenu = false;
                if (isSettingCoreMenu) continue;

                // Adds structure menu to sub menu. 
                if (coreDescription.contains("Dermatan sulfate") || coreDescription.contains("Heparan sulfate")) {
                    // Creates menu of GAGs core type.
                    String coreDescriptionPrefix = coreDescription.substring(0, coreDescription.indexOf("("));
                    MenuItem coreDescriptionMenu = structureClassSubMenu.addItem(coreDescriptionPrefix);
                    SubMenu coreDescriptionSubMenu = coreDescriptionMenu.getSubMenu();
                    for (CoreType GAGsCoreType : CoreDictionary.getCores("GAGs")) {
                        if(GAGsCoreType.getDescription().contains(coreDescriptionPrefix)) {
                            String coreName = GAGsCoreType.getName();
                            coreDescriptionSubMenu.addItem(GAGsCoreType.getDescription(), mouseClickEvent -> {
                                glycanWebCanvas.addStructure(coreName);
                            });
                        }
                    }
                    isSettingCoreMenu = true;
                } else {
                    // Creates menu of other core type.
                    String coreName = coreType.getName();
                    structureClassSubMenu.addItem(coreDescription, mouseClickEvent -> {
                        glycanWebCanvas.addStructure(coreName);
                    });
                }
            }
        }
    }

    /**
     * Creates add residue menu to draw a residue.
     * @param glycanWebCanvas Canvas drawn residues and linkages.
     */
    public void createAddResidueMenu(GlycanWebCanvas glycanWebCanvas) {
        // Creates residue menu.
        addResidueMenu = structureSubMenu.addItem("Add residue");
        SubMenu addResidueSubMenu = addResidueMenu.getSubMenu();
        createAddResidueMenu(glycanWebCanvas, addResidueSubMenu);
    }

    /**
     * Creates add residue menu to draw a residue.
     * @param glycanWebCanvas Canvas drawn residues and linkages.
     * @param addResidueSubMenu Add residue sub menu.
     */
    public void createAddResidueMenu(GlycanWebCanvas glycanWebCanvas, SubMenu addResidueSubMenu) {
        for (String residueSuperClass : ResidueDictionary.getSuperclasses()) {
            // If super class is reducing end or modification, the process continue.
            if (residueSuperClass.equals("Reducing end")) continue;

            // Creates each residue menu.
            MenuItem residueClassMenu = addResidueSubMenu.addItem(residueSuperClass);
            SubMenu residueClassSubMenu = residueClassMenu.getSubMenu();
            if (residueSuperClass.equals("Substituent")) {
                for (SubstituentTypeDescriptor substitunetTypeDescriptor : SubstituentTypeDescriptor.getTypeList()) {
                    MenuItem substituentClassMenu = residueClassSubMenu.addItem(substitunetTypeDescriptor.getClassName());
                    SubMenu substituentClassSubMenu = substituentClassMenu.getSubMenu();
                    for (ResidueType residueType : ResidueDictionary.getResidues(residueSuperClass)) {
                        if (residueType.canHaveParent() && substitunetTypeDescriptor.equals(SubstituentTypeDescriptor.forClass(residueType.getCompositionClass()))) {
                            addResidueMenu(substituentClassSubMenu, residueType, glycanWebCanvas);
                        }
                    }
                }
            } else {
                for (ResidueType residueType : ResidueDictionary.getResidues(residueSuperClass)) {
                    if (residueType.canHaveParent()) {
                        addResidueMenu(residueClassSubMenu, residueType, glycanWebCanvas);
                    }
                }
            }
        }
    }

    /**
     * Adds residue menu to draw a residue.
     * @param subMenu Sub menu of add residue.
     * @param residueType Registered residue type to menu.
     * @param glycanWebCanvas Canvas drawn residues and linkages.
     */
    private void addResidueMenu(SubMenu subMenu, ResidueType residueType, GlycanWebCanvas glycanWebCanvas) {
        try {
            HorizontalLayout dummyMenuHorizontalLayout = new HorizontalLayout();
            MenuItem dummyMenuItem = subMenu.addItem(dummyMenuHorizontalLayout);
            ResidueIconCreator residueIconCreator = new ResidueIconCreator();
            String notation = glycanWebCanvas.getBuilderWorkspace().getGraphicOptions().NOTATION;
            residueIconCreator.createResidueIcon(dummyMenuHorizontalLayout, notation, "#898989", "#00000000", residueType, 24, image -> {
                HorizontalLayout menuHorizontalLayout = new HorizontalLayout();
                image.getStyle().set("margin-right", "8px");
                menuHorizontalLayout.add(image);
                menuHorizontalLayout.add(residueType.getName());
                subMenu.addItem(menuHorizontalLayout, mouseClickEvent -> {
                    glycanWebCanvas.addResidue(residueType);
                });
                subMenu.remove(dummyMenuItem);
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Creates add terminal menu to add template terminal.
     * @param glycanWebCanvas Canvas drawn residues and linkages.
     */
    public void createAddTerminalMenu(GlycanWebCanvas glycanWebCanvas) {
        // Creates add structure menu.
        MenuItem addTerminalMenu = terminalSubMenu.addItem("Add terminal");
        SubMenu addTerminalSubMenu = addTerminalMenu.getSubMenu();
        for (String terminalSuperclass : TerminalDictionary.getSuperclasses()) {
            MenuItem terminalClassMenu = addTerminalSubMenu.addItem(terminalSuperclass);
            SubMenu terminalClassSubMenu = terminalClassMenu.getSubMenu();
            for (TerminalType terminalType : TerminalDictionary.getTerminals(terminalSuperclass)) {
                // Creates menu of terminal.
                String terminalDescription = terminalType.getDescription();
                MenuItem terminalDescriptionMenu = terminalClassSubMenu.addItem(terminalDescription);
                SubMenu terminalDescriptionSubMenu = terminalDescriptionMenu.getSubMenu();

                // Creates menu of unknown and link type of terminal.
                String terminalName = terminalType.getName();
                terminalDescriptionSubMenu.addItem("Unknown linkage", mouseClickEvent -> {
                    glycanWebCanvas.addTerminal(terminalName);
                });
                for (int i = 1; i < 9; i++) {
                    terminalDescriptionSubMenu.addItem(i + "-linked", mouseClickEvent -> {
                        glycanWebCanvas.addTerminal(terminalName);
                    });
                    
                }
            }
        }
    }

    /**
     * Creates change reducing end type to change reducing end.
     * @param glycanWebCanvas Canvas drawn residues and linkages.
     */
    public void createChangeReducingEndTypeMenu(GlycanWebCanvas glycanWebCanvas) {
        MenuItem addReducingEndTypeMenu = reducingEndTypeMenu.addItem("Change reducing end type");
        for(ResidueType reducingEnd : ResidueDictionary.getReducingEnds()) {
            SubMenu addReducingEndTypeSubMenu = addReducingEndTypeMenu.getSubMenu();
            HorizontalLayout dummyMenuHorizontalLayout = new HorizontalLayout();
            MenuItem dummyMenuItem = addReducingEndTypeSubMenu.addItem(dummyMenuHorizontalLayout);
            ResidueIconCreator residueIconCreator = new ResidueIconCreator();
            String notation = glycanWebCanvas.getBuilderWorkspace().getGraphicOptions().NOTATION;
            residueIconCreator.createResidueIcon(dummyMenuHorizontalLayout, notation, "#898989", "#00000000", reducingEnd, 24, image -> {
                HorizontalLayout menuHorizontalLayout = new HorizontalLayout();
                image.getStyle().set("margin-right", "8px");
                menuHorizontalLayout.add(image);
                menuHorizontalLayout.add(reducingEnd.getDescription());
                addReducingEndTypeSubMenu.addItem(menuHorizontalLayout, mouseClickEvent -> {
                    glycanWebCanvas.changeReducingEndType(reducingEnd.getName());
                });
                addReducingEndTypeSubMenu.remove(dummyMenuItem);
            });
        }
    }

    /**
     * Changes notation of structure.
     * @param glycanWebCanvas Canvas drawn residues and linkages.
     */
     @Override
     public void changeNotation(GlycanWebCanvas glycanWebCanvas) {
         addResidueMenu.removeAll();
         reducingEndTypeMenu.removeAll();
         compositionSubMenu.removeAll();
         structureSubMenu.removeAll();
         addResidueMenu.removeAll();
         terminalSubMenu.removeAll();
         reducingEndTypeMenu.removeAll();
         createAddComposition(glycanWebCanvas);
         createAddStructureMenu(glycanWebCanvas);
         createAddResidueMenu(glycanWebCanvas);
         createAddTerminalMenu(glycanWebCanvas);
         createChangeReducingEndTypeMenu(glycanWebCanvas);
     }
}
