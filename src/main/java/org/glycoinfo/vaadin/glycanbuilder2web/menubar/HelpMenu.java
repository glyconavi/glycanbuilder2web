package org.glycoinfo.vaadin.glycanbuilder2web.menubar;

import org.glycoinfo.vaadin.glycanbuilder2web.dialog.AboutDialog;

import com.vaadin.flow.component.contextmenu.MenuItem;
import com.vaadin.flow.component.menubar.MenuBar;

/**
 * Help menu to explain Glycan Builder 2 Web.
 */
public class HelpMenu {
    /**
     * Initializes help menu.
     * @param glycanMenuBar Menu bar for Glycan Builder 2.
     */
    public HelpMenu(MenuBar glycanMenuBar) {
        MenuItem helpMenuItem = glycanMenuBar.addItem("Help");
        helpMenuItem.getSubMenu().addItem("About", mouseClickEvent -> {
            new AboutDialog();
        });
    }
}
