package org.glycoinfo.vaadin.glycanbuilder2web.menubar;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Map;

import org.eurocarbdb.application.glycanbuilder.renderutil.SVGUtils;
import org.glycoinfo.vaadin.glycanbuilder2web.canvas.GlycanWebCanvas;
import org.glycoinfo.vaadin.glycanbuilder2web.dialog.ImageDialog;
import org.glycoinfo.vaadin.glycanbuilder2web.dialog.SVGDialog;
import org.glycoinfo.vaadin.glycanbuilder2web.util.HTTP;
import org.glycoinfo.vaadin.glycanbuilder2web.util.WURCSConverter;

import com.vaadin.flow.component.contextmenu.MenuItem;
import com.vaadin.flow.component.contextmenu.SubMenu;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.menubar.MenuBar;
import com.vaadin.flow.server.StreamResource;

/**
 * File menu bar related file.
 */
public class FileMenu {
    /**
     * Sub menu to export structure to image.
     */
    private SubMenu exportToImageSubMenu;

    /**
     * Initializes file menu.
     * @param glycanMenuBar Menu bar for Glycan Builder 2.
     */
    public FileMenu(MenuBar glycanMenuBar) {
        MenuItem fileMenuItem = glycanMenuBar.addItem("File");
        exportToImageSubMenu = fileMenuItem.getSubMenu();
    }

    /**
     * Creates add export to image menu to export structures to image.
     * @param glycanWebCanvas Canvas drawn residues and linkages.
     */
    public void createExportToImage(GlycanWebCanvas glycanWebCanvas) {
        String[] validFormats = { "png", };
        for (Map.Entry<String,String> exportFormat : SVGUtils.getExportFormats().entrySet()) {
            for (String validFormat : validFormats) {
                if (!validFormat.equals(exportFormat.getKey())) continue;
                exportToImageSubMenu.addItem("Export to " + exportFormat.getValue(), mouseClickEvent -> {
                    // Retains values before changing canvas size.
                    boolean showMassesCanvas = glycanWebCanvas.getBuilderWorkspace().getGraphicOptions().SHOW_MASSES_CANVAS;
                    boolean showRedEndCanvas = glycanWebCanvas.getBuilderWorkspace().getGraphicOptions().SHOW_REDEND_CANVAS;

                    // Changes canvas size.
                    int height = glycanWebCanvas.getCanvasHeight();
                    glycanWebCanvas.setIsFullSize(false);
                    glycanWebCanvas.getBuilderWorkspace().getGraphicOptions().SHOW_MASSES_CANVAS = glycanWebCanvas.getBuilderWorkspace().getGraphicOptions().SHOW_MASSES;
                    glycanWebCanvas.getBuilderWorkspace().getGraphicOptions().SHOW_REDEND_CANVAS = glycanWebCanvas.getBuilderWorkspace().getGraphicOptions().SHOW_REDEND;
                    glycanWebCanvas.updateCanvas();

                    // Converts canvas to image.
                    String format = exportFormat.getKey().equals("svg") ? "image/" + exportFormat.getKey() + "+xml" : "image/" + exportFormat.getKey();
                    glycanWebCanvas.toDataURL(format, dataURI -> {
                        // Calls image dialog to display image.
                        String dataURIBase64 = dataURI.replace("data:image/" + exportFormat.getKey() + ";base64,", "");
                        byte[] imageBytes = Base64.getDecoder().decode(dataURIBase64.getBytes());
                        StreamResource streamResource = new StreamResource("structures.png", () -> new ByteArrayInputStream(imageBytes));
                        new ImageDialog(new Image(streamResource, ""), streamResource);

                        // Reverts to previous canvas size.
                        glycanWebCanvas.getBuilderWorkspace().getGraphicOptions().SHOW_MASSES_CANVAS = showMassesCanvas;
                        glycanWebCanvas.getBuilderWorkspace().getGraphicOptions().SHOW_REDEND_CANVAS = showRedEndCanvas;
                        glycanWebCanvas.getElement().setAttribute("height", String.valueOf(height));
                        glycanWebCanvas.setIsFullSize(true);
                        glycanWebCanvas.updateCanvas();
                    });
                });
            }
        }
    }

    /**
     * Creates add export to svg menu to export structures to svg.
     * @param glycanWebCanvas Canvas drawn residues and linkages.
     */
    public void createExportToSVG(GlycanWebCanvas glycanWebCanvas) {
        exportToImageSubMenu.addItem("Export to SVG", mouseClickEvent -> {
            try {
                String svgURL = String.format("https://api.glycosmos.org/wurcs2image/0.10.0/svg/html/%s", URLEncoder.encode(WURCSConverter.getWURCSFromFirstStructure(glycanWebCanvas), "UTF-8"));
                String svg = HTTP.httpGet(svgURL);
                StreamResource streamResource = new StreamResource("structures.svg", () -> new ByteArrayInputStream(svg.getBytes(StandardCharsets.UTF_8)));
                new SVGDialog(svgURL, streamResource);
            } catch (IOException e) {
            }
        });
    }
}
