package org.glycoinfo.vaadin.glycanbuilder2web.menubar;

import org.eurocarbdb.application.glycanbuilder.util.GraphicOptions;
import org.glycoinfo.vaadin.glycanbuilder2web.canvas.GlycanWebCanvas;
import org.glycoinfo.vaadin.glycanbuilder2web.dialog.DisplaySettingsDialog;

import com.vaadin.flow.component.contextmenu.MenuItem;
import com.vaadin.flow.component.contextmenu.SubMenu;
import com.vaadin.flow.component.menubar.MenuBar;

/**
 * View menu bar to change display related functions.
 */
public class ViewMenu {
    /**
     * Sub menu of view.
     */
    private SubMenu viewSubMenu;

    /**
     * Zoom menu to zoom to scale rate 400.
     */
    private MenuItem zoomMenuTo400;

    /**
     * Zoom menu to zoom to scale rate 300.
     */
    private MenuItem zoomMenuTo300;

    /**
     * Zoom menu to zoom to scale rate 200.
     */
    private MenuItem zoomMenuTo200;

    /**
     * Zoom menu to zoom to scale rate 150.
     */
    private MenuItem zoomMenuTo150;

    /**
     * Zoom menu to zoom to scale rate 100.
     */
    private MenuItem zoomMenuTo100;

    /**
     * Zoom menu to zoom to scale rate 67.
     */
    private MenuItem zoomMenuTo67;

    /**
     * Zoom menu to zoom to scale rate 50.
     */
    private MenuItem zoomMenuTo50;

    /**
     * Zoom menu to zoom to scale rate 33.
     */
    private MenuItem zoomMenuTo33;

    /**
     * Zoom menu to zoom to scale rate 25.
     */
    private MenuItem zoomMenuTo25;

    /**
     * CFG notation menu to change CFG notation.
     */
    private MenuItem cfgNotationMenu;

    /**
     * CFG black and white notation menu to change CFG black and white notation.
     */
    private MenuItem cfgBlackAndWhiteNotationMenu;

    /**
     * CFG white linkage placement notation menu to change CFG white linkage placement notation.
     */
    private MenuItem cfgWithLinkagePlacementNotationMenu;

    /**
     * UOXF notation menu to change UOXF notation.
     */
    private MenuItem uoxfNotationMenu;

    /**
     * UOXF COL notation menu to change UOXF COL notation.
     */
    private MenuItem uoxfCOLNotationMenu;

    /**
     * Text only notation menu to change Text only notation.
     */
    private MenuItem textOnlyNotationMenu;

    /**
     * SNFG notation menu to change SNFG notation.
     */
    private MenuItem snfgNotationMenu;

    /**
     * Compact view menu to change structures to style of compact view.
     */
    private MenuItem compactViewMenu;

    /**
     * Normal view menu to change structures to syle of normal view.
     */
    private MenuItem normalViewMenu;

    /**
     * Normal view with linkage info menu to change structures to style of normal view with linkage info.
     */
    private MenuItem normalViewWithLinkageInfoMenu;

    /**
     * Menu to enable function that multiple fragments toggle between displaying them separately or combining them.
     */
    private MenuItem collapseMultipleAntennaMenu;

    /**
     * Menu to enable function that show mass value for each glycan.
     */
    private MenuItem showMassesInTheDrawingCanvasMenu;

    /**
     * Menu to enable function that output mass value for each glycan structure when exporting into any graphic file.
     */
    private MenuItem showMassesWhenExportingMenu;

    /**
     * Menu to enable function that display the reducing end symbol for each glycan structure.
     */
    private MenuItem showReducingEndIndicatorInTheDrawingCanvasMenu;

    /**
     * Menu to enable function that output the symbol of the reducing end when exporting into any graphic file.
     */
    private MenuItem showReducingEndIndicatorWhenExportingMenu;

    /**
     * Initializes view menu.
     * @param glycanMenuBar Menu bar for Glycan Builder 2.
     */
    public ViewMenu(MenuBar glycanMenuBar) {
        MenuItem viewMenuItem = glycanMenuBar.addItem("View");
        viewSubMenu = viewMenuItem.getSubMenu();
    }

    /**
     * Creates zoom menu to zoom structure.
     * @param glycanWebCanvas Canvas drawn residues and linkages.
     */
    public void createZoomMenu(GlycanWebCanvas glycanWebCanvas) {
        MenuItem zoomMenu = viewSubMenu.addItem("Zoom");

        // Sets zoom menu to zoom to scale rate 400.
        zoomMenuTo400 = zoomMenu.getSubMenu().addItem("400%", mouseClickEvent -> {
            changeScaleRateTo400(glycanWebCanvas);
        });
        zoomMenuTo400.setCheckable(true);

        // Sets zoom menu to zoom to scale rate 300.
        zoomMenuTo300 = zoomMenu.getSubMenu().addItem("300%", mouseClickEvent -> {
            changeScaleRateTo300(glycanWebCanvas);
        });
        zoomMenuTo300.setCheckable(true);

        // Sets zoom menu to zoom to scale rate 200.
        zoomMenuTo200 = zoomMenu.getSubMenu().addItem("200%", mouseClickEvent -> {
            changeScaleRateTo200(glycanWebCanvas);
        });
        zoomMenuTo200.setCheckable(true);

        // Sets zoom menu to zoom to scale rate 150.
        zoomMenuTo150 = zoomMenu.getSubMenu().addItem("150%", mouseClickEvent -> {
            changeScaleRateTo150(glycanWebCanvas);
        });
        zoomMenuTo150.setCheckable(true);

        // Sets zoom menu to zoom to scale rate 100.
        zoomMenuTo100 = zoomMenu.getSubMenu().addItem("100%", mouseClickEvent -> {
            changeScaleRateTo100(glycanWebCanvas);
        });
        zoomMenuTo100.setCheckable(true);
        zoomMenuTo100.setChecked(true);

        // Sets zoom menu to zoom to scale rate 67.
        zoomMenuTo67 = zoomMenu.getSubMenu().addItem("67%", mouseClickEvent -> {
            changeScaleRateTo67(glycanWebCanvas);
        });
        zoomMenuTo67.setCheckable(true);

        // Sets zoom menu to zoom to scale rate 50.
        zoomMenuTo50 = zoomMenu.getSubMenu().addItem("50%", mouseClickEvent -> {
            changeScaleRateTo50(glycanWebCanvas);
        });
        zoomMenuTo50.setCheckable(true);

        // Sets zoom menu to zoom to scale rate 33.
        zoomMenuTo33 = zoomMenu.getSubMenu().addItem("33%", mouseClickEvent -> {
            changeScaleRateTo33(glycanWebCanvas);
        });
        zoomMenuTo33.setCheckable(true);

        // Sets zoom menu to zoom to scale rate 25.
        zoomMenuTo25 = zoomMenu.getSubMenu().addItem("25%", mouseClickEvent -> {
            changeScaleRateTo25(glycanWebCanvas);
        });
        zoomMenuTo25.setCheckable(true);
    }

    /**
     * Changes scale rate to 400%.
     * @param glycanWebCanvas Canvas drawn residues and linkages.
     */
    public void changeScaleRateTo400(GlycanWebCanvas glycanWebCanvas) {
        glycanWebCanvas.changeScaleRate(4d);
        zoomMenuTo400.setChecked(true);
        zoomMenuTo300.setChecked(false);
        zoomMenuTo200.setChecked(false);
        zoomMenuTo150.setChecked(false);
        zoomMenuTo100.setChecked(false);
        zoomMenuTo67.setChecked(false);
        zoomMenuTo50.setChecked(false);
        zoomMenuTo33.setChecked(false);
        zoomMenuTo25.setChecked(false);
    }

    /**
     * Changes scale rate to 300%.
     * @param glycanWebCanvas Canvas drawn residues and linkages.
     */
    public void changeScaleRateTo300(GlycanWebCanvas glycanWebCanvas) {
        glycanWebCanvas.changeScaleRate(3d);
        zoomMenuTo400.setChecked(false);
        zoomMenuTo300.setChecked(true);
        zoomMenuTo200.setChecked(false);
        zoomMenuTo150.setChecked(false);
        zoomMenuTo100.setChecked(false);
        zoomMenuTo67.setChecked(false);
        zoomMenuTo50.setChecked(false);
        zoomMenuTo33.setChecked(false);
        zoomMenuTo25.setChecked(false);
    }

    /**
     * Changes scale rate to 200%.
     * @param glycanWebCanvas Canvas drawn residues and linkages.
     */
    public void changeScaleRateTo200(GlycanWebCanvas glycanWebCanvas) {
        glycanWebCanvas.changeScaleRate(2d);
        zoomMenuTo400.setChecked(false);
        zoomMenuTo300.setChecked(false);
        zoomMenuTo200.setChecked(true);
        zoomMenuTo150.setChecked(false);
        zoomMenuTo100.setChecked(false);
        zoomMenuTo67.setChecked(false);
        zoomMenuTo50.setChecked(false);
        zoomMenuTo33.setChecked(false);
        zoomMenuTo25.setChecked(false);
    }

    /**
     * Changes scale rate to 150%.
     * @param glycanWebCanvas Canvas drawn residues and linkages.
     */
    public void changeScaleRateTo150(GlycanWebCanvas glycanWebCanvas) {
        glycanWebCanvas.changeScaleRate(1.5d);
        zoomMenuTo400.setChecked(false);
        zoomMenuTo300.setChecked(false);
        zoomMenuTo200.setChecked(false);
        zoomMenuTo150.setChecked(true);
        zoomMenuTo100.setChecked(false);
        zoomMenuTo67.setChecked(false);
        zoomMenuTo50.setChecked(false);
        zoomMenuTo33.setChecked(false);
        zoomMenuTo25.setChecked(false);
    }

    /**
     * Changes scale rate to 100%.
     * @param glycanWebCanvas Canvas drawn residues and linkages.
     */
    public void changeScaleRateTo100(GlycanWebCanvas glycanWebCanvas) {
        glycanWebCanvas.changeScaleRate(1d);
        zoomMenuTo400.setChecked(false);
        zoomMenuTo300.setChecked(false);
        zoomMenuTo200.setChecked(false);
        zoomMenuTo150.setChecked(false);
        zoomMenuTo100.setChecked(true);
        zoomMenuTo67.setChecked(false);
        zoomMenuTo50.setChecked(false);
        zoomMenuTo33.setChecked(false);
        zoomMenuTo25.setChecked(false);
    }

    /**
     * Changes scale rate to 67%.
     * @param glycanWebCanvas Canvas drawn residues and linkages.
     */
    public void changeScaleRateTo67(GlycanWebCanvas glycanWebCanvas) {
        glycanWebCanvas.changeScaleRate(0.67d);
        zoomMenuTo400.setChecked(false);
        zoomMenuTo300.setChecked(false);
        zoomMenuTo200.setChecked(false);
        zoomMenuTo150.setChecked(false);
        zoomMenuTo100.setChecked(false);
        zoomMenuTo67.setChecked(true);
        zoomMenuTo50.setChecked(false);
        zoomMenuTo33.setChecked(false);
        zoomMenuTo25.setChecked(false);
    }

    /**
     * Changes scale rate to 50%.
     * @param glycanWebCanvas Canvas drawn residues and linkages.
     */
    public void changeScaleRateTo50(GlycanWebCanvas glycanWebCanvas) {
        glycanWebCanvas.changeScaleRate(0.5d);
        zoomMenuTo400.setChecked(false);
        zoomMenuTo300.setChecked(false);
        zoomMenuTo200.setChecked(false);
        zoomMenuTo150.setChecked(false);
        zoomMenuTo100.setChecked(false);
        zoomMenuTo67.setChecked(false);
        zoomMenuTo50.setChecked(true);
        zoomMenuTo33.setChecked(false);
        zoomMenuTo25.setChecked(false);
    }

    /**
     * Changes scale rate to 33%.
     * @param glycanWebCanvas Canvas drawn residues and linkages.
     */
    public void changeScaleRateTo33(GlycanWebCanvas glycanWebCanvas) {
        glycanWebCanvas.changeScaleRate(0.33d);
        zoomMenuTo400.setChecked(false);
        zoomMenuTo300.setChecked(false);
        zoomMenuTo200.setChecked(false);
        zoomMenuTo150.setChecked(false);
        zoomMenuTo100.setChecked(false);
        zoomMenuTo67.setChecked(false);
        zoomMenuTo50.setChecked(false);
        zoomMenuTo33.setChecked(true);
        zoomMenuTo25.setChecked(false);
    }

    /**
     * Changes scale rate to 25%.
     * @param glycanWebCanvas Canvas drawn residues and linkages.
     */
    public void changeScaleRateTo25(GlycanWebCanvas glycanWebCanvas) {
        glycanWebCanvas.changeScaleRate(0.25d);
        zoomMenuTo400.setChecked(false);
        zoomMenuTo300.setChecked(false);
        zoomMenuTo200.setChecked(false);
        zoomMenuTo150.setChecked(false);
        zoomMenuTo100.setChecked(false);
        zoomMenuTo67.setChecked(false);
        zoomMenuTo50.setChecked(false);
        zoomMenuTo33.setChecked(false);
        zoomMenuTo25.setChecked(true);
    }

    /**
     * Creates change notation menu.
     * @param glycanWebCanvas Canvas drawn residues and linkages.
     */
    public void createChangeNotationMenu(GlycanWebCanvas glycanWebCanvas) {
        // Sets CFG notation menu to change CFG notation.
        cfgNotationMenu = viewSubMenu.addItem("CFG notation", mouseClickEvent -> {
            glycanWebCanvas.changeNotation(GraphicOptions.NOTATION_CFG);
            cfgNotationMenu.setChecked(true);
            cfgBlackAndWhiteNotationMenu.setChecked(false);
            cfgWithLinkagePlacementNotationMenu.setChecked(false);
            uoxfNotationMenu.setChecked(false);
            uoxfCOLNotationMenu.setChecked(false);
            textOnlyNotationMenu.setChecked(false);
            snfgNotationMenu.setChecked(false);
        });
        cfgNotationMenu.setCheckable(true);

        // Sets CFG black and white notation menu to change CFG black and white notation.
        cfgBlackAndWhiteNotationMenu = viewSubMenu.addItem("CFG black and white notation", mouseClickEvent -> {
            glycanWebCanvas.changeNotation(GraphicOptions.NOTATION_CFGBW);
            cfgNotationMenu.setChecked(false);
            cfgBlackAndWhiteNotationMenu.setChecked(true);
            cfgWithLinkagePlacementNotationMenu.setChecked(false);
            uoxfNotationMenu.setChecked(false);
            uoxfCOLNotationMenu.setChecked(false);
            textOnlyNotationMenu.setChecked(false);
            snfgNotationMenu.setChecked(false);
        });
        cfgBlackAndWhiteNotationMenu.setCheckable(true);

        // Sets CFG white linkage placement notation menu to change CFG white linkage placement notation.
        cfgWithLinkagePlacementNotationMenu = viewSubMenu.addItem("CFG with linkage placement notation", mouseClickEvent -> {
            glycanWebCanvas.changeNotation(GraphicOptions.NOTATION_CFGLINK);
            cfgNotationMenu.setChecked(false);
            cfgBlackAndWhiteNotationMenu.setChecked(false);
            cfgWithLinkagePlacementNotationMenu.setChecked(true);
            uoxfNotationMenu.setChecked(false);
            uoxfCOLNotationMenu.setChecked(false);
            textOnlyNotationMenu.setChecked(false);
            snfgNotationMenu.setChecked(false);
        });
        cfgWithLinkagePlacementNotationMenu.setCheckable(true);

        // Sets UOXF notation menu to change UOXF notation.
        uoxfNotationMenu = viewSubMenu.addItem("UOXF notation", mouseClickEvent -> {
            glycanWebCanvas.changeNotation(GraphicOptions.NOTATION_UOXF);
            cfgNotationMenu.setChecked(false);
            cfgBlackAndWhiteNotationMenu.setChecked(false);
            cfgWithLinkagePlacementNotationMenu.setChecked(false);
            uoxfNotationMenu.setChecked(true);
            uoxfCOLNotationMenu.setChecked(false);
            textOnlyNotationMenu.setChecked(false);
            snfgNotationMenu.setChecked(false);
        });
        uoxfNotationMenu.setCheckable(true);

        // Sets UOXF COL notation menu to change UOXF COL notation.
        uoxfCOLNotationMenu = viewSubMenu.addItem("UOXFCOL notation", mouseClickEvent -> {
            glycanWebCanvas.changeNotation(GraphicOptions.NOTATION_UOXFCOL);
            cfgNotationMenu.setChecked(false);
            cfgBlackAndWhiteNotationMenu.setChecked(false);
            cfgWithLinkagePlacementNotationMenu.setChecked(false);
            uoxfNotationMenu.setChecked(false);
            uoxfCOLNotationMenu.setChecked(true);
            textOnlyNotationMenu.setChecked(false);
            snfgNotationMenu.setChecked(false);
        });
        uoxfCOLNotationMenu.setCheckable(true);

        // Sets Text only notation menu to change Text only notation.
        textOnlyNotationMenu = viewSubMenu.addItem("text", mouseClickEvent -> {
            glycanWebCanvas.changeNotation(GraphicOptions.NOTATION_TEXT);
            cfgNotationMenu.setChecked(false);
            cfgBlackAndWhiteNotationMenu.setChecked(false);
            cfgWithLinkagePlacementNotationMenu.setChecked(false);
            uoxfNotationMenu.setChecked(false);
            uoxfCOLNotationMenu.setChecked(false);
            textOnlyNotationMenu.setChecked(true);
            snfgNotationMenu.setChecked(false);
        });
        textOnlyNotationMenu.setCheckable(true);

        // Sets SNFG notation menu to change SNFG notation.
        snfgNotationMenu = viewSubMenu.addItem("SNFG notation", mouseClickEvent -> {
            glycanWebCanvas.changeNotation(GraphicOptions.NOTATION_SNFG);
            cfgNotationMenu.setChecked(false);
            cfgBlackAndWhiteNotationMenu.setChecked(false);
            cfgWithLinkagePlacementNotationMenu.setChecked(false);
            uoxfNotationMenu.setChecked(false);
            uoxfCOLNotationMenu.setChecked(false);
            textOnlyNotationMenu.setChecked(false);
            snfgNotationMenu.setChecked(true);
        });
        snfgNotationMenu.setCheckable(true);
        snfgNotationMenu.setChecked(true);
    }

    /**
     * Creates view setting menu to change display style of structure.
     * @param glycanWebCanvas Canvas drawn residues and linkages.
     */
    public void createStructureViewSettingMenu(GlycanWebCanvas glycanWebCanvas) {
        // Sets compact view menu.
        compactViewMenu = viewSubMenu.addItem("compact view", mouseClickEvent -> {
            changeToCompactView(glycanWebCanvas);
        });
        compactViewMenu.setCheckable(true);

        // Sets normal view menu.
        normalViewMenu = viewSubMenu.addItem("normal view", mouseClickEvent -> {
            glycanWebCanvas.changeStructureView(GraphicOptions.DISPLAY_NORMAL);
            compactViewMenu.setChecked(false);
            normalViewMenu.setChecked(true);
            normalViewWithLinkageInfoMenu.setChecked(false);
        });
        normalViewMenu.setCheckable(true);

        // Sets normal view with linkage info menu.
        normalViewWithLinkageInfoMenu = viewSubMenu.addItem("normal view with linkage info", mouseClickEvent -> {
            changeToNormalViewWithLinkageInfo(glycanWebCanvas);
        });
        normalViewWithLinkageInfoMenu.setCheckable(true);
        normalViewWithLinkageInfoMenu.setChecked(true);
    }

    /**
     * Changes to compact view.
     * @param glycanWebCanvas Canvas drawn residues and linkages.
     */
    public void changeToCompactView(GlycanWebCanvas glycanWebCanvas) {
        glycanWebCanvas.changeStructureView(GraphicOptions.DISPLAY_COMPACT);
        compactViewMenu.setChecked(true);
        normalViewMenu.setChecked(false);
        normalViewWithLinkageInfoMenu.setChecked(false);
    }

    /**
     * Changes to view with linkage info.
     * @param glycanWebCanvas Canvas drawn residues and linkages.
     */
    public void changeToNormalViewWithLinkageInfo(GlycanWebCanvas glycanWebCanvas) {
        glycanWebCanvas.changeStructureView(GraphicOptions.DISPLAY_NORMALINFO);
        compactViewMenu.setChecked(false);
        normalViewMenu.setChecked(false);
        normalViewWithLinkageInfoMenu.setChecked(true);
    }

    /**
     * Creates view setting menu to change display style of canvas.
     * @param glycanWebCanvas Canvas drawn residues and linkages.
     */
    public void createCanvasViewSettingMenu(GlycanWebCanvas glycanWebCanvas) {
        // Sets collapse multiple antenna menu.
        collapseMultipleAntennaMenu = viewSubMenu.addItem("Collapse multiple antenna", mouseClickEvent -> {
            glycanWebCanvas.collapseMultipleAntenna(collapseMultipleAntennaMenu.isChecked());
        });
        collapseMultipleAntennaMenu.setCheckable(true);
        collapseMultipleAntennaMenu.setChecked(glycanWebCanvas.getBuilderWorkspace().getGraphicOptions().COLLAPSE_MULTIPLE_ANTENNAE);

        // Sets show masses in the drawing canvas menu.
        showMassesInTheDrawingCanvasMenu = viewSubMenu.addItem("Show masses in the drawing canvas", mouseClickEvent -> {
            glycanWebCanvas.showMassesInDrawingCanvas(showMassesInTheDrawingCanvasMenu.isChecked());
        });
        showMassesInTheDrawingCanvasMenu.setCheckable(true);
        showMassesInTheDrawingCanvasMenu.setChecked(glycanWebCanvas.getBuilderWorkspace().getGraphicOptions().SHOW_MASSES_CANVAS);

        // Sets show masses when exporting menu.
        showMassesWhenExportingMenu = viewSubMenu.addItem("Show masses when exporting", mouseClickEvent -> {
            glycanWebCanvas.showMassesWhenExporting(showMassesWhenExportingMenu.isChecked());
        });
        showMassesWhenExportingMenu.setCheckable(true);
        showMassesWhenExportingMenu.setChecked(glycanWebCanvas.getBuilderWorkspace().getGraphicOptions().SHOW_MASSES);

        // Sets show reducing end indicator in the drawing canvas menu.
        showReducingEndIndicatorInTheDrawingCanvasMenu = viewSubMenu.addItem("Show reducing end indicator in the drawing canvas", mouseClickEvent -> {
            glycanWebCanvas.showReducingEndIndicatorInDrawingCanvas(showReducingEndIndicatorInTheDrawingCanvasMenu.isChecked());
        });
        showReducingEndIndicatorInTheDrawingCanvasMenu.setCheckable(true);
        showReducingEndIndicatorInTheDrawingCanvasMenu.setChecked(glycanWebCanvas.getBuilderWorkspace().getGraphicOptions().SHOW_REDEND_CANVAS);

        // Sets show reducing end indicator when exporting menu.
        showReducingEndIndicatorWhenExportingMenu = viewSubMenu.addItem("Show reducing end indicator when exporting", mouseClickEvent -> {
            glycanWebCanvas.showReducingEndIndicatorWhenExporting(showReducingEndIndicatorWhenExportingMenu.isChecked());
        });
        showReducingEndIndicatorWhenExportingMenu.setCheckable(true);
        showReducingEndIndicatorWhenExportingMenu.setChecked(glycanWebCanvas.getBuilderWorkspace().getGraphicOptions().SHOW_REDEND);
    }

    /**
     * Shows / hides masses in the drawing canvas.
     * @param glycanWebCanvas Canvas drawn residues and linkages.
     */
    public void showHideMassesInTheDrawingCanvas(GlycanWebCanvas glycanWebCanvas, boolean isShowing) {
        glycanWebCanvas.showMassesInDrawingCanvas(isShowing);
        showMassesInTheDrawingCanvasMenu.setChecked(isShowing);
    }

    /**
     * Creates display setting menu.
     * @param glycanWebCanvas Canvas drawn residues and linkages.
     */
    public void createDisplaySettingsMenu(GlycanWebCanvas glycanWebCanvas) {
        viewSubMenu.addItem("Change display settings", mouseClickEvent -> {
            new DisplaySettingsDialog(glycanWebCanvas, glycanWebCanvas.getBuilderWorkspace().getGraphicOptions());
        });
    }
}
