package org.glycoinfo.vaadin.glycanbuilder2web.menubar;

import org.glycoinfo.vaadin.glycanbuilder2web.canvas.GlycanWebCanvas;

import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.littemplate.LitTemplate;
import com.vaadin.flow.component.menubar.MenuBar;
import com.vaadin.flow.component.template.Id;

/**
 * Menu bar for Glycan Builder 2.
 */
@Tag("glycan-menu-bar")
@JsModule("./src/glycan-menu-bar.ts")
@SuppressWarnings("serial")
public class GlycanMenuBar extends LitTemplate {
    /**
     * Menu bar for Glycan Builder 2.
     */
    @Id("glycanMenuBar")
    private MenuBar glycanMenuBar;

    /**
     * Menu of structure.
     */
    private StructureMenu structureMenu;

    /**
     * Menu of view.
     */
    private ViewMenu viewMenu;

    /**
     * Creates glycan menu bar to call some functions.
     * @param glycanWebCanvas Canvas drawn residues and linkages.
     */
    public void createGlycanMenuBar(GlycanWebCanvas glycanWebCanvas) {
        // Creates file menu.
        FileMenu filemenu = new FileMenu(glycanMenuBar);
        filemenu.createExportToImage(glycanWebCanvas);
        filemenu.createExportToSVG(glycanWebCanvas);

        // Creates structure menu.
        structureMenu = new StructureMenu(glycanMenuBar);
        structureMenu.createAddComposition(glycanWebCanvas);
        structureMenu.createAddStructureMenu(glycanWebCanvas);
        structureMenu.createAddResidueMenu(glycanWebCanvas);
        structureMenu.createAddTerminalMenu(glycanWebCanvas);
        structureMenu.createChangeReducingEndTypeMenu(glycanWebCanvas);

        // Creates view menu.
        viewMenu = new ViewMenu(glycanMenuBar);
        viewMenu.createZoomMenu(glycanWebCanvas);
        viewMenu.createChangeNotationMenu(glycanWebCanvas);
        viewMenu.createStructureViewSettingMenu(glycanWebCanvas);
        viewMenu.createCanvasViewSettingMenu(glycanWebCanvas);
        viewMenu.createDisplaySettingsMenu(glycanWebCanvas);

        // Creates help menu.
        new HelpMenu(glycanMenuBar);
    }

    /**
     * Gets menu of structure.
     * @return Returns menu of structure.
     */
    public StructureMenu getStructureMenu() {
        return structureMenu;
    }

    /**
     * Gets menu of view.
     * @return
     */
    public ViewMenu getViewMenu() {
        return viewMenu;
    }
}
