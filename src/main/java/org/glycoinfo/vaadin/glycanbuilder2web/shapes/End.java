package org.glycoinfo.vaadin.glycanbuilder2web.shapes;

import org.eurocarbdb.application.glycanbuilder.ResidueStyle;
import org.glycoinfo.vaadin.WebCanvasRenderingContext2D;

/**
 * Drawing end class.
 */
public class End extends BaseShape {
    /**
     * Stroke style for end.
     */
    public String strokeStyleOfEnd = null;

    /**
     * Angle of shape.
     */
    public double angle;

    /**
     * 
     * @param angle Angle of shape.
     * @param x X coordinate of the shape.
     * @param y Y coordinate of the shape.
     * @param width Width of the shape.
     * @param height Height of the shape.
     * @param residueStyle Residue style of the shape.
     * @param renderer Renderer to draw the shape.
     * @param isSelected Whether this shape is selected.
     */
    public End(double angle, double x, double y, double width, double height, ResidueStyle residueStyle, String strokeStyleOfEnd, WebCanvasRenderingContext2D renderer, boolean isSelected) {
        super(x, y, width, height, residueStyle, renderer, isSelected);
        this.strokeStyleOfEnd = strokeStyleOfEnd;
        this.angle = angle;
    }

    /**
     * Draws a shape.
     */
    @Override
    protected void paintShape() {
        renderer.setStrokeStyle(strokeStyleOfEnd != null ? strokeStyleOfEnd : "#" + Integer.toHexString(residueStyle.getShapeColor().getRGB()).substring(2));
        BaseShape.createEnd(renderer, angle, x, y, width, height);
    }
}
