package org.glycoinfo.vaadin.glycanbuilder2web.shapes;

import java.awt.Color;

import org.eurocarbdb.application.glycanbuilder.ResidueStyle;
import org.eurocarbdb.application.glycanbuilder.renderutil.ResAngle;
import org.glycoinfo.vaadin.WebCanvasRenderingContext2D;

/**
 * Base class of a drawing shape.
 */
public abstract class BaseShape {
    /**
     * X coordinate of the shape.
     */
    protected double x;

    /**
     * Y coordinate of the shape.
     */
    protected double y;

    /**
     * Width of the shape.
     */
    protected double width;

    /**
     * Height of the shape.
     */
    protected double height;

    /**
     * Residue style of the shape.
     */
    protected ResidueStyle residueStyle;

    /**
     * Renderer to draw the shape.
     */
    protected WebCanvasRenderingContext2D renderer;

    /**
     * Whether this shape is selected.
     */
    protected boolean isSelected;

    /**
     * Constructor to draw a shape.
     * @param x X coordinate of the shape.
     * @param y Y coordinate of the shape.
     * @param width Width of the shape.
     * @param height Height of the shape.
     * @param residueStyle Residue style of the shape.
     * @param renderer Renderer to draw the shape.
     * @param isSelected Whether this circle is selected.
     */
    public BaseShape(double x, double y, double width, double height, ResidueStyle residueStyle, WebCanvasRenderingContext2D renderer, boolean isSelected) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.residueStyle = residueStyle;
        this.renderer = renderer;
        this.isSelected = isSelected;
    }

    /**
     * Draws a shape.
     */
    public void paint() {
        // Draws the surrounding shape. If it is selected, increase width of its line.
        renderer.save();
        if (isSelected) {
            renderer.setLineWidth(2);
        } else {
            renderer.setLineWidth(1);
        }
        setShapeFillStyle();
        paintShape();

        // Draws the internal shape. If it is selected, increase transparency of it.
        if (isSelected) {
            renderer.setGlobalAlpha(0.6);
        }
        setInternalShapeFillStyle();
        paintFillShape();
        renderer.restore();
    }

    /**
     * Draws a bold line around the selected shape.
     */
    protected void paintSelected() {
        renderer.save();
        renderer.setGlobalAlpha(0.4);
        renderer.fillRect(x, y, width, height);
        renderer.setStrokeStyle(residueStyle.getShapeColor());
        renderer.strokeRect(x, y, width, height);
        renderer.restore();
    }

    /**
     * Draws an internal shape.
     */
    protected void internalShapeFull() {
        throw new UnsupportedOperationException();
    }

    /**
     * Draws an internal shape filled left side.
     */
    protected void internalShapeLeft() {
        throw new UnsupportedOperationException();
    }

    /**
     * Draws an internal shape filled right side.
     */
    protected void internalShapeRight() {
        throw new UnsupportedOperationException();
    }

    /**
     * Draws an internal shape filled top side.
     */
    protected void internalShapeTop() {
        throw new UnsupportedOperationException();
    }

    /**
     * Draws an internal shape filled bottom side.
     */
    protected void internalShapeBottom() {
        throw new UnsupportedOperationException();
    }

    /**
     * Draws an internal shape filled top and left side.
     */
    protected void internalShapeTopLeft() {
        throw new UnsupportedOperationException();
    }

    /**
     * Draws an internal shape filled top and right side.
     */
    protected void internalShapeTopRight() {
        throw new UnsupportedOperationException();
    }

    /**
     * Draws an internal shape filled bottom and right side.
     */
    protected void internalShapeBottomRight() {
        throw new UnsupportedOperationException();
    }

    /**
     * Draws an internal shape filled bottom and left side.
     */
    protected void internalShapeBottomLeft() {
        throw new UnsupportedOperationException();
    }

    /**
     * Draws a shape to according residue style.
     */
    protected void paintFillShape() {
        // If the fill style is empty, ends the process.
        String fillStyle = residueStyle.getFillStyle();
        if (fillStyle.equals("empty")) {
            return;
        }

        // Draws a shape.
        switch (fillStyle) {
            case "full":
                internalShapeFull();
                break;
            case "left":
                internalShapeLeft();
                break;
            case "top":
                internalShapeTop();
                break;
            case "right":
                internalShapeRight();
                break;
            case "bottom":
                internalShapeBottom();
                break;
            case "topleft":
                internalShapeTopLeft();
                break;
            case "topright":
                internalShapeTopRight();
                break;
            case "bottomright":
                internalShapeBottomRight();
                break;
            case "bottomleft":
                internalShapeBottomLeft();
                break;
            case "circle":
                renderer.beginPath();
                renderer.arc(x + width / 2d, y + height / 2d, width / 8d, 0 * Math.PI, Math.PI * 2, false);
                renderer.closePath();
                renderer.fill();
                renderer.setFillStyle(residueStyle.getShapeColor());
                renderer.stroke();
                break;
            case "checkered":
                renderer.setLineWidth(0.5);
                renderer.beginPath();
                renderer.moveTo(x + width / 2d, y);
                renderer.lineTo(x + width / 2d, y + height);
                renderer.moveTo(x, y + height / 2d);
                renderer.lineTo(x + width, y + height / 2d);
                renderer.closePath();
                renderer.setFillStyle(residueStyle.getShapeColor());
                renderer.stroke();
                break;
        }
    }

    /**
     * Draws a shape.
     */
    protected abstract void paintShape();

    /**
     * Sets fill style for the surrounding shape.
     * If ResidueStyle.isFillNegative() is true, the color of it is specified the fill color(e.g. NeuGc of UOXF).
     */
    protected final void setShapeFillStyle() {
        renderer.setFillStyle(residueStyle.isFillNegative() ? residueStyle.getFillColor() : Color.WHITE);
    }

    /**
     * Sets fill style for the internal shape.
     * If ResidueStyle.isFillNegative() is true, the color of it is specified the white color(e.g. NeuGc of UOXF).
     */
    protected final void setInternalShapeFillStyle() {
        renderer.setFillStyle(residueStyle.isFillNegative() ? Color.WHITE : residueStyle.getFillColor());
    }

    /**
     * Draws a triangle.
     * @param x X coordinate that is the base of the triangle.
     * @param y Y coordinate that is the base of the triangle.
     * @param width Width that is the base of the triangle.
     * @param height Height that is the base of the triangle.
     * @param isRightTriangle Whether the triangle is right triangle.
     * @param renderer Renderer to draw the triangle.
     */
    public final static void createTriangle(double x, double y, double width, double height, boolean isRightTriangle, WebCanvasRenderingContext2D renderer) {
        createTriangle(-3d * Math.PI / 4d, x, y, width, height, isRightTriangle, renderer);
    }

    /**
     * Draws a triangle after determining vertexes of a triangle.
     * @param angle Angle of the triangle.
     * @param x X coordinate that is the base of the triangle.
     * @param y Y coordinate that is the base of the triangle.
     * @param width Width that is the base of the triangle.
     * @param height Height that is the base of the triangle.
     * @param isRightTriangle Whether the triangle is right triangle.
     * @param renderer Renderer to draw the triangle.
     */
    protected final static void createTriangle(double angle, double x, double y, double width, double height, boolean isRightTriangle, WebCanvasRenderingContext2D renderer) {
        double x1;
        double y1;
        double x2;
        double y2;
        double x3;
        double y3;
        if (angle >= -Math.PI / 4d && angle <= Math.PI / 4d) {
            x1 = x + width;
            y1 = y + (height / 2d);
            x2 = x;
            y2 = y + height;
            x3 = x;
            y3 = isRightTriangle ? y + (height / 2d) : y;
        } else if (angle >= Math.PI / 4d && angle <= 3d * Math.PI / 4d) {
            x1 = x + (width / 2d);
            y1 = y + height;
            x2 = x;
            y2 = y;
            x3 = isRightTriangle ? x + (width / 2d) : x + width;
            y3 = y;
        } else if (angle >= -3d * Math.PI / 4d && angle <= -Math.PI / 4d) {
            x1 = x + (width / 2d);
            y1 = y;
            x2 = x + width;
            y2 = y + height;
            x3 = isRightTriangle ? x + (width / 2d) : x;
            y3 = y + height;
        } else {
            x1 = x;
            y1 = y + (height / 2d);
            x2 = x + width;
            y2 = y + height;
            x3 = isRightTriangle ? x + (width / 2d) : x + width;
            y3 = y;
        }
        createTriangle(x1, y1, x2, y2, x3, y3, renderer);
    }

    /**
     * Draws a triangle to according parameters.
     * @param x1 X coordinate of the first vertex.
     * @param y1 Y coordinate of the first vertex.
     * @param x2 X coordinate of the second vertex.
     * @param y2 Y coordinate of the second vertex.
     * @param x3 X coordinate of the third vertex.
     * @param y3 Y coordinate of the third vertex.
     * @param renderer Renderer to draw the triangle.
     */
    protected final static void createTriangle(double x1, double y1, double x2, double y2, double x3, double y3, WebCanvasRenderingContext2D renderer) {
        renderer.beginPath();
        renderer.moveTo(x1, y1);
        renderer.lineTo(x2, y2);
        renderer.lineTo(x3, y3);
        renderer.closePath();
    }

    /**
     * Draws a diamond.
     * @param x X coordinate of the diamond.
     * @param y Y coordinate of the diamond.
     * @param width Width of the diamond.
     * @param height Height of the diamond.
     * @param renderer Renderer to draw the diamond.
     */
    public final static void createDiamond(double x, double y, double width, double height, WebCanvasRenderingContext2D renderer) {
        createDiamond(x, y, width, height, renderer, true);
    }

    /**
     * Draws a hat diamond.
     * @param x X coordinate of the hat diamond.
     * @param y Y coordinate of the hat diamond.
     * @param width Width of the hat diamond.
     * @param height Height of the hat diamond.
     * @param renderer Renderer to draw the hat diamond.
     */
    public final static void createHatDiamond(double x, double y, double width, double height, WebCanvasRenderingContext2D renderer) {
        renderer.beginPath();
        createDiamond(x, y, width, height, renderer, false);
        renderer.moveTo(x - 2, y + (height / 2) - 2);
        renderer.lineTo(x + (width / 2) - 2, y - 2);
        renderer.closePath();
    }

    /**
     * Draws a R hat diamond.
     * @param x X coordinate of the R hat diamond.
     * @param y Y coordinate of the R hat diamond.
     * @param width Width of the R hat diamond.
     * @param height Height of the R hat diamond.
     * @param renderer Renderer to draw the R hat diamond.
     */
    public final static void createRHatDiamond(double x, double y, double width, double height, WebCanvasRenderingContext2D renderer) {
        renderer.beginPath();
        createDiamond(x, y, width, height, renderer, false);
        renderer.moveTo(x + width + 2, y + (height / 2) - 2);
        renderer.lineTo(x + (width / 2) + 2, y - 2);
        renderer.closePath();
    }

    /**
     * Draws a diamond.
     * @param x X coordinate of the diamond.
     * @param y Y coordinate of the diamond.
     * @param width Width of the diamond.
     * @param height Height of the diamond.
     * @param renderer Renderer to draw the diamond.
     * @param shouldUsePath Whether uses path in this method.
     */
    public final static void createDiamond(double x, double y, double width, double height, WebCanvasRenderingContext2D renderer, boolean shouldUsePath) {
        // If it is odd, change it to even.
        if (width % 2 == 1) {
            width++;
        }
        if (height % 2 == 1) {
            height++;
        }

        // If use path, do beginPath and closePath.
        if (shouldUsePath) {
            renderer.beginPath();
        }
        renderer.moveTo(x + (width / 2d), y);
        renderer.lineTo(x + width, y + (height / 2d));
        renderer.lineTo(x + (width / 2d), y + height);
        renderer.lineTo(x, y + (height / 2d));
        if (shouldUsePath) {
            renderer.closePath();
        }
    }

    /**
     * Draws a hexagon.
     * @param x X coordinate of the hexagon.
     * @param y Y coordinate of the hexagon.
     * @param width Width of the hexagon.
     * @param height Height of the hexagon.
     * @param renderer Renderer to draw the hexagon.
     */
    public final static void createHexagon(double x, double y, double width, double height, WebCanvasRenderingContext2D renderer) {
        double rx = width / 2d;
        double ry = height / 2d;
        double cx = x+width / 2d;
        double cy = y+height / 2d;
        double step = Math.PI / 3d;
        renderer.beginPath();
        renderer.moveTo(cx + (rx * Math.cos(0 * step)), cy + (ry * Math.sin(0 * step)));
        for (int i = 1; i <= 6; i++) {
            renderer.lineTo(cx + (rx * Math.cos(i * step)), cy + (ry * Math.sin(i * step)));
        }
        renderer.closePath();
    }

    /**
     * Draws flat hexagon.
     * @param x X coordinate of the hexagon.
     * @param y Y coordinate of the hexagon.
     * @param width width Width of the hexagon.
     * @param height Height of the hexagon.
     * @param renderer Renderer to draw the hexagon.
     */
    public final static void createFlatHexagon(double x, double y, double width, double height, WebCanvasRenderingContext2D renderer) {
        double rx = width / 2d;
        double ry = height /2.5d;
        double cx = x + width / 2d;
        double cy = y + height / 2d;
        double step = Math.PI / 3d;
        renderer.beginPath();
        renderer.moveTo(cx + (rx * Math.cos(0 * step)), cy + (ry * Math.sin(0 * step)));
        for (int i = 1; i <= 6; i++) {
            renderer.lineTo(cx + (rx * Math.cos(i * step)), cy + (ry * Math.sin(i * step)));
        }
        renderer.closePath();
    }

    /**
     * Draws a heptagon.
     * @param x X coordinate of the heptagon.
     * @param y Y coordinate of the heptagon.
     * @param width Width of the heptagon.
     * @param height Height of the heptagon.
     * @param renderer Renderer to draw the heptagon.
     */
    public final static void createHeptagon(double x, double y, double width, double height, WebCanvasRenderingContext2D renderer){
        double rx = width / 2d;
        double ry = height / 2d;
        double cx = x+width / 2d;
        double cy = y+height / 2d;
        double step = Math.PI / 3.5;
        renderer.beginPath();
        renderer.moveTo(cx + (rx * Math.cos((0 * step) - (Math.PI / 2d))), cy + (ry * Math.sin((0 * step) - (Math.PI / 2d))));
        for (int i=1; i<=7; i++ ) {
            renderer.lineTo(cx + (rx * Math.cos((i * step) - (Math.PI / 2d))), cy + (ry * Math.sin((i * step) - (Math.PI / 2d))));
        }
        renderer.closePath();
    }

    /**
     * Draws a star.
     * @param x X coordinate of the star.
     * @param y Y coordinate of the star.
     * @param width Width of the star.
     * @param height Height of the star.
     * @param renderer Renderer to draw the star.
     * @param points Points of the star.
     */
    public final static void createStar(double x, double y, double width, double height, WebCanvasRenderingContext2D renderer, int points) {
        double rx = width / 2d;
        double ry = height / 2d;
        double cx = x + width / 2d;
        double cy = y + height / 2d;
        double step = Math.PI / (double) points;
        double nstep = (Math.PI / 2d) - (2d * step);
        double mrx = rx / (Math.cos(step) + (Math.sin(step) / Math.tan(nstep)));
        double mry = ry / (Math.cos(step) + (Math.sin(step) / Math.tan(nstep)));

        // Draws a star.
        renderer.beginPath();
        renderer.moveTo(cx + (rx * Math.cos((0 * step) - (Math.PI / 2d))), cy + (ry * Math.sin((0 * step) - (Math.PI / 2d))));
        for (int i = 1; i <= 2 * points; i++) {
            if (i % 2 == 0) {
                renderer.lineTo(cx + (rx * Math.cos((i * step) - (Math.PI / 2d))), cy + (ry * Math.sin((i * step) - (Math.PI / 2d))));
            } else {
                renderer.lineTo(cx + (mrx * Math.cos((i * step) - (Math.PI / 2d))), cy + (mry * Math.sin((i * step) - (Math.PI / 2d))));
            }
        }
        renderer.closePath();
    }

    /**
     * Draws a pentagon.
     * @param x X coordinate of the pentagon.
     * @param y Y coordinate of the pentagon.
     * @param width Width of the pentagon.
     * @param height Height of the pentagon.
     * @param renderer Renderer to draw the pentagon.
     */
    public final static void createPentagon(double x, double y, double width, double height, WebCanvasRenderingContext2D renderer) {
        double rx = width / 2d;
        double ry = height / 2d;
        double cx = x + width / 2d;
        double cy = y + height / 2d;
        double step = Math.PI / 2.5;
        renderer.beginPath();
        renderer.moveTo(cx + (rx * Math.cos((0 * step) - (Math.PI / 2d))), cy + (ry * Math.sin((0 * step) - (Math.PI / 2d))));
        for (int i = 1; i <= 5; i++) {
            renderer.lineTo(cx + (rx * Math.cos((i * step) - (Math.PI / 2d))), cy + (ry * Math.sin((i * step) - (Math.PI / 2d))));
        }
        renderer.closePath();
    }

    /**
     * Draws a R hombus.
     * @param x X coordinate of the R hombus.
     * @param y Y coordinate of the R hombus.
     * @param width Width of the R hombus.
     * @param height Height of the R hombus.
     * @param renderer Renderer to draw the R hombus.
     */
    public final static void createRhombus(double x, double y, double width, double height, WebCanvasRenderingContext2D renderer) {
        renderer.beginPath();
        renderer.moveTo(x + (0.50 * width), y);
        renderer.lineTo(x + (0.85 * width), y + (0.50 * height));
        renderer.lineTo(x + (0.50 * width), y + height);
        renderer.lineTo(x + (0.15 * width), y + (0.50 * height));
        renderer.closePath();
    }

    /**
     * Draws a dashed line.
     * @param renderer Renderer to draw the dashed line.
     * @param x1 X coordinate of beginning of the dashed line.
     * @param y1 Y coordinate of beginning of the dashed line.
     * @param x2 X coordinate of ending of the dashed line.
     * @param y2 Y coordinate of ending of the dashed line.
     */
    public static void paintDashedLine(WebCanvasRenderingContext2D renderer, int x1, int y1, int x2, int y2) {	
        int dx = (x2 - x1);
        int dy = (y2 - y1);

        // What's the orientation of the triangle, i.e. y steps x or x steps y.
        boolean xSlope = (Math.abs(dx) > Math.abs(dy));

        // Gets the gradient y2 - y1 / x2 - x1 or x2 - x1 / y2 - y1.
        double gradient;
        boolean xSlopeDown = false;
        boolean ySlopeDown = false;
        if (xSlope) {
            gradient = dy / (double) dx;
            xSlopeDown = dx < 0;
        } else {
            gradient = dx / (double) dy;
            ySlopeDown = dy < 0;
        }

        // Converts to double for positioning.
        double x = x1;
        double y = y1;

        // Draws initial position.
        renderer.moveTo(x, y);

        // The length of the dashed line "c" is equal to the square root of a^2 + b^2
        double dashedLineLength = Math.sqrt(dx * dx + dy * dy);

        // How much of the line is left to go.
        double distRemaining = dashedLineLength;
        boolean penUpOperation = false;
        int dashLineLength = 2;
        int numDashes = (int) (dashedLineLength / dashLineLength);
        if (dashedLineLength % dashLineLength > 0) {
            numDashes++;
        }

        // Draws dashed line.
        for (int i = 0; i < numDashes; i++) {
            // Sets dashLine length to either the default or what ever is left.
            double dashLength=Math.min(distRemaining, dashLineLength);

            // Calculates where x2 and y2 are based on c (dash length) and the gradient we need on the dashed line.
            double step = Math.sqrt(dashLength * dashLength / (1 + gradient * gradient));
            if (xSlope) {
                if (xSlopeDown) {
                    step =- step;
                }
                x += step;
                y += gradient * step;
            } else {
                if (ySlopeDown) {
                    step =- step;
                }
                x += gradient * step;
                y += step;
            }
            if (penUpOperation) {
                renderer.moveTo(x, y);
            } else {
                renderer.lineTo(x, y);
            }
            distRemaining -= dashLength;
            penUpOperation =! penUpOperation;
        }
    }

    /**
     * Draws a reducing end.
     * @param renderer Renderer to draw the reducing end.
     * @param angle Angle of the reducing end.
     * @param x X coordinate of the reducing end.
     * @param y Y coordinate of the reducing end.
     * @param width Width of the reducing end.
     * @param height Height of the reducing end.
     */
    public static void createEnd(WebCanvasRenderingContext2D renderer, double angle, double x, double y, double width, double height) {
        double rx = width / 2d;
        double ry = height / 2d;
        double cx = x + width / 2d;
        double cy = y + height / 2d;

        // Calculates start point.
        double x1 = cx + rx * Math.cos(angle - Math.PI / 2d);
        double y1 = cy + ry * Math.sin(angle - Math.PI / 2d);

        // Calculates end point.
        double x2 = cx + rx * Math.cos(angle + Math.PI / 2d);
        double y2 = cy + ry * Math.sin(angle + Math.PI / 2d);

        // Calculates ctrl point 1.
        double cx1 = cx + 0.5 * rx * Math.cos(angle - Math.PI / 2d);
        double cy1 = cy + 0.5 * ry * Math.sin(angle - Math.PI / 2d);
        double tx1 = cx1 + 0.5 * rx * Math.cos(angle - Math.PI);
        double ty1 = cy1 + 0.5 * ry * Math.sin(angle - Math.PI);

        // Calculates ctrl point 2.
        double cx2 = cx + 0.5 * rx * Math.cos(angle + Math.PI / 2d);
        double cy2 = cy + 0.5 * ry * Math.sin(angle + Math.PI / 2d);
        double tx2 = cx2 + 0.5 * rx * Math.cos(angle);
        double ty2 = cy2 + 0.5 * ry * Math.sin(angle);

        // Draws reducing end.
        renderer.beginPath();
        renderer.moveTo(x1, y1);
        renderer.bezierCurveTo(tx1, ty1, tx2, ty2, x2, y2);
        renderer.stroke();
        renderer.closePath();
    }

    /**
     * Draws a bracket.
     * @param renderer Renderer to draw the bracket.
     * @param angle Angle of the bracket.
     * @param x X coordinate of the bracket.
     * @param y Y coordinate of the bracket.
     * @param width Width of the bracket.
     * @param height Height of the bracket.
     */
    public static void createBracket(WebCanvasRenderingContext2D renderer, double angle, double x, double y, double width, double height) {
        double rx = width / 2d;
        double ry = height / 2d;
        double cx = x + width / 2d;
        double cy = y + height / 2d;

        // Calculates and draws first start point.
        double xygap = 10d;
        double x1;
        double y1;
        if (angle == Math.PI) {
            x1 = cx + rx * Math.cos(angle - Math.PI / 2d) - rx * Math.cos(angle) - xygap;
            y1 = cy + ry * Math.sin(angle - Math.PI / 2d) + ry * Math.sin(angle);
        } else if (angle == 0) {
            x1 = cx + rx * Math.cos(angle - Math.PI / 2d) - rx * Math.cos(angle) + xygap;
            y1 = cy + ry * Math.sin(angle - Math.PI / 2d) + ry * Math.sin(angle);
        } else if (angle == Math.PI / 2d) {
            x1 = cx + rx * Math.cos(angle - Math.PI / 2d) + rx * Math.cos(angle);
            y1 = cy + ry * Math.sin(angle - Math.PI / 2d) - ry * Math.sin(angle) + xygap;
        }
        else {
            x1 = cx + rx * Math.cos(angle - Math.PI / 2d) + rx * Math.cos(angle);
            y1 = cy + ry * Math.sin(angle - Math.PI / 2d) - ry * Math.sin(angle) - xygap;
        }
        renderer.beginPath();
        renderer.moveTo(x1, y1);

        // Calculates and draws first end point.
        double x2 = cx + rx * Math.cos(angle - Math.PI / 2d);
        double y2 = cy + ry * Math.sin(angle - Math.PI / 2d);
        x2 = (angle == Math.PI) ? x2 - xygap : x2;
        x2 = (angle == 0) ? x2 + xygap : x2;

        if (angle == Math.PI) {
            //y2 = y2;
        } else if (angle == 0) {
            //y2 = y2 - xygap;
        } else if (angle == Math.PI / 2d) {
            y2 = y2 + xygap;
        } else {
            y2 = y2 - xygap;
        }


        renderer.lineTo(x2, y2);

        // Calculates and draws second start point.
        double x3 = cx + rx * Math.cos(angle + Math.PI / 2d);
        double y3 = cy + ry * Math.sin(angle + Math.PI / 2d);
        x3 = (angle == Math.PI) ? x3 - xygap : x3;
        x3 = (angle == 0) ? x3 + xygap : x3;
        //y3 = (angle == Math.PI || angle == 0 ) ? y3 : y3 - xygap;
        if (angle == Math.PI) {
            // y2 = y2;
        } else if (angle == 0) {
            // y2 = y2 - xygap;
        } else if (angle == Math.PI / 2d) {
            y3 = y3 + xygap;
        } else {
            y3 = y3 - xygap;
        }

        renderer.lineTo(x3, y3);

        // Calculates and draws second end point.
        double x4;
        double y4;
        if (angle == Math.PI) {
            x4 = cx + rx * Math.cos(angle + Math.PI / 2d) - rx * Math.cos(angle) - xygap;
            y4 = cy + ry * Math.sin(angle + Math.PI / 2d) + ry * Math.sin(angle);
        } else if (angle == 0) {
            x4 = cx + rx * Math.cos(angle + Math.PI / 2d) - rx * Math.cos(angle) + xygap;
            y4 = cy + ry * Math.sin(angle + Math.PI / 2d) + ry * Math.sin(angle);
        } else if(angle==Math.PI/2d) {
            x4 = cx + rx * Math.cos(angle + Math.PI / 2d) - rx * Math.cos(angle);
            y4 = cy + ry * Math.sin(angle + Math.PI / 2d) - ry * Math.sin(angle) + xygap;
        }
        else {
            x4 = cx + rx * Math.cos(angle + Math.PI / 2d) + rx * Math.cos(angle);
            y4 = cy + ry * Math.sin(angle + Math.PI / 2d) - ry * Math.sin(angle) - xygap;
        }

        renderer.lineTo(x4, y4);

        // Draws close shape.
        renderer.lineTo(x3, y3);
        renderer.lineTo(x2, y2);
        renderer.stroke();
        renderer.closePath();
    }

    /**
     * Draws a square bracket.
     * @param renderer Renderer to draw the square bracket.
     * @param angle Angle of the square bracket.
     * @param x X coordinate of the square bracket.
     * @param y Y coordinate of the square bracket.
     * @param width Width of the square bracket.
     * @param height Height of the square bracket.
     */
    public static void createSquareBracket(WebCanvasRenderingContext2D renderer, double angle, double x, double y, double width, double height) {
        double r = Math.min(width, height);
        double cx = x + width / 2d;
        double cy = y + height / 2d;

        // Calculates and draws first point.
        double x1 = cx + r * Math.cos(angle - Math.PI / 2d) + r / 4d * Math.cos(angle + Math.PI);
        double y1 = cy + r * Math.sin(angle - Math.PI / 2d) + r / 4d * Math.sin(angle + Math.PI);
        renderer.beginPath();
        renderer.moveTo(x1, y1);

        // Calculates and draws second point.
        double x2 = cx + r * Math.cos(angle - Math.PI / 2d);
        double y2 = cy + r * Math.sin(angle - Math.PI / 2d);
        renderer.lineTo(x2, y2);

        // Calculates and draws third point.
        double x3 = cx + r * Math.cos(angle + Math.PI / 2d);
        double y3 = cy + r * Math.sin(angle + Math.PI / 2d);
        renderer.lineTo(x3, y3);

        // Calculates and draws fourth point.
        double x4 = cx + r * Math.cos(angle + Math.PI / 2d) + r / 4d * Math.cos(angle + Math.PI);
        double y4 = cy + r * Math.sin(angle + Math.PI / 2d) + r / 4d * Math.sin(angle + Math.PI);
        renderer.lineTo(x4, y4);

        // Draws close shape.
        renderer.lineTo(x3, y3);
        renderer.lineTo(x2, y2);
    }

    /**
     * Draws a cyclic.
     * @param renderer Renderer to draw the cyclic.
     * @param orientation Orientation of the cyclic.
     * @param x X coordinate of the cyclic.
     * @param y Y coordinate of the cyclic.
     * @param width Width of the cyclic.
     * @param height Height of the cyclic.
     */
    public static void createCyclic(WebCanvasRenderingContext2D renderer, ResAngle orientation, double x, double y, double width, double height) {
        double a_dXpoint = x;
        double a_dCtrlX = 0.0;
        double a_dCtrlY = 0.0;
        
        // Draws cyclic according to angle.
        if(orientation.getIntAngle() == 0) {
            a_dCtrlX = a_dXpoint + width;
            a_dCtrlY = (y - height * 0.5 + y + height * 1.5) * 0.5;
            renderer.beginPath();
            renderer.bezierCurveTo(a_dXpoint, y-height * 0.5, a_dCtrlX, a_dCtrlY, a_dXpoint, y + height * 1.5);
            renderer.stroke();
            renderer.closePath();
        }
        if(orientation.getIntAngle() == 180) {
            a_dCtrlX = a_dXpoint;
            a_dCtrlY = (y-height * 0.5 + y + height * 1.5) * 0.5;
            a_dXpoint = a_dXpoint + width;
            renderer.beginPath();
            renderer.bezierCurveTo(a_dXpoint, y-height * 0.5, a_dCtrlX, a_dCtrlY, a_dXpoint, y + height * 1.5);
            renderer.stroke();
            renderer.closePath();
        }
        if(orientation.getIntAngle() == 90) {
            a_dCtrlX = (a_dXpoint * 2 + 23) * 0.5;
            a_dCtrlY = y + height;
            renderer.beginPath();
            renderer.bezierCurveTo(a_dXpoint + 34, y, a_dCtrlX, a_dCtrlY, a_dXpoint - 11, y);
            renderer.stroke();
            renderer.closePath();
        }
        if(orientation.getIntAngle() == -90) {
            a_dCtrlX = (a_dXpoint * 2 + 23) * 0.5;
            a_dCtrlY = y;
            renderer.beginPath();
            renderer.bezierCurveTo(a_dXpoint + 34, y + height, a_dCtrlX, a_dCtrlY, a_dXpoint - 11, y + height);
            renderer.stroke();
            renderer.closePath();
        }
    }
}
