package org.glycoinfo.vaadin.glycanbuilder2web.shapes;

import org.eurocarbdb.application.glycanbuilder.ResidueStyle;
import org.glycoinfo.vaadin.WebCanvasRenderingContext2D;

/**
 * Drawing a triangle class.
 */
public class Triangle extends BaseShape {
    /**
     * Angle of shape.
     */
    private double angle;

    /**
     * Calls super class constructor.
     * @param angle Angle of the shape.
     * @param  x X coordinate of the shape.
     * @param y Y coordinate of the shape.
     * @param width Width of the shape.
     * @param height Height of the shape.
     * @param residueStyle Residue style of the shape.
     * @param renderer Renderer to draw the shape.
     * @param isSelected Whether this circle is selected.
     */
    public Triangle(double angle, double x, double y, double width, double height, ResidueStyle residueStyle, WebCanvasRenderingContext2D renderer, boolean isSelected) {
        super(x, y, width, height, residueStyle, renderer, isSelected);
        this.angle = angle;
    }

    /**
     * Draws a shape.
     */
    @Override
    protected void paintShape() {
        BaseShape.createTriangle(angle, x, y, width, height, false, renderer);
        renderer.fill();
        renderer.setStrokeStyle(residueStyle.getShapeColor());
        renderer.stroke();
    }

    /**
     * Draws internal a shape.
     */
    @Override
    protected void internalShapeFull() {
        BaseShape.createTriangle(angle, x, y, width, height, false, renderer);
        renderer.fill();
    }

    /**
     * Draws an internal shape filled right side.
     */
    @Override
    protected void internalShapeRight() {
        BaseShape.createTriangle(angle, x, y, width, height, true, renderer);
        renderer.fill();
        renderer.setFillStyle(residueStyle.getShapeColor());
        renderer.stroke();
    }
}
