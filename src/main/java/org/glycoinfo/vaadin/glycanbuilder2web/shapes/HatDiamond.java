package org.glycoinfo.vaadin.glycanbuilder2web.shapes;

import org.eurocarbdb.application.glycanbuilder.ResidueStyle;
import org.glycoinfo.vaadin.WebCanvasRenderingContext2D;

/**
 * Drawing a hat diamond class.
 */
public class HatDiamond extends Diamond {
    /**
     * Calls super class constructor.
     * @param x X coordinate of the shape.
     * @param y Y coordinate of the shape.
     * @param width Width of the shape.
     * @param height Height of the shape.
     * @param residueStyle Residue style of the shape.
     * @param renderer Renderer to draw the shape.
     * @param isSelected Whether this circle is selected.
     */
    public HatDiamond(double x, double y, double width, double height, ResidueStyle residueStyle, WebCanvasRenderingContext2D renderer, boolean isSelected) {
        super(x, y, width, height, residueStyle, renderer,isSelected);
    }

    /**
     * Draws a shape.
     */
    @Override
    protected void paintShape() {
        BaseShape.createHatDiamond(x, y, width, height, renderer);
        renderer.fill();
        renderer.setStrokeStyle(residueStyle.getShapeColor());
        renderer.stroke();
    }
}
