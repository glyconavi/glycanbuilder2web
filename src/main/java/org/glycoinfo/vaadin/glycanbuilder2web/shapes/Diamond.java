package org.glycoinfo.vaadin.glycanbuilder2web.shapes;

import org.eurocarbdb.application.glycanbuilder.ResidueStyle;
import org.glycoinfo.vaadin.WebCanvasRenderingContext2D;

/**
 * Drawing diamond class.
 */
public class Diamond extends BaseShape {
    /**
     * Calls super class constructor.
     * @param x X coordinate of the shape.
     * @param y Y coordinate of the shape.
     * @param width Width of the shape.
     * @param height Height of the shape.
     * @param residueStyle Residue style of the shape.
     * @param renderer Renderer to draw the shape.
     * @param isSelected Whether this circle is selected.
     */
    public Diamond(double x, double y, double width, double height, ResidueStyle style, WebCanvasRenderingContext2D renderer, boolean isSelected) {
        super(x, y, width, height, style, renderer, isSelected);
    }

    /**
     * Draws a shape.
     */
    @Override
    protected void paintShape() {
        BaseShape.createDiamond(x, y, width, height, renderer);
        renderer.fill();
        renderer.setStrokeStyle(residueStyle.getShapeColor());
        renderer.stroke();
    }

    /**
     * Draws internal a shape.
     */
    @Override 
    protected void internalShapeFull() {
        BaseShape.createDiamond(x, y, width, height, renderer);
        renderer.fill();
    }

    /**
     * Draws an internal shape filled top side.
     */
    @Override
    protected void internalShapeTop() {
        BaseShape.createTriangle(x, y + (height / 2d), x + (width / 2d), y, x + width, y + (height / 2d), renderer);
        renderer.fill();
        renderer.setLineWidth(1d);
        renderer.beginPath();
        renderer.moveTo(x, y + (height / 2d));
        renderer.lineTo(x + width, y + (height / 2d));
        renderer.stroke();
        renderer.closePath();
    }

    /**
     * Draws an internal shape filled top and left side.
     */
    @Override
    protected void internalShapeLeft() {
        BaseShape.createTriangle(x + (width / 2d), y, x + (width / 2d), y + height, x, y + (height / 2d), renderer);
        renderer.fill();
        renderer.setLineWidth(1d);
        renderer.beginPath();
        renderer.moveTo(x + (width / 2d), y);
        renderer.lineTo(x + (width / 2d), y + height);
        renderer.stroke();
        renderer.closePath();
        
    }

    /**
     * Draws an internal shape filled bottom side.
     */
    @Override
    protected void internalShapeBottom() {
        BaseShape.createTriangle(x, y + (height / 2d), x + width, y + (height / 2d), x + (width / 2d), y + height, renderer);
        renderer.fill();
        renderer.setLineWidth(1d);
        renderer.beginPath();
        renderer.moveTo(x, y + (height / 2d));
        renderer.lineTo(x + width, y + (height / 2d));
        renderer.stroke();
        renderer.closePath();
    }

    /**
     * Draws an internal shape filled top and right side.
     */
    @Override
    protected void internalShapeRight() {
        BaseShape.createTriangle(x + (width / 2d), y, x + width, y + (height / 2d), x + (width / 2d), y + height, renderer);
        renderer.fill();
        renderer.setLineWidth(1d);
        renderer.beginPath();
        renderer.moveTo(x + (width / 2d), y);
        renderer.lineTo(x + (width / 2d), y + height);
        renderer.stroke();
        renderer.closePath();
    }
}
