package org.glycoinfo.vaadin.glycanbuilder2web.shapes;

import org.eurocarbdb.application.glycanbuilder.ResidueStyle;
import org.glycoinfo.vaadin.WebCanvasRenderingContext2D;

/**
 * Drawing a circle class.
 */
public class Circle extends BaseShape {
    /**
     * Calls super class constructor.
     * @param x X coordinate of the shape.
     * @param y Y coordinate of the shape.
     * @param width Width of the shape.
     * @param height Height of the shape.
     * @param residueStyle Residue style of the shape.
     * @param renderer Renderer to draw the shape.
     * @param isSelected Whether this circle is selected.
     */
    public Circle(double x, double y, double width, double height, ResidueStyle residueStyle, WebCanvasRenderingContext2D renderer, boolean isSelected) {
        super(x, y, width, height, residueStyle, renderer, isSelected);
    }

    /**
     * Draws internal a shape.
     */
    @Override
    protected void internalShapeFull() {
        renderer.beginPath();
        renderer.arc(x + (width / 2d), y + (height / 2d), width / 2d, 0d * Math.PI, 2d * Math.PI, false);
        renderer.closePath();
        renderer.fill();
    }

    /**
     * Draws a surrounding shape to use specified color and fills internal the shape.
     */
    @Override
    protected void paintShape() {
        renderer.beginPath();
        renderer.arc(x + (width / 2d), y + (height / 2d), width / 2d, 0d * Math.PI, 2d * Math.PI, false);
        renderer.closePath();
        renderer.fill();
        renderer.setStrokeStyle(residueStyle.getShapeColor());
        renderer.stroke();
    }
}
