package org.glycoinfo.vaadin.glycanbuilder2web.shapes;

import org.eurocarbdb.application.glycanbuilder.ResidueStyle;
import org.glycoinfo.vaadin.WebCanvasRenderingContext2D;

/**
 * Drawing a star class.
 */
public class Star extends BaseShape {
    /**
     * Points of the star.
     */
    protected int points;

    /**
     * Calls super class constructor.
     * @param x X coordinate of the shape.
     * @param y Y coordinate of the shape.
     * @param width Width of the shape.
     * @param height Height of the shape.
     * @param residueStyle Residue style of the shape.
     * @param renderer Renderer to draw the shape.
     * @param points Points of the star.
     * @param isSelected Whether this circle is selected.
     */
    public Star(double x, double y, double width, double height, ResidueStyle residueStyle, WebCanvasRenderingContext2D renderer, int points, boolean isSelected) {
        super(x, y, width, height, residueStyle, renderer, isSelected);
        this.points = points;
    }

    /**
     * Draws a shape.
     */
    @Override
    protected void paintShape() {
        BaseShape.createStar(x, y, width, height, renderer, points);
        renderer.fill();
        renderer.setStrokeStyle(residueStyle.getShapeColor());
        renderer.stroke();
    }

    /**
     * Draws internal a shape.
     */
    @Override
    protected void internalShapeFull() {
        BaseShape.createStar(x, y, width, height, renderer, points);
        renderer.fill();
    }
}
