package org.glycoinfo.vaadin.glycanbuilder2web.shapes;

import org.eurocarbdb.application.glycanbuilder.ResidueStyle;
import org.glycoinfo.vaadin.WebCanvasRenderingContext2D;

/**
 * Drawing a square class.
 */
public class Square extends BaseShape {
    /**
     * Calls super class constructor.
     * @param x X coordinate of the shape.
     * @param y Y coordinate of the shape.
     * @param width Width of the shape.
     * @param height Height of the shape.
     * @param residueStyle Residue style of the shape.
     * @param renderer Renderer to draw the shape.
     * @param isSelected Whether this circle is selected.
     */
    public Square(double x, double y, double width, double height, ResidueStyle residueStyle, WebCanvasRenderingContext2D renderer, boolean isSelected) {
        super(x, y, width, height, residueStyle, renderer, isSelected);
    }

    /**
     * Draws a shape.
     */
    @Override
    protected void paintShape() {
        renderer.fillRect(x, y, width, height);
        renderer.setStrokeStyle(residueStyle.getShapeColor());
        renderer.strokeRect(x, y, width, height);
    }

    /**
     * Draws an internal shape.
     */
    @Override
    protected void internalShapeFull() {
        renderer.fillRect(x, y, width, height);
        renderer.setFillStyle(residueStyle.getShapeColor());
    }

    /**
     * Draws an internal shape filled left side.
     */
    @Override
    protected void internalShapeLeft() {
        renderer.fillRect(x, y, width / 2d, height);
        renderer.setFillStyle(residueStyle.getShapeColor());
        renderer.strokeRect(x, y, width / 2d, y);
    }

    /**
     * Draws an internal shape filled top side.
     */
    @Override
    protected void internalShapeTop() {
        renderer.fillRect(x, y, width, height / 2d);
        renderer.setFillStyle(residueStyle.getShapeColor());
        renderer.strokeRect(x, y, width, y / 2d);
    }

    /**
     * Draws an internal shape filled right side.
     */
    @Override
    protected void internalShapeRight() {
        renderer.fillRect(x + (width / 2d), y, width / 2d, height);
        renderer.setFillStyle(residueStyle.getShapeColor());
        renderer.strokeRect(x + width / 2d, y, width / 2d, height);
    }

    /**
     * Draws an internal shape filled bottom side.
     */
    @Override
    protected void internalShapeBottom() {
        renderer.fillRect(x, y + height / 2d, width, height / 2d);
        renderer.setFillStyle(residueStyle.getShapeColor());
        renderer.strokeRect(x, y + height / 2d, width, height / 2d);
    }

    /**
     * Draws an internal shape filled top and left side.
     */
    @Override
    protected void internalShapeTopLeft() {
        BaseShape.createTriangle(x, y, x + width, y, x, y + height, renderer);
        renderer.fill();
        renderer.setFillStyle(residueStyle.getShapeColor());
        renderer.stroke();
    }

    /**
     * Draws an internal shape filled top and right side.
     */
    @Override
    protected void internalShapeTopRight() {
        BaseShape.createTriangle(x, y, x + width, y, x + width, y + height, renderer);
        renderer.fill();
        renderer.setFillStyle(residueStyle.getShapeColor());
        renderer.stroke();
    }

    /**
     * Draws an internal shape filled bottom and right side.
     */
    @Override
    protected void internalShapeBottomRight() {
        BaseShape.createTriangle(x + width, y, x + width, y + height, x, y + height, renderer);
        renderer.fill();
        renderer.setFillStyle(residueStyle.getShapeColor());
        renderer.stroke();
    }

    /**
     * Draws an internal shape filled bottom and left side.
     */
    @Override
    protected void internalShapeBottomLeft() {
        BaseShape.createTriangle(x, y, x + width, y + height, x, y + height, renderer);
        renderer.fill();
        renderer.setFillStyle(residueStyle.getShapeColor());
        renderer.stroke();
    }
}
