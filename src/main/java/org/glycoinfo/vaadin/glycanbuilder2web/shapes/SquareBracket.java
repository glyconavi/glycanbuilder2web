package org.glycoinfo.vaadin.glycanbuilder2web.shapes;

import org.eurocarbdb.application.glycanbuilder.ResidueStyle;
import org.glycoinfo.vaadin.WebCanvasRenderingContext2D;

/**
 * Drawing a square bracket class.
 */
public class SquareBracket extends BaseShape {
    /**
     * Angle of shape.
     */
    public double angle;

    /**
     * Calls super class constructor.
     * @param angle Angle of the shape.
     * @param x X coordinate of the shape.
     * @param y Y coordinate of the shape.
     * @param width Width of the shape.
     * @param height Height of the shape.
     * @param residueStyle Residue style of the shape.
     * @param renderer Renderer to draw the shape.
     * @param isSelected Whether this circle is selected.
     */
    public SquareBracket(double angle, double x, double y, double width, double height, ResidueStyle residueStyle, WebCanvasRenderingContext2D renderer, boolean isSelected) {
        super(x, y, width, height, residueStyle, renderer, isSelected);
        this.angle = angle;
    }

    /**
     * Draws a shape.
     */
    @Override
    protected void paintShape() {
        renderer.setStrokeStyle(residueStyle.getShapeColor());
        BaseShape.createSquareBracket(renderer, angle, x, y, width, height);
        renderer.setStrokeStyle(residueStyle.getShapeColor());
        renderer.stroke();
    }

    /**
     * Draws internal a shape.
     */
    @Override
    protected void internalShapeFull() {
        //throw new UnsupportedOperationException();
    }
}
