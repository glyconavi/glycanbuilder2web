package org.glycoinfo.vaadin.glycanbuilder2web.shapes;

import org.eurocarbdb.application.glycanbuilder.ResidueStyle;
import org.glycoinfo.vaadin.WebCanvasRenderingContext2D;

/**
 * Drawing a r hat diamond class.
 */
public class RHatDiamond extends Diamond {
    /**
     * Calls super class constructor.
     * @param x X coordinate of the shape.
     * @param y Y coordinate of the shape.
     * @param width Width of the shape.
     * @param height Height of the shape.
     * @param residueStyle Residue style of the shape.
     * @param renderer Renderer to draw the shape.
     * @param isSelected Whether this circle is selected.
     */
    public RHatDiamond(double x, double y, double width, double height, ResidueStyle residueStyle, WebCanvasRenderingContext2D renderer, boolean isSelected) {
        super(x, y, width, height, residueStyle, renderer, isSelected);
    }

    /**
     * Draws a shape.
     */
    @Override
    protected void paintShape() {
        BaseShape.createDiamond(x, y, width, height, renderer);
        renderer.fill();
        renderer.setStrokeStyle(residueStyle.getShapeColor());
        renderer.stroke();
        renderer.beginPath();
        renderer.moveTo(x + width + 2, y + (height / 2) - 2);
        renderer.lineTo(x + (width / 2) + 2, y - 2);
        renderer.stroke();
        renderer.closePath();
    }
}
