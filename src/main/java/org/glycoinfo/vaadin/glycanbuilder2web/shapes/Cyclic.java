package org.glycoinfo.vaadin.glycanbuilder2web.shapes;

import org.eurocarbdb.application.glycanbuilder.ResidueStyle;
import org.eurocarbdb.application.glycanbuilder.renderutil.ResAngle;
import org.glycoinfo.vaadin.WebCanvasRenderingContext2D;

/**
 * Drawing bracket class.
 */
public class Cyclic extends BaseShape {
    /**
     * Orientation of shape.
     */
    private ResAngle orientation;

    /**
     * Calls super class constructor.
     * @param orientation Orientation of shape.
     * @param x X coordinate of the shape.
     * @param y Y coordinate of the shape.
     * @param width Width of the shape.
     * @param height Height of the shape.
     * @param residueStyle Residue style of the shape.
     * @param renderer Renderer to draw the shape.
     * @param isSelected Whether this circle is selected.
     */
    public Cyclic(ResAngle orientation, double x, double y, double width, double height, ResidueStyle residueStyle, WebCanvasRenderingContext2D renderer, boolean isSelected) {
        super(x, y, width, height, residueStyle, renderer, isSelected);
        this.orientation = orientation;
    }

    /**
     * Draws a shape.
     */
    @Override
    protected void paintShape() {
        renderer.setStrokeStyle(residueStyle.getShapeColor());
        BaseShape.createCyclic(renderer, orientation, x, y, width, height);
        renderer.setStrokeStyle(residueStyle.getShapeColor());
    }

    /**
     * Draws internal a shape.
     */
    @Override
    protected void internalShapeFull() {
        //throw new UnsupportedOperationException();
    }
}
