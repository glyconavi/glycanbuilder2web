package org.glycoinfo.vaadin.glycanbuilder2web.canvas;

/**
 * Listener to be called when changing notation.
 */
public interface NotationChangeListener {
    /**
     * Changes notation of structure.
     * @param glycanWebCanvas Canvas drawn residues and linkages.
     */
    public void changeNotation(GlycanWebCanvas glycanWebCanvas);
}
