package org.glycoinfo.vaadin.glycanbuilder2web.canvas;

import static org.eurocarbdb.application.glycanbuilder.renderutil.Geometry.distance;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

import org.eurocarbdb.application.glycanbuilder.BuilderWorkspace;
import org.eurocarbdb.application.glycanbuilder.Glycan;
import org.eurocarbdb.application.glycanbuilder.GlycanDocument;
import org.eurocarbdb.application.glycanbuilder.Residue;
import org.eurocarbdb.application.glycanbuilder.ResidueType;
import org.eurocarbdb.application.glycanbuilder.dataset.CoreDictionary;
import org.eurocarbdb.application.glycanbuilder.dataset.ResidueDictionary;
import org.eurocarbdb.application.glycanbuilder.dataset.TerminalDictionary;
import org.eurocarbdb.application.glycanbuilder.linkage.Linkage;
import org.eurocarbdb.application.glycanbuilder.linkage.Union;
import org.eurocarbdb.application.glycanbuilder.renderutil.BBoxManager;
import org.eurocarbdb.application.glycanbuilder.renderutil.Geometry;
import org.eurocarbdb.application.glycanbuilder.renderutil.GlycanRenderer;
import org.eurocarbdb.application.glycanbuilder.renderutil.Paintable;
import org.eurocarbdb.application.glycanbuilder.renderutil.PositionManager;
import org.eurocarbdb.application.glycanbuilder.util.GraphicOptions;
import org.glycoinfo.application.glycanbuilder.dataset.TextSymbolDescriptor;
import org.glycoinfo.application.glycanbuilder.util.GlycanUtils;
import org.glycoinfo.vaadin.WebCanvas;
import org.glycoinfo.vaadin.WebCanvasRenderingContext2D;
import org.glycoinfo.vaadin.glycanbuilder2web.dialog.CompositionDialog;
import org.glycoinfo.vaadin.glycanbuilder2web.dialog.ExplanationDialog;
import org.glycoinfo.vaadin.glycanbuilder2web.dialog.ImportStringDialog;
import org.glycoinfo.vaadin.glycanbuilder2web.dialog.OutputStringDialog;
import org.glycoinfo.vaadin.glycanbuilder2web.dialog.ResetCanvasDialog;
import org.glycoinfo.vaadin.glycanbuilder2web.dialog.ResidueRepetitionPropertiesDialog;
import org.glycoinfo.vaadin.glycanbuilder2web.dialog.ResidueSelectorDialog;
import org.glycoinfo.vaadin.glycanbuilder2web.renderer.GlycanWebGlycanRenderer;
import org.glycoinfo.vaadin.glycanbuilder2web.toolbar.LinkageToolbar;
import org.glycoinfo.vaadin.glycanbuilder2web.toolbar.StructureToolbar;

import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.orderedlayout.Scroller;

/**
 * Canvas component for rendering residue and linkage.
 */
@Tag("canvas")
@SuppressWarnings("serial")
public class GlycanWebCanvas extends WebCanvas implements Paintable {
    /**
     * Container of all the documents, dictionaries and options that used in the GlycanBuilder.
     */
    private BuilderWorkspace builderWorkspace = new BuilderWorkspace(new GlycanWebGlycanRenderer());

    /**
     * Document containing a set of glycan structures.
     */
    private GlycanDocument glycanDocument = builderWorkspace.getStructures();

    /**
     * Position manager that be used to compute and store position of a residue around its parent.
     */
    private PositionManager positionManager = new PositionManager();

    /**
     * Bounding box manager that be used to compute and store the bounding box of a residue.
     */
    private BBoxManager boundingBoxManager = new BBoxManager();

    /**
     * Whether the canvas is multiple select.
     */
    private boolean isMultipleSelect = true;

    /**
     * Whether the canvas is full size.
     */
    private boolean isFullSize = false;

    /**
     * End points for selecting residue on residue selector dialog.
     */
    private LinkedList<Residue> endPoints = null;

    /**
     * Currently selected residue.
     */
    private Residue currentResidue;

    /**
     * Residues that a user selected.
     */
    private HashSet<Residue> selectedResidues = new HashSet<Residue>();

    /**
     * Currently selected linkage.
     */
    private Linkage currentLinkage;

    /**
     * Linkages that a user selected.
     */
    private HashSet<Linkage> selectedLinkages = new HashSet<Linkage>();

    /**
     * List of notation change listener.
     */
    private List<NotationChangeListener> notationCangeListenerList = new ArrayList<NotationChangeListener>();

    /**
     * Structure toolbar.
     */
    private StructureToolbar structureToolbar;

    /**
     * Linkage toolbar.
     */
    private LinkageToolbar linkageToolbar;

    /**
     * Scroller of GlycanWebCanvas.
     */
    private Scroller glycanWebCanvasScroller;

    /**
     * Completion value of canvas height.
     */
    public final int CANVAS_HEIGHT_COMPLETION_VALUE = 7;

    /**
     * Height on before copy image to clipboard.
     */
    private int heightOnBeforeCopyImageToClipboard = 0;

    /**
     * Constructs the canvas at size 0.
     */
    public GlycanWebCanvas() {
        this(0, 0, GraphicOptions.NOTATION_SNFG);
    }

    /**
     * Calls super class constructor.
     * @param width Width of the canvas.
     * @param height Height of the canvas.
     * @param notation Notation of structures.
     */
    public GlycanWebCanvas(int width, int height, String notation) {
        super(width, height);
        builderWorkspace.setNotation(notation);
    }

    /**
     * Calls super class constructor and sets whether is multiple select and end points.
     * @param width Width of the canvas.
     * @param height Height of the canvas.
     * @param notation Notation of structures.
     * @param isMultipleSelect Whether is multiple select.
     * @param endPoints End points for selecting residue on residue selector dialog.
     */
    public GlycanWebCanvas(int width, int height, String notation, boolean isMultipleSelect, LinkedList<Residue> endPoints) {
        this(width, height, notation);
        this.isMultipleSelect = isMultipleSelect;
        this.endPoints = endPoints;
    }

    /**
     * Adds a specified residue to the canvas.
     * @param residueType Residue type that adds to the canvas.
     */
    public void addResidue(ResidueType residueType) {
        // Adds a specified residue.
        try {
            // Retains current residue before adding residue.
            Residue previousResidue = currentResidue;

            // Adds a residue to the canvas.
            Residue targetResidue = ResidueDictionary.newResidue(residueType.getName());
            if (glycanDocument.addResidue(currentResidue, getLinkedResidues(), targetResidue) == null) return;
            setSelectedResidue(targetResidue);

            // If previous residue is bracket, its children fragment.
            if (previousResidue != null && previousResidue.isBracket()) {
                GlycanUtils glycanUtils = new GlycanUtils();
                glycanUtils.getCoreResidue(getSelectedStructures());
                LinkedList<Residue> coreResidues = glycanUtils.getCoreResidues();
                for (Residue coreResidue : coreResidues) {
                    currentResidue.addParentOfFragment(coreResidue);
                }
            }
        } catch (Exception e) {
        }

        // Sets enabled of undo and redo button.
        setEnabledOfUndoRedoButton();

        // Update linkage toolbar.
        updateLinkageToolbar();

        // Updates the canvas with the information of documents, dictionaries and options.
        updateCanvas();
    }

    /**
     * Returns all the residues that are shown at the same position of the residue with the focus.
     */
    public ArrayList<Residue> getLinkedResidues() {
        return boundingBoxManager.getLinkedResidues(currentResidue);
    }

    /**
     * Gets all the structures containing the selected residues and linkages.
     * @return Returns all the structures containing the selected residues and linkages.
     */
    public Collection<Glycan> getSelectedStructures() {
        return glycanDocument.findStructuresWith(selectedResidues, selectedLinkages);
    }

    /**
     * Updates the canvas with the information of documents, dictionaries and options.
     */
    public void updateCanvas() {
        // Clears pixels in whole the canvas.
        clear();

        // Updates canvas size to fit on the screen.
        updateCanvasSize();

        // Draws residues according to the builder workspace. 
        paintResidues();

        // Paints selection indicates the selecting glycans.
        paintSelection();
    }

    /**
     * Draws residues according to the builder workspace.
     */
    private void paintResidues() {
        if (!glycanDocument.isEmpty()) {
            positionManager = new PositionManager();
            boundingBoxManager = new BBoxManager();
            builderWorkspace.getGlycanRenderer().computeBoundingBoxes(glycanDocument.getStructures(), builderWorkspace.getGraphicOptions().SHOW_MASSES_CANVAS, builderWorkspace.getGraphicOptions().SHOW_REDEND_CANVAS, positionManager, boundingBoxManager);
            for (Glycan glycan : glycanDocument.getStructures()) {
                builderWorkspace.getGlycanRenderer().paint(this, glycan, selectedResidues, selectedLinkages, builderWorkspace.getGraphicOptions().SHOW_MASSES_CANVAS, builderWorkspace.getGraphicOptions().SHOW_REDEND_CANVAS, positionManager, boundingBoxManager);
            }
        }
    }

    /**
     * Paints selection indicates the selecting glycans.
     */
    private void paintSelection() {
        // Fills a rectangle indicates the selecting glycans.
        Collection<Glycan> selectStructures = getSelectedStructures();
        GraphicOptions graphicOptions = builderWorkspace.getGlycanRenderer().getGraphicOptions();
        boolean isShowingReduceEnd = graphicOptions.SHOW_REDEND_CANVAS;
        for (Glycan glycan : glycanDocument.getStructures()) {
            if (selectStructures.contains(glycan)) {
                Rectangle boundingBox = boundingBoxManager.getBBox(glycan, isShowingReduceEnd);
                if (boundingBox != null) {
                    WebCanvasRenderingContext2D renderer = (WebCanvasRenderingContext2D) getObject();
                    renderer.setFillStyle(Color.BLUE);
                    renderer.fillRect(graphicOptions.MARGIN_LEFT / 2 - 3, boundingBox.y, 5, boundingBox.height + graphicOptions.MASS_TEXT_SPACE + graphicOptions.MASS_TEXT_SIZE);
                }
            }
        }

        // Draws a rectangle indicates the selecting glycans.
        Glycan currentStructure = getCurrentStructure();
        if (currentStructure != null) {
            Rectangle currentBoundingBox = boundingBoxManager.getBBox(currentStructure, isShowingReduceEnd);
            if (currentBoundingBox != null) {
                renderer.setFillStyle(Color.BLUE);
                renderer.rect(graphicOptions.MARGIN_LEFT / 2 - 3, currentBoundingBox.y, 5, currentBoundingBox.height + graphicOptions.MASS_TEXT_SPACE + graphicOptions.MASS_TEXT_SIZE);
            }
        }
    }

    /**
     * Returns the structure containing the residue with the focus.
     */
    public Glycan getCurrentStructure() {
        if (currentResidue != null) return glycanDocument.findStructureWith(currentResidue);
        if (currentLinkage != null) return glycanDocument.findStructureWith(currentLinkage.getChildResidue());
        return null;
    }

    /**
     * Updates canvas size to fit size of structures.
     */
    private void updateCanvasSize() {
        try {
            int scrollerWidth = Integer.parseInt(glycanWebCanvasScroller.getStyle().get("width").replace("px", ""));
            int scrollerHeight = Integer.parseInt(glycanWebCanvasScroller.getStyle().get("height").replace("px", ""));
            Dimension structureDimension = computeSize();
            int width = isFullSize && structureDimension.width <= scrollerWidth ? scrollerWidth : structureDimension.width;
            int height = isFullSize && structureDimension.height <= scrollerHeight - CANVAS_HEIGHT_COMPLETION_VALUE ? scrollerHeight - CANVAS_HEIGHT_COMPLETION_VALUE : structureDimension.height;
            setSize(width, height);
        } catch (Exception e) {
        }
    }

    /**
     * Updates selection on the canvas.
     */
    @Override
    public void updateSelection(double x, double y, double width, double height, boolean isMoved) {
        // Selects residues in the rectangle selection.
        selectResidues(x, y, width, height, isMoved);
    }

    /**
     * Selects residues in the rectangle selection.
     * @param x X coordinate with mouse pointer.
     * @param y Y coordinate with mouse pointer.
     * @param width Width of rectangle selection.
     * @param height Height of rectangle selection.
     * @param isMoved Whether mouse is moved.
     */
    private void selectResidues(double x, double y, double width,double height, boolean isMoved) {
        // Gets residues in the rectangle selection.
        List<Residue> selectedResidues = getSelectedResidues(x, y, width,height, isMoved);

        // Resets selection state.
        resetSelection();

        // Sets selected residues in the rectangle selection.
        if (selectedResidues.size() == 1) {
            if (endPoints == null) {
                setSelectedResidue(selectedResidues.get(0));
            } else {
                if (endPoints.contains(selectedResidues.get(0))) setSelectedResidue(selectedResidues.get(0));
            }
        } else {
            if (isMultipleSelect) {
                setSelectedResidues(selectedResidues, new Point((int) x, (int) y));
            }
        }

        // Sets a selected linkage in the rectangle selection.
        if (!hasSelectedResidues() && endPoints == null) {
            Linkage linkage = getLinkageAtPoint(new Point((int) x, (int) y));
            setSelectedLinkage(linkage);
        }

        // Updates linkage toolbar.
        updateLinkageToolbar();

        // Updates the canvas with the information of documents, dictionaries and options.
        updateCanvas();
    }

    /**
     * Gets residues in the rectangle selection.
     * @param x X coordinate with mouse pointer.
     * @param y Y coordinate with mouse pointer.
     * @param width Width of rectangle selection.
     * @param height Height of rectangle selection.
     * @param isMoved Whether mouse is moved.
     * @return Returns list of residue in the rectangle selection.
     */
    private List<Residue> getSelectedResidues(double x, double y, double width,double height, boolean isMoved) {
        List<Residue> selectedResidues = new ArrayList<Residue>();
        for (Residue residue : boundingBoxManager.current_bboxes.keySet()) {
            Rectangle rectangle = boundingBoxManager.current_bboxes.get(residue);
            if (isMoved) {
                if ((rectangle.x < x + width && rectangle.x + rectangle.width > x) && (rectangle.y < y + height && rectangle.y + rectangle.height > y)) {
                    selectedResidues.add(residue);
                }
            } else {
                if ((x >= rectangle.x && x <= rectangle.x + rectangle.width) && (y >= rectangle.y && y <= rectangle.y + rectangle.height)) {
                    selectedResidues.add(residue);
                }
            }
        }
        return selectedResidues;
    }

    /**
     * Whether has selected residues.
     * @return Returns whether has selected residues.
     */
    private boolean hasSelectedResidues() {
        return selectedResidues.size() > 0 || selectedLinkages.size() > 0 ? true : false;
    }

    /**
     * Resets selection state.
     */
    public void resetSelection() {
        currentResidue = null;
        selectedResidues.clear();
        currentLinkage = null;
        selectedLinkages.clear();
        if (structureToolbar != null) structureToolbar.setEnabled(hasCurrentResidue());
    }

    /**
     * Sets a selected residue in the rectangle selection.
     * @param residue Selected residue in the rectangle selection.
     */
    private void setSelectedResidue(Residue residue) {
        resetSelection();
        if (residue != null) {
            selectedResidues.add(residue);
            currentResidue = residue;
            if (structureToolbar != null) structureToolbar.setEnabled(hasCurrentResidue());
        }
    }

    /**
     * Sets selected residues in the rectangle selection.
     * @param residues Selected residues in the rectangle selection.
     * @param mousePoint Mouse point that is clicked last time.
     */
    private void setSelectedResidues(Collection<Residue> residues, Point mousePoint) {
        resetSelection();
        if (residues != null && !residues.isEmpty()) {
            for (Residue residue : residues) {
                selectedResidues.add(residue);
            }
            currentResidue = findNearestResidue(mousePoint, selectedResidues);
            if (structureToolbar != null) structureToolbar.setEnabled(hasCurrentResidue());
        }
    }

    /**
     * Finds nearest residue from mouse point.
     * @param mousePoint Mouse point that is clicked last time.
     * @param residues Selected residues in the rectangle selection.
     * @return Returns nearest residue from mouse point.
     */
    private Residue findNearestResidue(Point mousePoint, Collection<Residue> residues) {
        // If mouse point is null, ends the process.
        if (mousePoint == null) return null;

        // Finds nearest residue from mouse point.
        Residue nearestResidue = null;
        double nearestDistance = 0d;
        for (Residue currentResidue : residues) {
            Rectangle currentBoundingBox = boundingBoxManager.getCurrent(currentResidue);
            if (currentBoundingBox == null) continue;
            double currentDistance = distance(mousePoint, currentBoundingBox);
            if (nearestResidue == null || nearestDistance > currentDistance) {
                nearestResidue = currentResidue;
                nearestDistance = currentDistance;
            }
        }
        return nearestResidue;
    }

    /**
     * Sets a selected linkage in the rectangle selection.
     * @param linkage Selected linkage in the rectangle selection.
     */
    private void setSelectedLinkage(Linkage linkage) {
        resetSelection();
        if (linkage != null) {
            selectedLinkages.add(linkage);
            currentLinkage = linkage;
        }
    }

    /**
     * Gets the linkage at the specified position.
     * @param point Point with mouse pointer.
     * @return Returns the linkage at the specified position.
     */
    private Linkage getLinkageAtPoint(Point point) {
        for (Glycan glycan : glycanDocument.getStructures()) {
            // Gets the linkage follow from the root glycan.
            Linkage linkageAtPoint = getLinkageAtPoint(glycan.getRoot(), point);
            if (linkageAtPoint != null) return linkageAtPoint;

            // Gets the linkage follow from the bracket.
            linkageAtPoint = getLinkageAtPoint(glycan.getBracket(), point);
            if (linkageAtPoint != null) return linkageAtPoint;
        }
        return null;
    }

    /**
     * Gets the linkage from a residue that is displayed at the specified position.
     * @param rootResidue Root residue of the glycan.
     * @param point Point with mouse pointer.
     * @return Returns the linkage from residue that is displayed at he specified position.
     */
    private Linkage getLinkageAtPoint(Residue rootResidue, Point point) {
        // If the root residue is not exist, doesn't select linkage.
        if (rootResidue == null) return null;

        // Gets the linkage from current bounding box from the root residue.
        Rectangle currentBoundingBox = boundingBoxManager.getCurrent(rootResidue);
        for (Linkage linkage : rootResidue.getChildrenLinkages()) {
            // If current linkage matches at the specified position, returns it.
            Rectangle childBoundingBox = boundingBoxManager.getCurrent(linkage.getChildResidue());
            if (currentBoundingBox != null && childBoundingBox != null && Geometry.distance(point, Geometry.center(currentBoundingBox), Geometry.center(childBoundingBox)) < 4d) return linkage;

            // Finds the linkage at the specified position from the child residue.
            Linkage linkageAtPoint = getLinkageAtPoint(linkage.getChildResidue(), point);
            if (linkageAtPoint != null) return linkageAtPoint;
        }
        return null;
    }

    /**
     * Change scale rate.
     * @param scaleRate Scale rate.
     */
    public void changeScaleRate(double scaleRate) {
        builderWorkspace.getGraphicOptions().SCALE_CANVAS = scaleRate;
        builderWorkspace.getGlycanRenderer().getGraphicOptions().setScale(builderWorkspace.getGlycanRenderer().getGraphicOptions().SCALE_CANVAS);
        updateCanvas();
    }

    /**
     * Gets scale rate.
     * @return Returns scale rate.
     */
    public double getScaleRate() {
        return builderWorkspace.getGraphicOptions().SCALE_CANVAS;
    }

    /**
     * Changes notation of structure.
     * @param notation Notation of structure.
     */
    public void changeNotation(String notation) {
        builderWorkspace.setNotation(notation);
        updateCanvas();
        for (NotationChangeListener notationChangeListener : notationCangeListenerList) {
            notationChangeListener.changeNotation(this);
        }
    }

    /**
     * Adds notation change listener.
     * @param notationChangeListener Notation change listener.
     */
    public void addNotationChangeListener(NotationChangeListener notationChangeListener) {
        notationCangeListenerList.add(notationChangeListener);
    }

    /**
     * Changes the display style of structures to the specified style.
     * @param styleOfDisplay Specified style of display.
     */
    public void changeStructureView(String styleOfDisplay) {
        builderWorkspace.setDisplay(styleOfDisplay);
        updateCanvas();
    }

    /**
     * Sets whether collapses multiple fragments.
     * @param shouldCollapse Whether should collapse.
     */
    public void collapseMultipleAntenna(boolean shouldCollapse) {
        builderWorkspace.getGraphicOptions().COLLAPSE_MULTIPLE_ANTENNAE = shouldCollapse;
        updateCanvas();
    }

    /**
     * Sets whether show mass value for each glycan.
     * @param shouldShowMasses Whether should show masses.
     */
    public void showMassesInDrawingCanvas(boolean shouldShowMasses) {
        builderWorkspace.getGraphicOptions().SHOW_MASSES_CANVAS = shouldShowMasses;
        updateCanvas();
    }

    /**
     * Sets whether output mass value for each glycan structure when exporting into any graphic file.
     * @param shouldShowMasses Whether should show masses when exporting into any graphic file.
     */
    public void showMassesWhenExporting(boolean shouldShowMasses) {
        builderWorkspace.getGraphicOptions().SHOW_MASSES = shouldShowMasses;
        updateCanvas();
    }

    /**
     * Sets whether show reducing end indicator in drawing canvas.
     * @param shouldShowReducingEndIndicator Whether should show reducing end indicator.
     */
    public void showReducingEndIndicatorInDrawingCanvas(boolean shouldShowReducingEndIndicator) {
        builderWorkspace.getGraphicOptions().SHOW_REDEND_CANVAS = shouldShowReducingEndIndicator;
        updateCanvas();
    }

    /**
     * Sets whether output the symbol of the reducing end when exporting into any graphic file.
     * @param shouldShowReducingEndIndicator Whether should show reducing end indicator when exporting into any graphic file.
     */
    public void showReducingEndIndicatorWhenExporting(boolean shouldShowReducingEndIndicator) {
        builderWorkspace.getGraphicOptions().SHOW_REDEND = shouldShowReducingEndIndicator;
        updateCanvas();
    }

    /**
     * Shows composition dialog.
     */
    public void showCompositionDialog() {
        new CompositionDialog(this, builderWorkspace.getCompositionOptions());
    }

    /**
     * Adds composition.
     */
    public void addComposition() {
        try {
            // Adds composition from composition option.
            Glycan composition = getBuilderWorkspace().getCompositionOptions().getCompositionAsGlycan(getBuilderWorkspace().getDefaultMassOptions());
            getGlycanDocument().addStructure(composition);

            // Sets enabled of undo and redo button.
            setEnabledOfUndoRedoButton();

            // Updates the canvas with the information of documents, dictionaries and options.
            updateCanvas();
        } catch (Exception e) {
            e.getMessage();
        }
    }

    /**
     * Adds structure from core name.
     * @param coreName Core name of structure.
     */
    public void addStructure(String coreName) {
        try {
            // Adds structure from coreName.
            Residue structure = CoreDictionary.newCore(coreName);
            if(structure.isReducingEnd() && structure.getTypeName().equals("redEnd")) {
                structure.firstChild().setAlditol(true);
                structure.firstChild().setAnomericState('?');
                structure.firstChild().setRingSize('o');
            }
            glycanDocument.addStructure(structure);

            // Sets enabled of undo and redo button.
            setEnabledOfUndoRedoButton();

            // Updates the canvas with the information of documents, dictionaries and options.
            updateCanvas();
        } catch (Exception e) {
            e.getMessage();
        }
    }

    /**
     * Adds structure from root residue.
     * @param root Root residue.
     */
    public void addStructure(Residue root) {
        glycanDocument.addStructure(root);
        setEnabledOfUndoRedoButton();
        updateCanvas();
    }

    /**
     * Adds terminal from terminal name.
     * @param terminalName Terminal name of terminal.
     */
    public void addTerminal(String terminalName) {
        try {
            // Adds structure from coreName.
            Residue terminal = TerminalDictionary.newTerminal(terminalName);
            Residue currentResidueTemp = getCurrentResidue();
            if (glycanDocument.addResidue(currentResidueTemp, getLinkedResidues(), terminal) != null) setSelectedResidue(currentResidue);

            // Sets enabled of undo and redo button.
            setEnabledOfUndoRedoButton();

            // Updates the canvas with the information of documents, dictionaries and options.
            updateCanvas();
        } catch (Exception e) {
            e.getMessage();
        }
    }

    /**
     * Change reducing end type of structure with focus.
     * @param reducingEndTypeName Name of new reducing end type.
     */
    public void changeReducingEndType(String reducingEndTypeName) {
        try {
            // If not be selecting residue or selecting residue isn't reducing end, ends the process.
            if (currentResidue == null || !currentResidue.isReducingEnd()) throw new Exception("This utility need to select reducing end");

            // Changes reducing end type from specified reducing end type name.
            TextSymbolDescriptor reducingEndType = TextSymbolDescriptor.forRedType(reducingEndTypeName);
            ResidueType newReducingEndtype = ResidueDictionary.findResidueType(reducingEndType.toString());
            Residue current = currentResidue;
            glycanDocument.changeReducingEndType(current, newReducingEndtype);

            // Sets alditol, ring size and anomeric state.
            if (reducingEndType.equals(TextSymbolDescriptor.REDEND)) {
                if(builderWorkspace.getGlycanRenderer().getGraphicOptions().NOTATION.equals(GraphicOptions.NOTATION_SNFG)) {
                    current.firstChild().setAlditol(true);
                    current.firstChild().setRingSize('o');
                    current.firstChild().setAnomericState('?');
                }   
            } else {
                current.firstChild().setAlditol(false);
                if(current.firstChild().getRingSize() == 'o') {
                    current.firstChild().setRingSize('?');
                    current.firstChild().setAnomericState('?');
                }
            }

            // Updates the canvas with the information of documents, dictionaries and options.
            updateCanvas();
        } catch(Exception e) {
            new ExplanationDialog("Error in Change reducing end type", e.getMessage());
        }
    }

    /**
     * Adds bracket to current residue.
     */
    public void addBracket() {
        // Adds bracket to current residue.
        Residue bracket = glycanDocument.addBracket(currentResidue);
        if (bracket != null) setSelectedResidue(bracket);

        // Update linkage toolbar.
        updateLinkageToolbar();

        // Updates the canvas with the information of documents, dictionaries and options.
        updateCanvas();
    }

    /**
     * Adds repeat to current residue or end point.
     */
    public void addRepeat() {
        try {
            Collection<Residue> residues = selectedResidues;
            if (glycanDocument.createRepetition(null, residues)) {
                // Updates the canvas with the information of documents, dictionaries and options.
                resetSelection();
                updateCanvas();
            } else {
                // If user selects multiple residue, calls residue selector dialog to select end point of the repetition.
                LinkedList<Residue> endPoints = new LinkedList<Residue>();
                for (Residue residue : residues) {
                    if (residue.isSaccharide()) endPoints.add(residue);
                }
                Glycan structure = getSelectedStructures().iterator().next();
                GlycanWebGlycanRenderer glycanRenderer = new GlycanWebGlycanRenderer();
                Rectangle structureBoundingBox = glycanRenderer.computeBoundingBoxes(new Union<Glycan>(structure), false, true, positionManager, boundingBoxManager);
                Dimension structureDimension = glycanRenderer.computeSize(structureBoundingBox);
                String notation = builderWorkspace.getGraphicOptions().NOTATION;
                new ResidueSelectorDialog("Select ending point", "Select ending point of the repetition", structureDimension.width, structureDimension.height, notation, structure, endPoints, residueSelectingCanvas ->{
                    try {
                        glycanDocument.createRepetition(residueSelectingCanvas.getCurrentResidue(), selectedResidues);
                        resetSelection();
                        updateCanvas();
                    } catch (Exception e) {
                        new ExplanationDialog("Error while creating the repeating unit", e.getMessage());
                    }
                });
            }
        } catch (Exception e) {
            new ExplanationDialog("Error while creating the repeating unit", e.getMessage());
        }
    }

    /**
     * Adds cyclic to end point.
     */
    public void addCyclic() {
        try {
            // If residues has less than 2 residues, throws exception.
            Collection<Residue> residues = selectedResidues;
            if(residues.size() < 2) throw new Exception("Cyclic structure must be contain two or more monosaccharide");

            // If user selects multiple residue, calls residue selector dialog to select end point of the repetition.
            LinkedList<Residue> endPoints = new LinkedList<Residue>();
            for (Residue residue : residues) {
                if (residue.isReducingEnd()) throw new Exception("Add cyclic could not handle reducing end");
                if (residue.isSaccharide()) endPoints.add(residue);
            }
            Glycan structure = getSelectedStructures().iterator().next();
            GlycanWebGlycanRenderer glycanRenderer = new GlycanWebGlycanRenderer();
            Rectangle structureBoundingBox = glycanRenderer.computeBoundingBoxes(new Union<Glycan>(structure), false, true, positionManager, boundingBoxManager);
            Dimension structureDimension = glycanRenderer.computeSize(structureBoundingBox);
            String notation = builderWorkspace.getGraphicOptions().NOTATION;
            new ResidueSelectorDialog("Select the last node", "Select the last node of the cyclic", structureDimension.width, structureDimension.height, notation, structure, endPoints, residueSelectingCanvas ->{
                try {
                    glycanDocument.createCyclic(residueSelectingCanvas.getCurrentResidue(), selectedResidues);
                    Glycan oGlycan = getCurrentStructure();
                    if (!oGlycan.getRoot().firstChild().isStartCyclic()) {
                        Residue oGlycnFirst = oGlycan.getRoot().firstChild(); 
                        oGlycan.getRoot().getChildrenLinkages().remove(oGlycan.getRoot().getLinkageAt(0));
                        oGlycan.getRoot().addChild(oGlycnFirst.getStartCyclicResidue());
                    }
                    resetSelection();
                    structureToolbar.setEnabled(hasCurrentResidue());
                    updateCanvas();
                } catch (Exception e) {
                    new ExplanationDialog("Error while creating the repeating unit", e.getMessage());
                }
            });
        } catch (Exception e) {
            new ExplanationDialog("Error while creating the cyclic unit", e.getMessage());
        }
    }

    /**
     * Adjusts properties of current residue.
     */
    public void adjustResidueProperties() {
        // If current residue doesn't select, ends the process.
        if (currentResidue == null) return;

        // Displays ResidueRepetitionPropertiesDialog.
        if (currentResidue.isEndRepetition()) {
            new ResidueRepetitionPropertiesDialog(currentResidue, this);
        }
    }

    /**
     * Resets the canvas.
     */
    public void resetCanvas() {
        new ResetCanvasDialog(this);
    }

    /**
     * Imports structures string.
     */
    public void importStructures() {
        new ImportStringDialog(this);
    }

    /**
     * Exports structures string.
     */
    public void exportStructures() {
        new OutputStringDialog(this);
    }

    /**
     * Deletes selected residue or structure.
     */
    public void deleteResidue() {
        Residue targetParentResidue = currentResidue != null ? currentResidue.getParent() : null;
        glycanDocument.removeResidues(selectedResidues);
        if (glycanDocument.contains(targetParentResidue)) {
            setSelectedResidue(targetParentResidue);
        } else {
            resetSelection();
        }

        // Sets enabled of undo and redo button.
        setEnabledOfUndoRedoButton();

        // Update linkage toolbar.
        updateLinkageToolbar();

        // Updates the canvas with the information of documents, dictionaries and options.
        updateCanvas();
    }

    /**
     * Undo operation.
     */
    public void undoOperation() {
        try {
            glycanDocument.getUndoManager().undo();
            setEnabledOfUndoRedoButton();
            resetSelection();
            updateLinkageToolbar();
            updateCanvas();
        } catch(Exception e) {
            e.getMessage();
        }
    }

    /**
     * Redo operation.
     */
    public void redoOperation() {
        try {
            glycanDocument.getUndoManager().redo();
            setEnabledOfUndoRedoButton();
            resetSelection();
            updateLinkageToolbar();
            updateCanvas();
        } catch(Exception e) {
            e.getMessage();
        }
    }

    /**
     * Calculates minimum size of canvas.
     * @return Returns minimum size of canvas.
     */
    public Dimension computeSize() {
        Collection<Glycan> structures = getGlycanDocument().getStructures();
        GlycanRenderer glycanRenderer = builderWorkspace.getGlycanRenderer();
        Rectangle structureBoundingBox = glycanRenderer.computeBoundingBoxes(structures, builderWorkspace.getGraphicOptions().SHOW_MASSES_CANVAS, builderWorkspace.getGraphicOptions().SHOW_REDEND_CANVAS, positionManager, boundingBoxManager);
        return glycanRenderer.computeSize(structureBoundingBox);
    }

    /**
     * Sets enabled of undo and redo button.
     */
    public void setEnabledOfUndoRedoButton() {
        if (structureToolbar != null) {
            structureToolbar.setEnabledOfUndoButton(glycanDocument.getUndoManager().canUndo());
            structureToolbar.setEnabledOfRedoButton(glycanDocument.getUndoManager().canRedo());
        }
    }

    /**
     * Rotates structure clockwise.
     */
    public void rotateStructureClockwise() {
        builderWorkspace.getGraphicOptions().ORIENTATION = (builderWorkspace.getGraphicOptions().ORIENTATION + 1) % 4;
        updateCanvas();
    }

    /**
     * Updates linkage toolbar.
     */
    private void updateLinkageToolbar() {
        if (linkageToolbar == null) return;
        linkageToolbar.updateLinkageToolbar(true, this);
    }

    /**
     * 
     * @param isFullSize
     */
    public void setIsFullSize(boolean isFullSize) {
        this.isFullSize = isFullSize;
    }

    /**
     * Gets glycan document.
     * @return Returns glycan document.
     */
    public GlycanDocument getGlycanDocument() {
        return glycanDocument;
    }

    /**
     * Gets current residue.
     * @return Returns current residue.
     */
    public Residue getCurrentResidue() {
        return currentResidue;
    }

    /**
     * Gets selected residues.
     * @return Returns selected residues.
     */
    public HashSet<Residue> getSelectedResidues() {
        return selectedResidues;
    }

    /**
     * Whether has current residue.
     * @return
     */
    private boolean hasCurrentResidue() {
        return currentResidue != null;
    }

    /**
     * Sets a structure toolbar instance.
     * @param structureToolbar StructureToolbar.
     */
    public void setStructureToolbar(StructureToolbar structureToolbar) {
        this.structureToolbar = structureToolbar;
    }

    /**
     * Sets a linkage toolbar instance.
     * @param linkageToolbar LinkageToolbar.
     */
    public void setLinkageToolbar(LinkageToolbar linkageToolbar) {
        this.linkageToolbar = linkageToolbar;
    }

    /**
     * Gets BuilderWorkspace.
     * @return Returns BuilderWorkspace.
     */
    public BuilderWorkspace getBuilderWorkspace() {
        return builderWorkspace;
    }

    /**
     * Executes process before copying image to clipboard.
     */
    @Override
    protected void beforeCopyImageToClipboard() {
        // Changes canvas size to fit size of structures.
        heightOnBeforeCopyImageToClipboard = getCanvasHeight();
        setIsFullSize(false);
        updateCanvas();
    }

    /**
     * Executes process after copying image to clipboard.
     */
    @Override
    protected void afterCopyImageToClipboard() {
        // Changes canvas size to full size.
        getElement().setAttribute("height", String.valueOf(heightOnBeforeCopyImageToClipboard));
        setIsFullSize(true);
        updateCanvas();
    }

    /**
     * Gets graphics 2D.
     */
    @Override
    public Graphics2D getGraphics2D() {
        return null;
    }

    /**
     * Gets canvas instance for drawing shapes.
     */
    @Override
    public Object getObject() {
        return renderer;
    }

    /**
     * Clears pixels in whole the canvas.
     */
    @Override
    public void clear() {
        String width = getElement().getAttribute("width");
        String height = getElement().getAttribute("height");
        renderer.clear(0, 0, Double.parseDouble(width), Double.parseDouble(height));
    }

    /**
     * Sets scroller of GlycanWebCanvas.
     * @param glycanWebCanvasScroller Scroller of GlycanWebCanvas.
     */
    public void setGlycanWebCanvasScroller(Scroller glycanWebCanvasScroller) {
        this.glycanWebCanvasScroller = glycanWebCanvasScroller;
    }
}
