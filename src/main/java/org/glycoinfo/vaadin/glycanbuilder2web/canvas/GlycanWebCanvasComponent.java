package org.glycoinfo.vaadin.glycanbuilder2web.canvas;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasComponents;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.orderedlayout.Scroller;

/**
 * Component that has GlycanWebCanvas to draw residues and linkages.
 */
@Tag("glycan-web-canvas-component")
@JsModule("./src/glycan-web-canvas-component.ts")
@SuppressWarnings("serial")
public class GlycanWebCanvasComponent extends Component implements HasComponents {
    /**
     * Scroller of GlycanWebCanvas.
     */
    private Scroller glycanWebCanvasScroller;

    /**
     * GlycanWebCanvas instance that draws residues and linkages.
     */
    private GlycanWebCanvas glycanWebCanvas = new GlycanWebCanvas();

    /**
     * Adds GlycanWebCanvas instance to this component.
     */
    public GlycanWebCanvasComponent() {
        glycanWebCanvasScroller = new Scroller(glycanWebCanvas);
        add(glycanWebCanvasScroller);
    }

    /**
     * Gets scroller of GlycanWebCanvas.
     * @return Returns scroller of GlycanWebCanvas.
     */
    public Scroller getGlycanWebCanvasScroller() {
        return glycanWebCanvasScroller;
    }

    /**
     * Gets GlycanWebCanvas instance.
     * @return Returns GlycanWebCanvas instance.
     */
    public GlycanWebCanvas getGlycanWebCanvas() {
        return glycanWebCanvas;
    }
}
