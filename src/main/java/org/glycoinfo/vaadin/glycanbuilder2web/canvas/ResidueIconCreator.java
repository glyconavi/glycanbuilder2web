package org.glycoinfo.vaadin.glycanbuilder2web.canvas;

import java.awt.Rectangle;
import java.io.ByteArrayInputStream;
import java.util.Base64;
import java.util.function.Consumer;

import org.eurocarbdb.application.glycanbuilder.Residue;
import org.eurocarbdb.application.glycanbuilder.ResidueType;
import org.eurocarbdb.application.glycanbuilder.renderutil.ResAngle;
import org.glycoinfo.vaadin.glycanbuilder2web.renderer.GlycanWebResidueRenderer;

import com.vaadin.flow.component.HasComponents;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.server.StreamResource;

/**
 * Creator of residue icon.
 */
public class ResidueIconCreator {
    /**
     * Creates a residue icon using the residue type.
     * @param hasComponents Interface that controls components.
     * @param notation Notation of structures.
     * @param strokeStyle Stroke style for residue.
     * @param fillStyle Fill style for residue icon.
     * @param residueType Residue type that is drawn.
     * @param maxSize Max size of the residue icon.
     * @param resultHandler Result handler for handling imaged icon.
     */
    public void createResidueIcon(HasComponents hasComponents, String notation, String strokeStyle, String fillStyle, ResidueType residueType, int maxSize, Consumer<Image> resultHandler) {
        // Adds canvas for a drawing residue temporarily.
        GlycanWebCanvas glycanWebCanvas = new GlycanWebCanvas(maxSize, maxSize, notation);
        hasComponents.add(glycanWebCanvas);

        // Draws a residue icon.
        GlycanWebResidueRenderer residueRenderer = (GlycanWebResidueRenderer) glycanWebCanvas.getBuilderWorkspace().getGlycanRenderer().getResidueRenderer();
        residueRenderer.setColorOfTextNotation(strokeStyle);
        residueRenderer.setBackgroundOfTextNotation(fillStyle);
        Residue residue = new Residue(residueType);
        Rectangle boundingBox = residueRenderer.computeBoundingBox(residue, false, 1, 1, new ResAngle(), maxSize - 2, maxSize - 2);
        glycanWebCanvas.setSize(boundingBox.width + 2, maxSize);
        residueRenderer.paint(glycanWebCanvas, residue, false, false, null, boundingBox, null, new ResAngle());

        // Images a drawn residue icon.
        glycanWebCanvas.toDataURL(dataURI -> {
            hasComponents.remove(glycanWebCanvas);
            String dataURIBase64 = dataURI.replace("data:image/png;base64,", "");
            byte[] imageBytes = Base64.getDecoder().decode(dataURIBase64.getBytes());
            StreamResource streamResource = new StreamResource("", () -> new ByteArrayInputStream(imageBytes));
            Image image = new Image(streamResource, "");
            resultHandler.accept(image);
        });
    }
}
