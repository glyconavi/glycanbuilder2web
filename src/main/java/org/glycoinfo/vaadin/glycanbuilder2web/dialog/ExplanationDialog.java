package org.glycoinfo.vaadin.glycanbuilder2web.dialog;

import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;

/**
 * Dialog to display message.
 */
public class ExplanationDialog {
    /**
     * Open explanation dialog.
     * @param title Title of the dialog.
     * @param message Message displaying on the dialog.
     */
    public ExplanationDialog(String title, String message) {
        Dialog dialog = new Dialog();
        Div div = new Div();
        VerticalLayout verticalLayout = new VerticalLayout();
        H2 titleH2 = new H2(title);
        titleH2.addClassName("titleOfDialog");
        verticalLayout.add(titleH2);
        Div textArea = new Div(new Text(message));
        textArea.addClassName("textAreaOfDialog");
        verticalLayout.add(textArea);
        Button confirmButton = new Button("OK", mouseClickEvent -> {
            dialog.close();
        });
        confirmButton.addClassName("buttonInDialog");
        verticalLayout.add(confirmButton);
        div.add(verticalLayout);
        dialog.add(div);
        dialog.open();
    }
}
