package org.glycoinfo.vaadin.glycanbuilder2web.dialog;

import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;

/**
 * About Dialog to explain Glycan Builder 2 Web.
 */
public class AboutDialog {
    /**
     * Creates about dialog.
     */
    public AboutDialog() {
        Dialog dialog = new Dialog();
        Div div = new Div();
        VerticalLayout verticalLayout = new VerticalLayout();
        H2 title = new H2("GlycanBuilder4Web");
        title.addClassName("titleOfAbout");
        verticalLayout.add(title);
        H3 version = new H3("Version 1.5.3");
        version.addClassName("versionOfGlycanBuilder4Web");
        verticalLayout.add(version);
        Div textArea1 = new Div(new Text("A glycomic tool for building glycan structures. GlycanBuilder2 is part of the GlyCosmos Project (https://glycosmos.org)."));
        textArea1.addClassName("textAreaOfDialog");
        verticalLayout.add(textArea1);
        Div textArea2 = new Div(new Text("For questions and comments please contact: glyconavi@noguchi.or.jp "));
        textArea2.addClassName("textAreaOfDialog");
        verticalLayout.add(textArea2);
        Div textArea3 = new Div(new Text("Citation: Shinichiro Tsuchiya, et. al., Carbohydrate Research, 445, 2017, Pages 104-116"));
        textArea3.addClassName("textAreaOfDialog");
        verticalLayout.add(textArea3);
        Button confirmButton = new Button("OK", mouseClickEvent -> {
            dialog.close();
        });
        confirmButton.addClassName("buttonInDialog");
        verticalLayout.add(confirmButton);
        div.add(verticalLayout);
        dialog.add(div);
        dialog.open();
    }
}
