package org.glycoinfo.vaadin.glycanbuilder2web.dialog;

import java.util.ArrayList;
import java.util.Map;

import org.eurocarbdb.application.glycanbuilder.converter.GlycanParserFactory;
import org.glycoinfo.vaadin.glycanbuilder2web.canvas.GlycanWebCanvas;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.select.Select;

/**
 * Output string dialog to convert drawing structures to structures string.
 */
public class OutputStringDialog {
    /**
     * Encode type to convert string.
     */
    private Select<String> encodeType = new Select<String>();

    /**
     * Initializes output string dialog.
     * @param glycanWebCanvas Canvas to draw residues.
     */
    public OutputStringDialog(GlycanWebCanvas glycanWebCanvas) {
        initOutputStringDialog(glycanWebCanvas);
    }

    /**
     * Creates output string dialog to export structures string.
     * @param glycanWebCanvas Canvas to draw residues.
     */
    private void initOutputStringDialog(GlycanWebCanvas glycanWebCanvas) {
        Dialog dialog = new Dialog();
        VerticalLayout outputStringDialogLayout = new VerticalLayout();

        // Sets select of encode type.
        H2 title = new H2("String encoded");
        title.addClassName("titleOfDialog");
        outputStringDialogLayout.add(title);
        Map<String, String> encodeTypes = GlycanParserFactory.getExportFormats();
        ArrayList<String> encodeTypeList = new ArrayList<String>();
        for (String key : encodeTypes.keySet()) {
            encodeTypeList.add(key);
        }
        encodeType.setItems(encodeTypeList);
        encodeType.setValue(encodeTypes.keySet().iterator().next());
        encodeType.addClassName("selectInDialog");
        encodeType.addClassName("selectInOutputStringDialog");
        outputStringDialogLayout.add(encodeType);

        // Sets encode and cancel button on dialog.
        HorizontalLayout buttonLayout = new HorizontalLayout();
        Button encodeButton = new Button("Encode");
        encodeButton.addClassName("buttonInDialog");
        encodeButton.addClassName("buttonHorizontalLayoutInDialog");
        addClickListenerOfEncodeButton(dialog, encodeButton, glycanWebCanvas);
        buttonLayout.add(encodeButton);
        Button cancelButton = new Button("Cancel");
        cancelButton.addClassName("buttonInDialog");
        cancelButton.addClassName("buttonHorizontalLayoutInDialog");
        cancelButton.addClassName("button2ndHorizontalLayoutInDialog");
        addClickListenerOfCancelButton(dialog, cancelButton);
        buttonLayout.add(cancelButton);
        outputStringDialogLayout.add(buttonLayout);

        // Displays dialog.
        dialog.add(outputStringDialogLayout);
        dialog.open();
    }

    /**
     * Adds click listener to export button for exporting structures string from encode type.
     * @param dialog Dialog to export structures string.
     * @param encodeButton Encode button to run exporting.
     * @param glycanWebCanvas Canvas to draw residues.
     */
    private void addClickListenerOfEncodeButton(Dialog dialog, Button encodeButton, GlycanWebCanvas glycanWebCanvas) {
        encodeButton.addClickListener(mouseClickEvent -> {
            // Exports structures string from encode type.
            if (glycanWebCanvas.getSelectedStructures().isEmpty()) {
                glycanWebCanvas.getGlycanDocument().exportFromStructure(glycanWebCanvas.getGlycanDocument().getStructures(), encodeType.getValue());
            } else {
                glycanWebCanvas.getGlycanDocument().exportFromStructure(glycanWebCanvas.getSelectedStructures(), encodeType.getValue());
            }
            if (glycanWebCanvas.getGlycanDocument().getString().size() > 0) {
                String exportingStrings = "";
                for(String exportingString : glycanWebCanvas.getGlycanDocument().getString()) {
                    exportingStrings += exportingString + "\n";
                }
                glycanWebCanvas.getGlycanDocument().clearString();
                new OutputTextAreaDialog(exportingStrings);
            }
            dialog.close();
        });
    }

    /**
     * Adds click listener to cancel button for closing the dialog.
     * @param dialog Dialog to setting repetition of residue.
     * @param cancelButton Cancel button for closing the dialog.
     */
    private void addClickListenerOfCancelButton(Dialog dialog, Button cancelButton) {
        cancelButton.addClickListener(mouseClickEvent -> {
            dialog.close();
        });
    }
}
