package org.glycoinfo.vaadin.glycanbuilder2web.dialog;

import java.util.LinkedList;
import java.util.function.Consumer;

import org.eurocarbdb.application.glycanbuilder.Glycan;
import org.eurocarbdb.application.glycanbuilder.Residue;
import org.glycoinfo.vaadin.glycanbuilder2web.canvas.GlycanWebCanvas;

import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;

/**
 * Residue selector dialog to select end point.
 */
public class ResidueSelectorDialog {
    /**
     * Canvas drawn residues and linkages for residue selecting.
     */
    private GlycanWebCanvas residueSelectingCanvas;

    /**
     * Initializes residue selector dialog.
     * @param width Width of residue selecting canvas.
     * @param height Height of residue selecting canvas.
     * @param notation Notation of structures.
     * @param structure Structure to display residue selecting canvas.
     * @param endPoints End points to select end point.
     * @param resultHandler Result handler for handling when clicking ok button.
     */
    public ResidueSelectorDialog(String title, String message, int width, int height, String notation, Glycan structure, LinkedList<Residue> endPoints, Consumer<GlycanWebCanvas> resultHandler) {
        initResidueSelectorDialog(title, message, width, height, notation, structure, endPoints, resultHandler);
    }

    /**
     * Creates residue selector dialog.
     * @param width Width of residue selecting canvas.
     * @param height Height of residue selecting canvas.
     * @param notation Notation of structures.
     * @param structure Structure to display residue selecting canvas.
     * @param endPoints End points to select end point.
     * @param resultHandler Result handler for handling when clicking ok button.
     */
    public void initResidueSelectorDialog(String title, String message, int width, int height, String notation, Glycan structure, LinkedList<Residue> endPoints, Consumer<GlycanWebCanvas> resultHandler) {
        Dialog dialog = new Dialog();
        Div div = new Div();

        // Sets title and message.
        VerticalLayout verticalLayout = new VerticalLayout();
        H2 titleH2 = new H2(title);
        titleH2.addClassName("titleOfDialog");
        verticalLayout.add(titleH2);
        Div textArea = new Div(new Text(message));
        textArea.addClassName("textAreaOfDialog");
        verticalLayout.add(textArea);
        div.add(verticalLayout);

        // Sets residue selecting canvas.
        residueSelectingCanvas = new GlycanWebCanvas(width, height, notation, false, endPoints);
        residueSelectingCanvas.getBuilderWorkspace().getGraphicOptions().SHOW_MASSES_CANVAS = false;
        residueSelectingCanvas.addStructure(structure.getRoot());
        Div imageDiv = new Div();
        imageDiv.add(residueSelectingCanvas);
        imageDiv.addClassName("canvasContainer");
        div.add(imageDiv);

        // Sets ok and cancel button.
        HorizontalLayout buttonLayout = new HorizontalLayout();
        Button okButton = new Button("OK", mouseClickEvent -> {
            resultHandler.accept(residueSelectingCanvas);
            dialog.close();
        });
        okButton.addClassName("buttonInDialog");
        okButton.addClassName("buttonHorizontalLayoutInDialog");
        buttonLayout.add(okButton);
        Button cancelButton = new Button("Cancel", mouseClickEvent -> {
            dialog.close();
        });
        cancelButton.addClassName("buttonInDialog");
        cancelButton.addClassName("buttonHorizontalLayoutInDialog");
        cancelButton.addClassName("button2ndHorizontalLayoutInDialog");
        buttonLayout.add(cancelButton);
        div.add(buttonLayout);

        // Displays dialog.
        dialog.add(div);
        dialog.open();
    }
}
