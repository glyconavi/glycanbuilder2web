package org.glycoinfo.vaadin.glycanbuilder2web.dialog;

import org.eurocarbdb.application.glycanbuilder.massutil.CompositionOptions;
import org.glycoinfo.vaadin.glycanbuilder2web.canvas.GlycanWebCanvas;

import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.textfield.IntegerField;

/**
 * Composition dialog to add specified composition.
 */
public class CompositionDialog {
    /**
     * Number of pentose.
     */
    private IntegerField pentoseNumber = new IntegerField();

    /**
     * Number of hexuronic acid.
     */
    private IntegerField hexuronicAcidNumber = new IntegerField();

    /**
     * Number of hexose.
     */
    private IntegerField hexoseNumber = new IntegerField();

    /**
     * Number of dehydro hexuronic acid.
     */
    private IntegerField dehydroHexuronicAcidNumber = new IntegerField();

    /**
     * Number of heptose.
     */
    private IntegerField heptoseNumber = new IntegerField();

    /**
     * Number of N glycolyl neuraminic acid.
     */
    private IntegerField nGlycolylNeuraminicAcidNumber = new IntegerField();

    /**
     * Number of hexosamine.
     */
    private IntegerField hexosamineNumber = new IntegerField();

    /**
     * Number of N acetyl neuraminic acid.
     */
    private IntegerField nAcetylNeuraminicAcidNumber = new IntegerField();

    /**
     * Number of N acetyl hexomine.
     */
    private IntegerField nAcetylHexoamineNumber = new IntegerField();

    /**
     * Number of lactonized neu 5gc.
     */
    private IntegerField lactonizedNeu5GcNumber = new IntegerField();

    /**
     * Number of deoxy pentose.
     */
    private IntegerField deoxyPentoseNumber = new IntegerField();

    /**
     * Number of lactonized neu 5ac.
     */
    private IntegerField lactonizedNeu5AcNumber = new IntegerField();

    /**
     * Number of deoxy hexose.
     */
    private IntegerField deoxyHexoseNumber = new IntegerField();

    /**
     * Number of kdo.
     */
    private IntegerField kdoNumber = new IntegerField();

    /**
     * Number of dideoxy hexose.
     */
    private IntegerField diDeoxyHexoseNumber = new IntegerField();

    /**
     * Number of kdn.
     */
    private IntegerField kdnNumber = new IntegerField();

    /**
     * Number of methyl hexose.
     */
    private IntegerField methylHexoseNumber = new IntegerField();

    /**
     * Number of muramic acid.
     */
    private IntegerField muramicAcidNumber = new IntegerField();

    /**
     * Initializes composition dialog.
     * @param glycanWebCanvas Canvas to draw residues.
     * @param compositionOptions Composition option.
     */
    public CompositionDialog(GlycanWebCanvas glycanWebCanvas, CompositionOptions compositionOptions) {
        initCompositionDialog(glycanWebCanvas, compositionOptions);
    }

    /**
     * Creates composition dialog to add specified composition.
     * @param glycanWebCanvas Canvas to draw residues.
     * @param compositionOptions Composition option.
     */
    private void initCompositionDialog(GlycanWebCanvas glycanWebCanvas, CompositionOptions compositionOptions) {
        Dialog dialog = new Dialog();
        Div div = new Div();

        // Sets number of pentose.
        HorizontalLayout compositionLayout1 = new HorizontalLayout();
        Div pentoseFullName = new Div(new Text("Pentose"));
        pentoseFullName.addClassName("textAreaOfComposition");
        pentoseFullName.addClassName("fullNameOfTextAreaOnLeft");
        compositionLayout1.add(pentoseFullName);
        Div pentoseOmitName = new Div(new Text("Pen"));
        pentoseOmitName.addClassName("textAreaOfComposition");
        pentoseOmitName.addClassName("omitNameOfPentose");
        compositionLayout1.add(pentoseOmitName);
        compositionLayout1.add(pentoseNumber);
        pentoseNumber.setValue(0);
        pentoseNumber.setMin(0);
        pentoseNumber.setMax(99);
        pentoseNumber.setHasControls(true);

        // Sets number of hexuronic acid.
        Div hexuronicAcidFullName = new Div(new Text("Hexuronic Acid"));
        hexuronicAcidFullName.addClassName("textAreaOfComposition");
        hexuronicAcidFullName.addClassName("fullNameOfTextAreaOnRight");
        compositionLayout1.add(hexuronicAcidFullName);
        Div hexAOmitName = new Div(new Text("HexA"));
        hexAOmitName.addClassName("textAreaOfComposition");
        hexAOmitName.addClassName("omitNameOfHexuronicAcid");
        compositionLayout1.add(hexAOmitName);
        compositionLayout1.add(hexuronicAcidNumber);
        hexuronicAcidNumber.setValue(0);
        hexuronicAcidNumber.setMin(0);
        hexuronicAcidNumber.setMax(99);
        hexuronicAcidNumber.setHasControls(true);
        div.add(compositionLayout1);

        // Sets number of hexose.
        HorizontalLayout compositionLayout2 = new HorizontalLayout();
        Div hexoseFullName = new Div(new Text("Hexose"));
        hexoseFullName.addClassName("textAreaOfComposition");
        hexoseFullName.addClassName("fullNameOfTextAreaOnLeft");
        compositionLayout2.add(hexoseFullName);
        Div hexoseOmitName = new Div(new Text("Hex"));
        hexoseOmitName.addClassName("textAreaOfComposition");
        hexoseOmitName.addClassName("omitNameOfHexose");
        compositionLayout2.add(hexoseOmitName);
        compositionLayout2.add(hexoseNumber);
        hexoseNumber.setValue(0);
        hexoseNumber.setMin(0);
        hexoseNumber.setMax(99);
        hexoseNumber.setHasControls(true);

        // Sets number of dehydro hexuronic acid.
        Div dehydroHexuronicAcidFullName = new Div(new Text("Dehydro Hexuronic Acid"));
        dehydroHexuronicAcidFullName.addClassName("textAreaOfComposition");
        dehydroHexuronicAcidFullName.addClassName("fullNameOfTextAreaOnRight");
        compositionLayout2.add(dehydroHexuronicAcidFullName);
        Div dehydroHexuronicAcidOmitName = new Div(new Text("dHexA"));
        dehydroHexuronicAcidOmitName.addClassName("textAreaOfComposition");
        dehydroHexuronicAcidOmitName.addClassName("omitNameOfDehydroHexuronicAcid");
        compositionLayout2.add(dehydroHexuronicAcidOmitName);
        compositionLayout2.add(dehydroHexuronicAcidNumber);
        dehydroHexuronicAcidNumber.setValue(0);
        dehydroHexuronicAcidNumber.setMin(0);
        dehydroHexuronicAcidNumber.setMax(99);
        dehydroHexuronicAcidNumber.setHasControls(true);
        div.add(compositionLayout2);

        // Sets number of heptose.
        HorizontalLayout compositionLayout3 = new HorizontalLayout();
        Div heptoseFullName = new Div(new Text("Heptose"));
        heptoseFullName.addClassName("textAreaOfComposition");
        heptoseFullName.addClassName("fullNameOfTextAreaOnLeft");
        compositionLayout3.add(heptoseFullName);
        Div heptoseOmitName = new Div(new Text("Hep"));
        heptoseOmitName.addClassName("textAreaOfComposition");
        heptoseOmitName.addClassName("omitNameOfHeptose");
        compositionLayout3.add(heptoseOmitName);
        compositionLayout3.add(heptoseNumber);
        heptoseNumber.setValue(0);
        heptoseNumber.setMin(0);
        heptoseNumber.setMax(99);
        heptoseNumber.setHasControls(true);

        // Sets number of N glycolyl neuraminic acid.
        Div nGlycolylNeuraminicAcidFullName = new Div(new Text("N-Glycolyl Neuraminic Acid"));
        nGlycolylNeuraminicAcidFullName.addClassName("textAreaOfComposition");
        nGlycolylNeuraminicAcidFullName.addClassName("fullNameOfTextAreaOnRight");
        compositionLayout3.add(nGlycolylNeuraminicAcidFullName);
        Div nGlycolylNeuraminicAcidOmitName = new Div(new Text("Neu5Gc"));
        nGlycolylNeuraminicAcidOmitName.addClassName("textAreaOfComposition");
        nGlycolylNeuraminicAcidOmitName.addClassName("omitNameOfNGlycolylNeuraminicAcid");
        compositionLayout3.add(nGlycolylNeuraminicAcidOmitName);
        compositionLayout3.add(nGlycolylNeuraminicAcidNumber);
        nGlycolylNeuraminicAcidNumber.setValue(0);
        nGlycolylNeuraminicAcidNumber.setMin(0);
        nGlycolylNeuraminicAcidNumber.setMax(99);
        nGlycolylNeuraminicAcidNumber.setHasControls(true);
        div.add(compositionLayout3);

        // Sets number of hexosamine.
        HorizontalLayout compositionLayout4 = new HorizontalLayout();
        Div hexosamineFullName = new Div(new Text("Hexosamine"));
        hexosamineFullName.addClassName("textAreaOfComposition");
        hexosamineFullName.addClassName("fullNameOfTextAreaOnLeft");
        compositionLayout4.add(hexosamineFullName);
        Div hexosamineOmitName = new Div(new Text("HexN"));
        hexosamineOmitName.addClassName("textAreaOfComposition");
        hexosamineOmitName.addClassName("omitNameOfHexosamine");
        compositionLayout4.add(hexosamineOmitName);
        compositionLayout4.add(hexosamineNumber);
        hexosamineNumber.setValue(0);
        hexosamineNumber.setMin(0);
        hexosamineNumber.setMax(99);
        hexosamineNumber.setHasControls(true);

        // Sets number of N acetyl neuraminc acid.
        Div nAcetylNeuraminicAcidFullName = new Div(new Text("N-Acetyl Neuraminic Acid"));
        nAcetylNeuraminicAcidFullName.addClassName("textAreaOfComposition");
        nAcetylNeuraminicAcidFullName.addClassName("fullNameOfTextAreaOnRight");
        compositionLayout4.add(nAcetylNeuraminicAcidFullName);
        Div nAcetylNeuraminicAcidOmitName = new Div(new Text("Neu5Ac"));
        nAcetylNeuraminicAcidOmitName.addClassName("textAreaOfComposition");
        nAcetylNeuraminicAcidOmitName.addClassName("omitNameOfNAcetylNeuraminicAcid");
        compositionLayout4.add(nAcetylNeuraminicAcidOmitName);
        compositionLayout4.add(nAcetylNeuraminicAcidNumber);
        nAcetylNeuraminicAcidNumber.setValue(0);
        nAcetylNeuraminicAcidNumber.setMin(0);
        nAcetylNeuraminicAcidNumber.setMax(99);
        nAcetylNeuraminicAcidNumber.setHasControls(true);
        div.add(compositionLayout4);

        // Sets number of N acetyl hexomine.
        HorizontalLayout compositionLayout5 = new HorizontalLayout();
        Div nAcetylHexoamineFullName = new Div(new Text("N-Acetyl Hexoamine"));
        nAcetylHexoamineFullName.addClassName("textAreaOfComposition");
        nAcetylHexoamineFullName.addClassName("fullNameOfTextAreaOnLeft");
        compositionLayout5.add(nAcetylHexoamineFullName);
        Div nAcetylHexoamineOmitName = new Div(new Text("HexNAc"));
        nAcetylHexoamineOmitName.addClassName("textAreaOfComposition");
        nAcetylHexoamineOmitName.addClassName("omitNameOfNAcetylHexoamine");
        compositionLayout5.add(nAcetylHexoamineOmitName);
        compositionLayout5.add(nAcetylHexoamineNumber);
        nAcetylHexoamineNumber.setValue(0);
        nAcetylHexoamineNumber.setMin(0);
        nAcetylHexoamineNumber.setMax(99);
        nAcetylHexoamineNumber.setHasControls(true);

        // Sets number of lactonized neu 5gc.
        Div lactonizedNeu5GcFullName = new Div(new Text("Lactonized Neu5Gc"));
        lactonizedNeu5GcFullName.addClassName("textAreaOfComposition");
        lactonizedNeu5GcFullName.addClassName("fullNameOfTextAreaOnRight");
        compositionLayout5.add(lactonizedNeu5GcFullName);
        Div lactonizedNeu5GcOmitName = new Div(new Text("Neu5Gc-Lac"));
        lactonizedNeu5GcOmitName.addClassName("textAreaOfComposition");
        lactonizedNeu5GcOmitName.addClassName("omitNameOfLactonizedNeu5Gc");
        compositionLayout5.add(lactonizedNeu5GcOmitName);
        compositionLayout5.add(lactonizedNeu5GcNumber);
        lactonizedNeu5GcNumber.setValue(0);
        lactonizedNeu5GcNumber.setMin(0);
        lactonizedNeu5GcNumber.setMax(99);
        lactonizedNeu5GcNumber.setHasControls(true);
        div.add(compositionLayout5);

        // Sets number of deoxy pentose.
        HorizontalLayout compositionLayout6 = new HorizontalLayout();
        Div deoxyPentoseFullName = new Div(new Text("Deoxy-Pentose"));
        deoxyPentoseFullName.addClassName("textAreaOfComposition");
        deoxyPentoseFullName.addClassName("fullNameOfTextAreaOnLeft");
        compositionLayout6.add(deoxyPentoseFullName);
        Div deoxyPentoseOmitName = new Div(new Text("dPen"));
        deoxyPentoseOmitName.addClassName("textAreaOfComposition");
        deoxyPentoseOmitName.addClassName("omitNameOfDeoxyPentose");
        compositionLayout6.add(deoxyPentoseOmitName);
        compositionLayout6.add(deoxyPentoseNumber);
        deoxyPentoseNumber.setValue(0);
        deoxyPentoseNumber.setMin(0);
        deoxyPentoseNumber.setMax(99);
        deoxyPentoseNumber.setHasControls(true);

        // Sets number of lactonized neu 5ac.
        Div lactonizedNeu5AcFullName = new Div(new Text("Lactonized Neu5Ac"));
        lactonizedNeu5AcFullName.addClassName("textAreaOfComposition");
        lactonizedNeu5AcFullName.addClassName("fullNameOfTextAreaOnRight");
        compositionLayout6.add(lactonizedNeu5AcFullName);
        Div lactonizedNeu5AcOmitName = new Div(new Text("Neu5Ac-Lac"));
        lactonizedNeu5AcOmitName.addClassName("textAreaOfComposition");
        lactonizedNeu5AcOmitName.addClassName("omitNameOfLactonizedNeu5Ac");
        compositionLayout6.add(lactonizedNeu5AcOmitName);
        compositionLayout6.add(lactonizedNeu5AcNumber);
        lactonizedNeu5AcNumber.setValue(0);
        lactonizedNeu5AcNumber.setMin(0);
        lactonizedNeu5AcNumber.setMax(99);
        lactonizedNeu5AcNumber.setHasControls(true);
        div.add(compositionLayout6);

        // Sets number of deoxy hexose.
        HorizontalLayout compositionLayout7 = new HorizontalLayout();
        Div deoxyHexoseFullName = new Div(new Text("Deoxy-Hexose"));
        deoxyHexoseFullName.addClassName("textAreaOfComposition");
        deoxyHexoseFullName.addClassName("fullNameOfTextAreaOnLeft");
        compositionLayout7.add(deoxyHexoseFullName);
        Div deoxyHexoseOmitName = new Div(new Text("dHex"));
        deoxyHexoseOmitName.addClassName("textAreaOfComposition");
        deoxyHexoseOmitName.addClassName("omitNameOfDeoxyHexose");
        compositionLayout7.add(deoxyHexoseOmitName);
        compositionLayout7.add(deoxyHexoseNumber);
        deoxyHexoseNumber.setValue(0);
        deoxyHexoseNumber.setMin(0);
        deoxyHexoseNumber.setMax(99);
        deoxyHexoseNumber.setHasControls(true);

        // Sets number of kdo.
        Div kdoFullName = new Div(new Text("KDO"));
        kdoFullName.addClassName("textAreaOfComposition");
        kdoFullName.addClassName("fullNameOfTextAreaOnRight");
        compositionLayout7.add(kdoFullName);
        Div kdoOmitName = new Div(new Text("KDO"));
        kdoOmitName.addClassName("textAreaOfComposition");
        kdoOmitName.addClassName("omitNameOfKDO");
        compositionLayout7.add(kdoOmitName);
        compositionLayout7.add(kdoNumber);
        kdoNumber.setValue(0);
        kdoNumber.setMin(0);
        kdoNumber.setMax(99);
        kdoNumber.setHasControls(true);
        div.add(compositionLayout7);

        // Sets number of dideoxy hexose.
        HorizontalLayout compositionLayout8 = new HorizontalLayout();
        Div diDeoxyHexoseFullName = new Div(new Text("DiDeoxy-Hexose"));
        diDeoxyHexoseFullName.addClassName("textAreaOfComposition");
        diDeoxyHexoseFullName.addClassName("fullNameOfTextAreaOnLeft");
        compositionLayout8.add(diDeoxyHexoseFullName);
        Div diDeoxyHexoseOmitName = new Div(new Text("ddHex"));
        diDeoxyHexoseOmitName.addClassName("textAreaOfComposition");
        diDeoxyHexoseOmitName.addClassName("omitNameOfDiDeoxyHexose");
        compositionLayout8.add(diDeoxyHexoseOmitName);
        compositionLayout8.add(diDeoxyHexoseNumber);
        diDeoxyHexoseNumber.setValue(0);
        diDeoxyHexoseNumber.setMin(0);
        diDeoxyHexoseNumber.setMax(99);
        diDeoxyHexoseNumber.setHasControls(true);

        // Sets number of kdn.
        Div kdnFullName = new Div(new Text("KDN"));
        kdnFullName.addClassName("textAreaOfComposition");
        kdnFullName.addClassName("fullNameOfTextAreaOnRight");
        compositionLayout8.add(kdnFullName);
        Div kdnOmitName = new Div(new Text("KDN"));
        kdnOmitName.addClassName("textAreaOfComposition");
        kdnOmitName.addClassName("omitNameOfKDN");
        compositionLayout8.add(kdnOmitName);
        compositionLayout8.add(kdnNumber);
        kdnNumber.setValue(0);
        kdnNumber.setMin(0);
        kdnNumber.setMax(99);
        kdnNumber.setHasControls(true);
        div.add(compositionLayout8);

        // Sets number of methyl hexose.
        HorizontalLayout compositionLayout9 = new HorizontalLayout();
        Div methylHexoseFullName = new Div(new Text("Methyl-Hexose"));
        methylHexoseFullName.addClassName("textAreaOfComposition");
        methylHexoseFullName.addClassName("fullNameOfTextAreaOnLeft");
        compositionLayout9.add(methylHexoseFullName);
        Div methylHexoseOmitName = new Div(new Text("MeHex"));
        methylHexoseOmitName.addClassName("textAreaOfComposition");
        methylHexoseOmitName.addClassName("omitNameOfMethylHexose");
        compositionLayout9.add(methylHexoseOmitName);
        compositionLayout9.add(methylHexoseNumber);
        methylHexoseNumber.setValue(0);
        methylHexoseNumber.setMin(0);
        methylHexoseNumber.setMax(99);
        methylHexoseNumber.setHasControls(true);

        // Sets number of muramic acid.
        Div muramicAcidFullName = new Div(new Text("Muramic Acid"));
        muramicAcidFullName.addClassName("textAreaOfComposition");
        muramicAcidFullName.addClassName("fullNameOfTextAreaOnRight");
        compositionLayout9.add(muramicAcidFullName);
        Div muramicAcidOmitName = new Div(new Text("Mur"));
        muramicAcidOmitName.addClassName("textAreaOfComposition");
        muramicAcidOmitName.addClassName("omitNameOfMuramicAcid");
        compositionLayout9.add(muramicAcidOmitName);
        compositionLayout9.add(muramicAcidNumber);
        muramicAcidNumber.setValue(0);
        muramicAcidNumber.setMin(0);
        muramicAcidNumber.setMax(99);
        muramicAcidNumber.setHasControls(true);
        div.add(compositionLayout9);

        // Sets ok and cancel button on dialog.
        HorizontalLayout buttonLayout = new HorizontalLayout();
        Button okButton = new Button("OK");
        okButton.addClassName("buttonInDialog");
        okButton.addClassName("buttonHorizontalLayoutInDialog");
        okButton.addClassName("buttonInCompositionDialog");
        addClickListenerOfOKButton(dialog, okButton, glycanWebCanvas, compositionOptions);
        buttonLayout.add(okButton);
        Button cancelButton = new Button("Cancel");
        cancelButton.addClassName("buttonInDialog");
        cancelButton.addClassName("buttonHorizontalLayoutInDialog");
        cancelButton.addClassName("button2ndHorizontalLayoutInDialog");
        cancelButton.addClassName("buttonInCompositionDialog");
        addClickListenerOfCancelButton(dialog, cancelButton);
        buttonLayout.add(cancelButton);
        div.add(buttonLayout);

        // Displays dialog.
        dialog.add(div);
        dialog.open();
    }

    /**
     * Adds click listener to ok button for adding specified composition.
     * @param dialog Dialog to add specified composition.
     * @param okButton OK button adding specified composition.
     * @param glycanWebCanvas Canvas to draw residues.
     * @param compositionOptions Composition option.
     */
    private void addClickListenerOfOKButton(Dialog dialog, Button okButton, GlycanWebCanvas glycanWebCanvas, CompositionOptions compositionOptions) {
        okButton.addClickListener(mouseClickEvent -> {
            // Sets value of compositions.
            compositionOptions.PEN = pentoseNumber.getValue();
            compositionOptions.HEXA = hexuronicAcidNumber.getValue();
            compositionOptions.HEX = hexoseNumber.getValue();
            compositionOptions.DHEXA = dehydroHexuronicAcidNumber.getValue();
            compositionOptions.HEP = heptoseNumber.getValue();
            compositionOptions.NEU5GC = nGlycolylNeuraminicAcidNumber.getValue();
            compositionOptions.HEXN = hexosamineNumber.getValue();
            compositionOptions.NEU5AC = nAcetylNeuraminicAcidNumber.getValue();
            compositionOptions.HEXNAC = nAcetylHexoamineNumber.getValue();
            compositionOptions.NEU5GCLAC = lactonizedNeu5GcNumber.getValue();
            compositionOptions.DPEN = deoxyPentoseNumber.getValue();
            compositionOptions.NEU5ACLAC = lactonizedNeu5AcNumber.getValue();
            compositionOptions.DHEX = deoxyHexoseNumber.getValue();
            compositionOptions.KDO = kdoNumber.getValue();
            compositionOptions.DDHEX = diDeoxyHexoseNumber.getValue();
            compositionOptions.KDN = kdnNumber.getValue();
            compositionOptions.MEHEX = methylHexoseNumber.getValue();
            compositionOptions.MUR = muramicAcidNumber.getValue();

            // Adds composition.
            glycanWebCanvas.addComposition();

            // Closes dialog.
            dialog.close();
        });
    }

    /**
     * Adds click listener to cancel button for closing the dialog.
     * @param dialog Dialog to setting repetition of residue.
     * @param cancelButton Cancel button for closing the dialog.
     */
    private void addClickListenerOfCancelButton(Dialog dialog, Button cancelButton) {
        cancelButton.addClickListener(mouseClickEvent -> {
            dialog.close();
        });
    }
}
