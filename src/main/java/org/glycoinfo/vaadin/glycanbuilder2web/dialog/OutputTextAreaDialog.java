package org.glycoinfo.vaadin.glycanbuilder2web.dialog;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextArea;

/**
 * Text area dialog.
 */
public class OutputTextAreaDialog {
    /**
     * Text area to input text.
     */
    private TextArea textArea = new TextArea();

    /**
     * Creates text area dialog.
     * @param text
     */
    public OutputTextAreaDialog(String text) {
        Dialog dialog = new Dialog();
        VerticalLayout verticalLayout = new VerticalLayout();

        // Sets title.
        H2 title = new H2("String encoded");
        title.addClassName("titleOfOutputTextArea");
        verticalLayout.add(title);

        // Sets text area of structures string.
        VerticalLayout structuresStringLayout = new VerticalLayout();
        structuresStringLayout.add(textArea);
        textArea.getStyle().set("width", "286px");
        textArea.getStyle().set("height", "177px");
        textArea.setValue(text);
        structuresStringLayout.add(textArea);
        verticalLayout.add(structuresStringLayout);

        // Sets confirmButton button on dialog.
        Button confirmButton = new Button("OK", mouseClickEvent -> {
            dialog.close();
        });
        confirmButton.addClassName("buttonInDialog");
        confirmButton.addClassName("buttonInOutputTextAreaDialog");
        verticalLayout.add(confirmButton);

        // Displays dialog.
        dialog.add(verticalLayout);
        dialog.open();
    }
}
