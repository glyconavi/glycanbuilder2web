package org.glycoinfo.vaadin.glycanbuilder2web.dialog;

import org.eurocarbdb.application.glycanbuilder.Residue;
import org.glycoinfo.vaadin.glycanbuilder2web.canvas.GlycanWebCanvas;

import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.textfield.IntegerField;

/**
 * Dialog to setting repetition of residue.
 */
public class ResidueRepetitionPropertiesDialog {
    /**
     * Text field of min repetition.
     */
    private IntegerField minRepetitionsNumber = new IntegerField();

    /**
     * Text field of max repetition.
     */
    private IntegerField maxRepetitionsNumber = new IntegerField();

    /**
     * Initializes residue repetition properties dialog.
     * @param currentResidue Current residue selecting on canvas.
     * @param glycanWebCanvas GlycanWebCanvas to draw residues.
     */
    public ResidueRepetitionPropertiesDialog(Residue currentResidue, GlycanWebCanvas glycanWebCanvas) {
        initResidueRepetitionPropertiesDialog(currentResidue, glycanWebCanvas);
    }

    /**
     * Creates residue repetition properties dialog to setting repetition of residue.
     * @param currentResidue Current residue selecting on canvas.
     * @param glycanWebCanvas Canvas to draw residues.
     */
    private void initResidueRepetitionPropertiesDialog(Residue currentResidue, GlycanWebCanvas glycanWebCanvas) {
        Dialog dialog = new Dialog();
        Div div = new Div();

        // Sets min repetition text field on dialog.
        HorizontalLayout minRepetitionsLayout = new HorizontalLayout();
        Div minRepetitionsText = new Div(new Text("Min"));
        minRepetitionsText.addClassName("minRepetitionsText");
        minRepetitionsLayout.add(minRepetitionsText);
        minRepetitionsLayout.add(minRepetitionsNumber);
        minRepetitionsNumber.setValue(currentResidue.getMinRepetitions());
        minRepetitionsNumber.setMin(-1);
        minRepetitionsNumber.setHasControls(true);
        div.add(minRepetitionsLayout);

        // Sets max repetition text field on dialog.
        HorizontalLayout maxRepetitionsLayout = new HorizontalLayout();
        Div maxRepetitionsText = new Div(new Text("Max"));
        maxRepetitionsText.addClassName("maxRepetitionsText");
        maxRepetitionsLayout.add(maxRepetitionsText);
        maxRepetitionsLayout.add(maxRepetitionsNumber);
        maxRepetitionsNumber.setValue(currentResidue.getMaxRepetitions());
        maxRepetitionsNumber.setMin(-1);
        maxRepetitionsNumber.setHasControls(true);
        div.add(maxRepetitionsLayout);

        // Sets ok and cancel button on dialog.
        HorizontalLayout buttonLayout = new HorizontalLayout();
        Button okButton = new Button("OK");
        okButton.addClassName("buttonInDialog");
        okButton.addClassName("buttonHorizontalLayoutInDialog");
        okButton.addClassName("buttonInCompositionDialog");
        addClickListenerOfOKButton(dialog, okButton, currentResidue, glycanWebCanvas);
        buttonLayout.add(okButton);
        Button cancelButton = new Button("Cancel");
        cancelButton.addClassName("buttonInDialog");
        cancelButton.addClassName("buttonHorizontalLayoutInDialog");
        cancelButton.addClassName("button2ndHorizontalLayoutInDialog");
        cancelButton.addClassName("buttonInCompositionDialog");
        addClickListenerOfCancelButton(dialog, cancelButton);
        buttonLayout.add(cancelButton);
        div.add(buttonLayout);

        // Displays dialog.
        dialog.add(div);
        dialog.open();
    }

    /**
     * Adds click listener to ok button for setting repetition of residue.
     * @param dialog Dialog to setting repetition of residue.
     * @param okButton OK button for setting repetition of residue.
     * @param currentResidue Current residue selecting on canvas.
     * @param glycanWebCanvas Canvas to draw residues.
     */
    private void addClickListenerOfOKButton(Dialog dialog, Button okButton, Residue currentResidue, GlycanWebCanvas glycanWebCanvas) {
        okButton.addClickListener(mouseClickEvent -> {
            currentResidue.setMinRepetitions(String.valueOf(minRepetitionsNumber.getValue()));
            currentResidue.setMaxRepetitions(String.valueOf(maxRepetitionsNumber.getValue()));
            glycanWebCanvas.updateCanvas();
            dialog.close();
        });
    }

    /**
     * Adds click listener to cancel button for closing the dialog.
     * @param dialog Dialog to setting repetition of residue.
     * @param cancelButton Cancel button for closing the dialog.
     */
    private void addClickListenerOfCancelButton(Dialog dialog, Button cancelButton) {
        cancelButton.addClickListener(mouseClickEvent -> {
            dialog.close();
        });
    }
}
