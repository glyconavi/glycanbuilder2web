package org.glycoinfo.vaadin.glycanbuilder2web.dialog;

import org.eurocarbdb.application.glycanbuilder.util.GraphicOptions;
import org.glycoinfo.vaadin.glycanbuilder2web.canvas.GlycanWebCanvas;

import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.select.Select;
import com.vaadin.flow.component.textfield.IntegerField;

/**
 * Display settings dialog to tweak canvas settings.
 */
public class DisplaySettingsDialog {
    /**
     * Select of display type.
     */
    private Select<String> displayType = new Select<String>();

    /**
     * Checkbox of show linkage info.
     */
    private Checkbox showLinkageInfo = new Checkbox();

    /**
     * Size of residue.
     */
    private IntegerField residueSize = new IntegerField();

    /**
     * Size of residue text size.
     */
    private IntegerField residueTextSize = new IntegerField();

    /**
     * Size of composition text size.
     */
    private IntegerField compositionTextSize = new IntegerField();

    /**
     * Size of linkage info text size.
     */
    private IntegerField linkageInfoTextSize = new IntegerField();

    /**
     * Size of space between residues.
     */
    private IntegerField spaceBetweenResidues = new IntegerField();

    /**
     * Size of space between border residues.
     */
    private IntegerField spaceBetweenBorderResidues = new IntegerField();

    /**
     * Size of space between structures.
     */
    private IntegerField spaceBetweenStructures = new IntegerField();

    /**
     * Size of space before mass text.
     */
    private IntegerField spaceBeforeMass = new IntegerField();

    /**
     * Size of mass text size.
     */
    private IntegerField massTextSize = new IntegerField();

    /**
     * Initializes display settings dialog.
     * @param glycanWebCanvas Canvas drawn residues and linkages.
     * @param graphicOptions Graphic options.
     */
    public DisplaySettingsDialog(GlycanWebCanvas glycanWebCanvas, GraphicOptions graphicOptions) {
        initDisplaySettingsDialog(glycanWebCanvas, graphicOptions);
    }

    /**
     * Creates display settings dialog to tweak canvas settings.
     * @param glycanWebCanvas Canvas drawn residues and linkages.
     * @param graphicOptions Graphic options.
     */
    private void initDisplaySettingsDialog(GlycanWebCanvas glycanWebCanvas, GraphicOptions graphicOptions) {
        Dialog dialog = new Dialog();
        Div div = new Div();

        // Sets select of display type.
        HorizontalLayout displayTypeLayout = new HorizontalLayout();
        Div displayTypeText = new Div(new Text("Display type"));
        displayTypeText.addClassName("textOfDisplayType");
        displayTypeLayout.add(displayTypeText);
        displayType.setItems(GraphicOptions.DISPLAY_NORMALINFO, GraphicOptions.DISPLAY_NORMAL, GraphicOptions.DISPLAY_COMPACT, GraphicOptions.DISPLAY_CUSTOM);
        displayType.addClassName("selectInDialog");
        displayType.addClassName("selectInDisplaySettingDialog");
        addValueChangeListenerOfDisplayType();
        displayTypeLayout.add(displayType);
        div.add(displayTypeLayout);

        // Sets checkbox of show linkage info.
        HorizontalLayout showHorizontalLayout = new HorizontalLayout();
        Div showLinkageInfoText = new Div(new Text("Show linkage info"));
        showLinkageInfoText.addClassName("textOfShowLinkageInfo");
        showHorizontalLayout.add(showLinkageInfoText);
        showLinkageInfo.addClassName("checkboxInDialog");
        showHorizontalLayout.add(showLinkageInfo);
        div.add(showHorizontalLayout);

        // Sets size of residue.
        HorizontalLayout residueSizeLayout = new HorizontalLayout();
        Div residueSizeText = new Div(new Text("Residue size"));
        residueSizeText.addClassName("textOfResidueSize");
        residueSizeLayout.add(residueSizeText);
        residueSize.setMin(1);
        residueSize.setMax(100);
        residueSize.setHasControls(true);
        residueSizeLayout.add(residueSize);
        div.add(residueSizeLayout);

        // Sets size of residue text size.
        HorizontalLayout residueTextSizeLayout = new HorizontalLayout();
        Div residueTextSizeText = new Div(new Text("Residue text size"));
        residueTextSizeText.addClassName("textOfResidueTextSize");
        residueTextSizeLayout.add(residueTextSizeText);
        residueTextSize.setMin(1);
        residueTextSize.setMax(40);
        residueTextSize.setHasControls(true);
        residueTextSizeLayout.add(residueTextSize);
        div.add(residueTextSizeLayout);

        // Sets size of composition text size.
        HorizontalLayout compositionTextSizeLayout = new HorizontalLayout();
        Div compositionTextSizeText = new Div(new Text("Composition text size"));
        compositionTextSizeText.addClassName("textOfCompositionTextSize");
        compositionTextSizeLayout.add(compositionTextSizeText);
        compositionTextSize.setMin(1);
        compositionTextSize.setMax(40);
        compositionTextSize.setHasControls(true);
        compositionTextSizeLayout.add(compositionTextSize);
        div.add(compositionTextSizeLayout);

        // Sets size of linkage info text size.
        HorizontalLayout linkageInfoTextSizeLayout = new HorizontalLayout();
        Div linkageInfoTextSizeText = new Div(new Text("Linkage info text size"));
        linkageInfoTextSizeText.addClassName("textOfLinkageInfoTextSize");
        linkageInfoTextSizeLayout.add(linkageInfoTextSizeText);
        linkageInfoTextSize.setMin(1);
        linkageInfoTextSize.setMax(40);
        linkageInfoTextSize.setHasControls(true);
        linkageInfoTextSizeLayout.add(linkageInfoTextSize);
        div.add(linkageInfoTextSizeLayout);

        // Sets size of space between residues.
        HorizontalLayout spaceBetweenResiduesLayout = new HorizontalLayout();
        Div spaceBetweenResiduesText = new Div(new Text("Space between residues"));
        spaceBetweenResiduesText.addClassName("textOfSpaceBetweenResidues");
        spaceBetweenResiduesLayout.add(spaceBetweenResiduesText);
        spaceBetweenResidues.setMin(1);
        spaceBetweenResidues.setMax(40);
        spaceBetweenResidues.setHasControls(true);
        spaceBetweenResiduesLayout.add(spaceBetweenResidues);
        div.add(spaceBetweenResiduesLayout);

        // Sets size of space between border residues.
        HorizontalLayout spaceBetweenBorderResiduesLayout = new HorizontalLayout();
        Div spaceBetweenBorderResiduesText = new Div(new Text("Space between border residues"));
        spaceBetweenBorderResiduesText.addClassName("textOfSpaceBetweenBorderResidues");
        spaceBetweenBorderResiduesLayout.add(spaceBetweenBorderResiduesText);
        spaceBetweenBorderResidues.setMin(1);
        spaceBetweenBorderResidues.setMax(40);
        spaceBetweenBorderResidues.setHasControls(true);
        spaceBetweenBorderResiduesLayout.add(spaceBetweenBorderResidues);
        div.add(spaceBetweenBorderResiduesLayout);

        // Sets size of space between structures.
        HorizontalLayout spaceBetweenStructuresLayout = new HorizontalLayout();
        Div spaceBetweenStructuresText = new Div(new Text("Space between structures"));
        spaceBetweenStructuresText.addClassName("textOfSpaceBetweenStructures");
        spaceBetweenStructuresLayout.add(spaceBetweenStructuresText);
        spaceBetweenStructures.setMin(1);
        spaceBetweenStructures.setMax(100);
        spaceBetweenStructures.setHasControls(true);
        spaceBetweenStructuresLayout.add(spaceBetweenStructures);
        div.add(spaceBetweenStructuresLayout);

        // Sets size of space before mass text.
        HorizontalLayout spaceBeforeMassLayout = new HorizontalLayout();
        Div spaceBeforeMassText = new Div(new Text("Space before mass text"));
        spaceBeforeMassText.addClassName("textOfSpaceBeforeMass");
        spaceBeforeMassLayout.add(spaceBeforeMassText);
        spaceBeforeMass.setMin(1);
        spaceBeforeMass.setMax(100);
        spaceBeforeMass.setHasControls(true);
        spaceBeforeMassLayout.add(spaceBeforeMass);
        div.add(spaceBeforeMassLayout);

        // Sets size of mass text size.
        HorizontalLayout massTextSizeLayout = new HorizontalLayout();
        Div massTextSizeText = new Div(new Text("Mass text size"));
        massTextSizeText.addClassName("textOfMassTextSize");
        massTextSizeLayout.add(massTextSizeText);
        massTextSize.setMin(1);
        massTextSize.setMax(40);
        massTextSize.setHasControls(true);
        massTextSizeLayout.add(massTextSize);
        div.add(massTextSizeLayout);

        // Sets display settings.
        setDisplaySettings(graphicOptions);

        // Sets OK and cancel buttons.
        HorizontalLayout buttonLayout = new HorizontalLayout();
        Button okButton = new Button("OK");
        okButton.addClassName("buttonInDialog");
        okButton.addClassName("buttonHorizontalLayoutInDialog");
        addClickListenerOfOKButton(dialog, okButton, glycanWebCanvas, graphicOptions);
        buttonLayout.add(okButton);
        Button cancelButton = new Button("Cancel");
        cancelButton.addClassName("buttonInDialog");
        cancelButton.addClassName("buttonHorizontalLayoutInDialog");
        cancelButton.addClassName("button2ndHorizontalLayoutInDialog");
        addClickListenerOfCancelButton(dialog, cancelButton);
        buttonLayout.add(cancelButton);
        div.add(buttonLayout);

        dialog.add(div);
        dialog.open();
    }

    /**
     * Sets display settings.
     * @param graphicOptions Graphic options.
     */
    private void setDisplaySettings(GraphicOptions graphicOptions) {
        displayType.setValue(graphicOptions.DISPLAY);
        showLinkageInfo.setValue(graphicOptions.SHOW_INFO);
        residueSize.setValue(graphicOptions.NODE_SIZE);
        residueTextSize.setValue(graphicOptions.NODE_FONT_SIZE);
        compositionTextSize.setValue(graphicOptions.COMPOSITION_FONT_SIZE);
        linkageInfoTextSize.setValue(graphicOptions.LINKAGE_INFO_SIZE);
        spaceBetweenResidues.setValue(graphicOptions.NODE_SPACE);
        spaceBetweenBorderResidues.setValue(graphicOptions.NODE_SUB_SPACE);
        spaceBetweenStructures.setValue(graphicOptions.STRUCTURES_SPACE);
        spaceBeforeMass.setValue(graphicOptions.MASS_TEXT_SPACE);
        massTextSize.setValue(graphicOptions.MASS_TEXT_SIZE);
    }

    /**
     * Adds value change listener to display type for setting canvas settings.
     */
    private void addValueChangeListenerOfDisplayType() {
        displayType.addValueChangeListener(valueChangeEvent -> {
            setDisplaySettings(new GraphicOptions(valueChangeEvent.getValue()));
        });
    }

    /**
     * Adds click listener to OK button for tweaking canvas settings.
     * @param dialog Dialog to setting display settings.
     * @param okButton OK button for setting display settings.
     * @param glycanWebCanvas Canvas drawn residues and linkages.
     * @param graphicOptions Graphic options.
     */
    private void addClickListenerOfOKButton(Dialog dialog, Button okButton, GlycanWebCanvas glycanWebCanvas, GraphicOptions graphicOptions) {
        okButton.addClickListener(mouseClickEvent -> {
            // If display type is custom, Sets canvas settings.
            if (displayType.getValue().equals(GraphicOptions.DISPLAY_CUSTOM)) {
                graphicOptions.SHOW_INFO_CUSTOM = showLinkageInfo.getValue();
                graphicOptions.NODE_SIZE_CUSTOM = residueSize.getValue();
                graphicOptions.NODE_FONT_SIZE_CUSTOM = residueTextSize.getValue();
                graphicOptions.COMPOSITION_FONT_SIZE_CUSTOM = compositionTextSize.getValue();
                graphicOptions.LINKAGE_INFO_SIZE_CUSTOM = linkageInfoTextSize.getValue();
                graphicOptions.NODE_SPACE_CUSTOM = spaceBetweenResidues.getValue();
                graphicOptions.NODE_SUB_SPACE_CUSTOM = spaceBetweenBorderResidues.getValue();
                graphicOptions.STRUCTURES_SPACE_CUSTOM = spaceBetweenStructures.getValue();
                graphicOptions.MASS_TEXT_SPACE_CUSTOM = spaceBeforeMass.getValue();
                graphicOptions.MASS_TEXT_SIZE_CUSTOM = massTextSize.getValue();
            }

            // Sets display type.
            graphicOptions.setDisplay(displayType.getValue());

            // Updates the canvas with the information of documents, dictionaries and options.
            glycanWebCanvas.updateCanvas();

            // Closes dialog.
            dialog.close();
        });
    }

    /**
     * Adds click listener to cancel button for closing the dialog.
     * @param dialog Dialog to setting display settings.
     * @param cancelButton Cancel button for closing the dialog.
     */
    private void addClickListenerOfCancelButton(Dialog dialog, Button cancelButton) {
        cancelButton.addClickListener(mouseClickEvent -> {
            dialog.close();
        });
    }
}
