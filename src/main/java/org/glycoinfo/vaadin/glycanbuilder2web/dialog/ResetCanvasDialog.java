package org.glycoinfo.vaadin.glycanbuilder2web.dialog;

import org.glycoinfo.vaadin.glycanbuilder2web.canvas.GlycanWebCanvas;

import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;

/**
 * Reset canvas dialog to reset the canvas.
 */
public class ResetCanvasDialog {
    /**
     * Creates reset canvas dialog.
     * @param glycanWebCanvas Canvas drawn residues and linkages.
     */
    public ResetCanvasDialog(GlycanWebCanvas glycanWebCanvas) {
        Dialog dialog = new Dialog();
        Div div = new Div();
        VerticalLayout verticalLayout = new VerticalLayout();
        verticalLayout.add(new Text("Are you sure want to reset the canvas?"));
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        Button okButton = new Button("OK", mouseClickEvent -> {
            glycanWebCanvas.getGlycanDocument().init();
            glycanWebCanvas.updateCanvas();
            dialog.close();
        });
        okButton.addClassName("buttonInDialog");
        okButton.addClassName("buttonHorizontalLayoutInDialog");
        horizontalLayout.add(okButton);
        Button cancelButton = new Button("Cancel", mouseClickEvent -> {
            dialog.close();
        });
        cancelButton.addClassName("buttonInDialog");
        cancelButton.addClassName("buttonHorizontalLayoutInDialog");
        cancelButton.addClassName("button2ndHorizontalLayoutInDialog");
        horizontalLayout.add(cancelButton);
        verticalLayout.add(horizontalLayout);
        div.add(verticalLayout);
        dialog.add(div);
        dialog.open();
    }
}
