package org.glycoinfo.vaadin.glycanbuilder2web.dialog;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.Anchor;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.html.IFrame;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.server.StreamResource;

/**
 * SVG dialog to display image.
 */
public class SVGDialog {
    /**
     * Creates svg dialog to display svg.
     * @param href URL of svg.
     * @param streamResource Stream resource to be used when downloading svg.
     */
    public SVGDialog(String href, StreamResource streamResource) {
        Dialog dialog = new Dialog();
        Div div = new Div();
        VerticalLayout verticalLayout = new VerticalLayout();
        H2 title = new H2("Download image");
        title.addClassName("titleOfDialog");
        verticalLayout.add(title);
        Div iFrameDiv = new Div();
        iFrameDiv.addClassName("imageContainer");
        IFrame iFrame = new IFrame(href);
        iFrameDiv.add(iFrame);
        verticalLayout.add(iFrameDiv);
        HorizontalLayout buttonLayout = new HorizontalLayout();
        Anchor okAnchor = new Anchor(streamResource, "OK");
        okAnchor.getElement().setAttribute("download", "structures.svg");
        okAnchor.addClassName("okAnchor");
        buttonLayout.add(okAnchor);
        Button cancelButton = new Button("Cancel", mouseClickEvent -> {
            dialog.close();
        });
        cancelButton.addClassName("buttonInDialog");
        cancelButton.addClassName("buttonHorizontalLayoutInDialog");
        cancelButton.addClassName("button2ndHorizontalLayoutInDialog");
        buttonLayout.add(cancelButton);
        verticalLayout.add(buttonLayout);
        div.add(verticalLayout);
        dialog.add(div);
        dialog.open();
    }
}
