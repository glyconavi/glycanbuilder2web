package org.glycoinfo.vaadin.glycanbuilder2web.dialog;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Map;

import org.eurocarbdb.application.glycanbuilder.converter.GlycanParserFactory;
import org.glycoinfo.vaadin.glycanbuilder2web.canvas.GlycanWebCanvas;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.select.Select;
import com.vaadin.flow.component.textfield.TextArea;

/**
 * Import string dialog to convert structures string to drawing structures.
 */
public class ImportStringDialog {
    /**
     * Encode type to convert string.
     */
    private Select<String> encodeType = new Select<String>();

    /**
     * Text area for structures string.
     */
    private TextArea structuresStringTextArea = new TextArea();

    /**
     * Initializes import string dialog.
     * @param glycanWebCanvas Canvas to draw residues.
     */
    public ImportStringDialog(GlycanWebCanvas glycanWebCanvas) {
        initImportStringDialog(glycanWebCanvas);
    }

    /**
     * Creates import string dialog to import structures string to the canvas.
     * @param glycanWebCanvas Canvas to draw residues.
     */
    private void initImportStringDialog(GlycanWebCanvas glycanWebCanvas) {
        Dialog dialog = new Dialog();
        VerticalLayout importStringDialogLayout = new VerticalLayout();

        // Sets select of encode type.
        H2 title = new H2("Import seqeunce format");
        title.addClassName("titleOfDialog");
        importStringDialogLayout.add(title);
        Map<String, String> encodeTypes = GlycanParserFactory.getImportFormats(true);
        ArrayList<String> encodeTypeList = new ArrayList<String>();
        for (String key : encodeTypes.keySet()) {
            encodeTypeList.add(key);
        }
        encodeType.setItems(encodeTypeList);
        encodeType.setValue(encodeTypes.keySet().iterator().next());
        encodeType.addClassName("selectInDialog");
        importStringDialogLayout.add(encodeType);

        // Sets text area of structures string.
        VerticalLayout structuresStringLayout = new VerticalLayout();
        structuresStringTextArea.getStyle().set("width", "286px");
        structuresStringTextArea.getStyle().set("height", "177px");
        structuresStringLayout.add(structuresStringTextArea);
        importStringDialogLayout.add(structuresStringTextArea);

        // Sets import and cancel button on dialog.
        HorizontalLayout buttonLayout = new HorizontalLayout();
        Button importButton = new Button("Import");
        importButton.addClassName("buttonInDialog");
        importButton.addClassName("buttonHorizontalLayoutInDialog");
        addClickListenerOfEncodeButton(dialog, importButton, glycanWebCanvas);
        buttonLayout.add(importButton);
        Button cancelButton = new Button("Cancel");
        cancelButton.addClassName("buttonInDialog");
        cancelButton.addClassName("buttonHorizontalLayoutInDialog");
        cancelButton.addClassName("button2ndHorizontalLayoutInDialog");
        addClickListenerOfCancelButton(dialog, cancelButton);
        buttonLayout.add(cancelButton);
        importStringDialogLayout.add(buttonLayout);

        // Displays dialog.
        dialog.add(importStringDialogLayout);
        dialog.open();
    }

    /**
     * Adds click listener to import button for importing structures string from encode type.
     * @param dialog Dialog to import drawing structures.
     * @param importButton Encode button to run importing.
     * @param glycanWebCanvas Canvas to draw residues.
     */
    private void addClickListenerOfEncodeButton(Dialog dialog, Button importButton, GlycanWebCanvas glycanWebCanvas) {
        importButton.addClickListener(mouseClickEvent -> {
            try {
                // Import structures string from encode type.
                LinkedList<String> structuresStringList = new LinkedList<String>();
                if (!encodeType.getValue().contains("glycoct")) {
                    for (String structuresString : structuresStringTextArea.getValue().split("\n")) {
                        structuresStringList.addLast(structuresString);
                    }
                } else {
                    structuresStringList.addLast(structuresStringTextArea.getValue());
                }
                for (String structuresString : structuresStringList) {
                    if (!glycanWebCanvas.getGlycanDocument().importFromString(structuresString, encodeType.getValue())) throw new Exception("Occurs an error, when importing structure string.");
                }
                glycanWebCanvas.setEnabledOfUndoRedoButton();
                glycanWebCanvas.updateCanvas();
            } catch (Exception e) {
                new ExplanationDialog("", e.getMessage());
            } finally {
                dialog.close();
            }
        });
    }

    /**
     * Adds click listener to cancel button for closing the dialog.
     * @param dialog Dialog to setting repetition of residue.
     * @param cancelButton Cancel button for closing the dialog.
     */
    private void addClickListenerOfCancelButton(Dialog dialog, Button cancelButton) {
        cancelButton.addClickListener(mouseClickEvent -> {
            dialog.close();
        });
    }
}
