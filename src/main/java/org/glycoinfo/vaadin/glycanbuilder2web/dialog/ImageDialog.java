package org.glycoinfo.vaadin.glycanbuilder2web.dialog;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.Anchor;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.server.StreamResource;
import com.vaadin.flow.server.VaadinSession;

/**
 * Image dialog to display image.
 */
public class ImageDialog {
    /**
     * Creates image dialog to display image.
     * @param image Image to display on dialog.
     * @param streamResource Stream resource to be used when downloading image.
     */
    public ImageDialog(Image image, StreamResource streamResource) {
        Dialog dialog = new Dialog();
        Div div = new Div();
        VerticalLayout verticalLayout = new VerticalLayout();
        H2 title = new H2("Download image");
        title.addClassName("titleOfDialog");
        verticalLayout.add(title);
        Div imageDiv = new Div();
        imageDiv.addClassName("imageContainer");
        imageDiv.add(image);
        verticalLayout.add(imageDiv);
        HorizontalLayout buttonLayout = new HorizontalLayout();
        Anchor okAnchor = new Anchor(VaadinSession.getCurrent().getResourceRegistry().registerResource(streamResource).getResource(), "OK");
        okAnchor.getElement().setAttribute("download", "structures.png");
        okAnchor.addClassName("okAnchor");
        buttonLayout.add(okAnchor);
        Button cancelButton = new Button("Cancel", mouseClickEvent -> {
            dialog.close();
        });
        cancelButton.addClassName("buttonInDialog");
        cancelButton.addClassName("buttonHorizontalLayoutInDialog");
        cancelButton.addClassName("button2ndHorizontalLayoutInDialog");
        buttonLayout.add(cancelButton);
        verticalLayout.add(buttonLayout);
        div.add(verticalLayout);
        dialog.add(div);
        dialog.open();
    }
}
