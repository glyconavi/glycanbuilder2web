package org.glycoinfo.vaadin.glycanbuilder2web.util;

import org.eurocarbdb.application.glycanbuilder.BuilderWorkspace;
import org.eurocarbdb.application.glycanbuilder.GlycanDocument;
import org.glycoinfo.application.glycanbuilder.converterWURCS2.WURCS2Parser;
import org.glycoinfo.vaadin.glycanbuilder2web.canvas.GlycanWebCanvas;

/**
 * Converter to Convert canvas to WURCS.
 */
public class WURCSConverter {
    /**
     * Gets WORCS string from first structure.
     * @param glycanWebCanvas Canvas drawn structures.
     * @return Returns WORCS string.
     */
    public static String getWURCSFromFirstStructure(GlycanWebCanvas glycanWebCanvas) {
        // If structure doesn't exist, returns null.
        BuilderWorkspace builderWorkspace = glycanWebCanvas.getBuilderWorkspace();
        GlycanDocument glycanDocument = builderWorkspace.getStructures();
        if (glycanDocument.isEmpty()) return null;

        // Returns WORCS string.
        WURCS2Parser wurcs2Parser = new WURCS2Parser();
        return glycanDocument.toString(wurcs2Parser);
    }
}
