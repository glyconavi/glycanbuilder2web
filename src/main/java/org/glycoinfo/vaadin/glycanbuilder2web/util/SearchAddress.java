package org.glycoinfo.vaadin.glycanbuilder2web.util;

/**
 * Class with a set of search addresses.
 */
public class SearchAddress {
    /**
     * Address of sparqlist of GlycoNavi.
     */
    public static final String sparqlistOfGlycoNaviAdress = "https://sparqlist.glyconavi.org";

    /**
     * Address of GlyToucan.
     */
    public static final String glyTouCanAdress = "https://glytoucan.org";

    /**
     * Address of GlycoCosmos.
     */
    public static final String glyCosmosAdress = "https://glycosmos.org";

    /**
     * Address of GlycoCosmos.
     */
    public static final String glycoNaviAdress = "https://glyconavi.org";
}
